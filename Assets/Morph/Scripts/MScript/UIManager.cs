using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public List<ITargetViewer> viewers;
    public List<string> allpages = new List<string>();
    public static UIManager Instance;
    void Awake()
    {
        viewers = new List<ITargetViewer>();
        Instance = this;
        foreach(ITargetViewer iv  in BaxterUtils.GetInterfacesInChildrenA<ITargetViewer>(gameObject))
        {
            allpages.Add(iv.GetType.Name);
            viewers.Add(iv);
            if(iv.IsHideObject)
            {
                iv.Hide();
            }    
        }

    }

    public void Show( string classname)
    {
        ITargetViewer itv =  viewers.Find(mda => mda.GetType.Equals(Type.GetType(classname)));
        if(itv!=null)
        {
            itv.Show(itv);
        }
    }

    public void Hide(string classname)
    {
        ITargetViewer itv = viewers.Find(mda => mda.GetType.Equals(Type.GetType(classname)));
        if (itv != null)
        {
            itv.Hide();
        }
    }
}
