using System.Collections;
using System.Collections.Generic;
using Com.Morph.Baxter;
using UnityEngine;

public class VoiceOverManager : MonoBehaviour
{

    public AudioClip[] voiceOverClips;
    public AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void OnVoiceOverStateChange(VoiceOverState voiceOverState) {
        int index = (int)voiceOverState;
        GameManager.Instance.CurrentVoiceOverState = voiceOverState;
        if (audioSource.isPlaying)
            audioSource.Stop();
        audioSource.clip = voiceOverClips[index];
        audioSource.Play();
    }

}
