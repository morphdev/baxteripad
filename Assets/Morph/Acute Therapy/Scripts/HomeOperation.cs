using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.Morph.Baxter;

public class HomeOperation : MonoBehaviour
{

    public MAppController mappcontroller;


    public void OnHomePressed()
    {
        MAppData gameData = new MAppData
        {
            // screenId = MACUTESCREEN.PROGRAMMINGINFUSION,
            // videoid = "vtbi0",
            screenId = "HOME",
            parameter = ""
        };
        mappcontroller.SendDisplayData(gameData);
        // mappcontrolloer.SendLeftRghtData(gameData);
        Debug.LogError("data sent:home: " + gameData);
    }

    public void LateEnabler()
    {
        Invoke("OnHomePressed", 0.75f);
    }

    public void OnHomePressedForPD()
    {
        GameManager.Instance.OnReset();
        //SendData(0);
        Debug.LogError("pd:");
    }


    public void OnRestartPressed()
    {
      /*  MAppData gameData = new MAppData
        {
            // screenId = MACUTESCREEN.PROGRAMMINGINFUSION,
            // videoid = "vtbi0",
            screenId = "HOME",
            parameter = ""
        };
        mappcontroller.SendDisplayData(gameData);*/

        mappcontroller.SendLeftRghtData("/cues/Bank-1/cues/by_cell/col_1/row_2");                               // otherCueDataSend - Therapy Page(reference)
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
