using Com.Morph.Baxter;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Seperate Data mine 
/// </summary>
public class MAppData
{
    public string screenId=""; // which screen open 
    public string subScreenId=""; 
    public string subScreenChildId="";
    public string subScreenSubChildId="";
    public string imageId = "";
    public string videomode = "";
    public string videoid=""; // which video player 
    public string parameter="-1"; // offset [arameters handling 
                                  //parameters handling in , offset or , patterns 
    public Vector2 scrollPosition;              // added 28-06-22 scroll interaction for disposables
    public ModelData modelData;

    public static MAppData CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<MAppData>(jsonString);
    }

    public string SaveToString()
    {
        return JsonUtility.ToJson(this);
    }
}


public class MACUTESCREEN
{
    public const string MEDICATIONDELIVERYTITLE = "MEDICATIONDELIVERYTITLE";
    public const string VIDEOSEEK = "VIDEOSEEK";
    public const string IVTHERAPYTITLE          = "IVTHERAPYTITLE";
    public const string PATIENTPREPARATIONVIDEO = "PATIENTPREPATIONVIDEO";
    public const string IVACCESSTITLE           = "IVACCESSTITLE";
    public const string CONNECTIVITYTITLE           = "CONNECTIVITYTITLE";
    public const string PROGRAMMINGINFUSION     = "PROGRAMMINGINFUSION";
    public const string INFUSIONPROGRESS        = "INFUSIONPROGRESS";
    public const string INFUSIONCOMPLETION      = "INFUSIONCOMPLETION";
    public const string ALARMS                  = "ALARMS";
    public const string DSO = "DSO";
    public const string AIL = "AIL";
    public const string USO = "USO";
    public const string MOREALARMTITLE = "MOREALARMS";
    public const string SYRIGNE = "SYRINGEMISLOADED";
    public const string TUBELOADERROR = "TUBELOADERROR";
    public const string DOORNOTFULLYCLOSE = "DOORNOTFULLYCLOSE";
    public const string BATTERYRELATED = "BATTERYRELATED";
    public const string HALTINGERROR = "HALTINGERROR";
    public const string HARDWARETITLE = "HARDWARE";
    public const string HARDWARESYRINGERPUMP = "HARDWARESYRINGERPUMP";
    public const string BLOCKHARDWARESYRINGERPUMP = "BLOCKHARDWARESYRINGERPUMP";
    public const string BLOCKHARDWARESYRINGEREXPLODED = "BLOCKHARDWARESYRINGERPUMPEXPLDDED";
    public const string BLOCKHARDWARELVP = "BLOCKHARDWARELVP";
    public const string BLOCKHARDWAREEXPLODED = "BLOCKHARDWARELVPEXPLODED";
    public const string CONNECTIVITY = "CONNECTIVITY";
    public const string CONNECTIVITYBDL = "BDL";
    public const string CONNECTIVITYAPL = "APL";
    public const string CONNECTIVITYATL = "ATL";
    public const string CONNECTIVITYASSETDOCUMENT = "ASSETDOCUMENT";



}
