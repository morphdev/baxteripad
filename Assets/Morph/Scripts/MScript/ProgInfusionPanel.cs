using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class ProgInfusionPanel : MonoBehaviour
{
    public int infusionId;
    public Sprite select, nonselect;
    public Button btn1, btn2, btn3, btn4, btn5,btn6,btn7;
    public List<Button> btns;
    public TherapyPage thpage;
    public List<GameObject> objects;

    // Start is called before the first frame update
    void Start()
    {
        btn1.onClick.AddListener(Button1Op);
        btn2.onClick.AddListener(Button2Op);
        btn3.onClick.AddListener(Button3Op);
        btn4.onClick.AddListener(Button4Op);
        btn5.onClick.AddListener(Button5Op);
        btn6.onClick.AddListener(Button6Op);
        btn7.onClick.AddListener(Button7Op);
        btns.Add(btn1);
        btns.Add(btn2);
        btns.Add(btn3);
        btns.Add(btn4);
        btns.Add(btn5);
        btns.Add(btn6);
        btns.Add(btn7);
    }
    public Transform tr;
    void OffSkills(int index)
    {
         tr = objects[index].transform;
        for (int i = 0; i < objects.Count; i++)
        {
            objects[i].transform.GetChild(0).gameObject.SetActive(true);
            objects[i].transform.GetChild(1).gameObject.SetActive(false);
        }
        tr.GetChild(0).gameObject.SetActive(false);
        tr.GetChild(1).gameObject.SetActive(true);

    }

    private void EndReach(VideoPlayer source)
    {
     //PlayState();
    }

    public Image play, pause, replay;
    public GameObject videoplayparent;
    public MVideoSeek mvs;
    public void PlayPause(bool isplay)
    {
        thpage.PlayPauseVideo(isplay);
        if (isplay)
        {
            pause.gameObject.SetActive(true);
            play.gameObject.SetActive(false);
            replay.gameObject.SetActive(false);

        }
        else
        {
            pause.gameObject.SetActive(false);
            play.gameObject.SetActive(true);
            replay.gameObject.SetActive(false);
        }
        videoplayparent.SetActive(!isplay);
    }
    public void Replay()
    {
        bool isplay = true;
        thpage.PlayPauseVideo(isplay);
        if (isplay)
        {
            pause.gameObject.SetActive(true);
            play.gameObject.SetActive(false);
            replay.gameObject.SetActive(false);
        }
        videoplayparent.SetActive(!isplay);

    }
    private void OnEnable()
    {
        Button1Op();
        PlayPause(false);
        //mvs.video.isLooping = false;
        mvs.video.loopPointReached += EndReach;
    }
    private void Button7Op()
    {
        //foreach (Button btn in btns)
        //    btn.image.sprite = nonselect;
        //btn7.image.sprite = select;
        OffSkills(6);
        thpage.PlayVideo("bolusdose"+infusionId);
        currentvideostate = "bolusdose";
    }

    private void Button6Op()
    {
        //foreach (Button btn in btns)
        //    btn.image.sprite = nonselect;
        //btn6.image.sprite = select;
        OffSkills(5);
        thpage.PlayVideo("vtbichange"+infusionId);
        currentvideostate = "vtbichange";

    }

    // Update is called once per frame
    public void Button1Op()
    {
        //foreach (Button btn in btns)
        //    btn.image.sprite = nonselect;
        //btn1.image.sprite = select;
        OffSkills(0);

        thpage.PlayVideo("newpatient"+infusionId);
        currentvideostate = "newpatient";
    }
    void Button2Op()
    {
        //foreach (Button btn in btns)
        //    btn.image.sprite = nonselect;
        //btn2.image.sprite = select;
        OffSkills(1);

        thpage.PlayVideo("medsurg"+infusionId);
        currentvideostate = "medsurg";


    }
    void Button3Op()
    {
        //foreach (Button btn in btns)
        //    btn.image.sprite = nonselect;
        //btn3.image.sprite = select;
        OffSkills(2);

        thpage.PlayVideo("rate"+infusionId);
        currentvideostate = "rate";


    }
    void Button4Op()
    {
        //foreach (Button btn in btns)
        //    btn.image.sprite = nonselect;
        //btn4.image.sprite = select;
        OffSkills(3);

        thpage.PlayVideo("vtbi"+infusionId);
        currentvideostate = "vtbi";

    }
    void Button5Op()
    {
        //foreach (Button btn in btns)
        //    btn.image.sprite = nonselect;
        //btn5.image.sprite = select;
        OffSkills(4);

        thpage.PlayVideo("clamps"+infusionId);
        currentvideostate = "clamps";

    }

    public string currentvideostate = "";
    float dta = 0f;
    void PlayState()
    {
        if ((Time.time - dta) > 1f)
        {
            dta = Time.time;
            switch (currentvideostate)
            {
                case "newpatient":
                    Button2Op();
                    break;
                case "medsurg":
                    Button3Op();
                    break;
                case "rate":
                    Button4Op();
                    break;
                case "vtbi":
                    Button5Op();
                    break;
                case "clamps":
                    Button6Op();
                    break;
                case "vtbichange":
                    Button7Op();
                    break;
            }
        }
    }
    private void OnDisable()
    {
        thpage.vplayer.loopPointReached -= EndReach;
    }
}
