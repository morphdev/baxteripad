using Com.Morph.Baxter;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class AlarmScenarioHandler : MonoBehaviour
{

    public GameObject sliderGameObject;
    public GameObject seekSliderGameObject;
    private GameObject currentMachineContent;
    private int currentMachineScreenId;
    public GameObject videoRenderer;
    public ClariaAlarmScenarioState state;
    public GameController gameControllerScript;
    public GameObject play;
    public GameObject pause;
    public VideoPlayer videoPlayer;
    public GameObject machineObject;
    public GameObject endMachineObject;

    private void OnEnable()
    {

        if (machineObject)
            machineObject.SetActive(true);

        if (endMachineObject) {
            endMachineObject.SetActive(false);
            endMachineObject.transform.GetChild(endMachineObject.transform.childCount - 1).gameObject.SetActive(false);
        }

        if (videoRenderer.activeInHierarchy)
            videoRenderer.GetComponent<VideoPlayer>().loopPointReached -= OnVideoEnd;

        sliderGameObject.SetActive(false);
        videoRenderer.SetActive(false);
        seekSliderGameObject.SetActive(false);

        GameManager.Instance.CurrentClariaAlarmScenarioState = state;

        currentMachineScreenId = 5; //1 is added in the machine click button
        for (int i = currentMachineScreenId + 1; i < transform.GetChild(0).childCount; i++)
        {
            //Debug.Log(currentMachineScreenId);
            transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
        }
        OnMachineButtonClick();

    }

    public void OnMachineButtonClick()
    {
        int buttonId;
        currentMachineScreenId++;
        switch (GameManager.Instance.CurrentClariaAlarmScenarioState)
        {
            case ClariaAlarmScenarioState.LowDrain:
                switch (currentMachineScreenId)
                {
                    case 6: //check patient line
                        buttonId = 1;
                        break;
                    case 7: //initial drain
                        videoRenderer.SetActive(true);
                        sliderGameObject.SetActive(true);
                        seekSliderGameObject.SetActive(true);

                        if (machineObject)
                            machineObject.SetActive(false);

                        buttonId = -1;
                        videoRenderer.GetComponent<VideoPlayer>().loopPointReached += OnVideoEnd;
                        //occlusion
                        gameControllerScript.SendData(Constants.TVTherapyAlarmScenarioLowDrainId, -1);
                        break;
                    default:    //14
                        buttonId = -1;
                        break;
                }
                break;
            case ClariaAlarmScenarioState.Occlusion:
                switch (currentMachineScreenId)
                {
                    case 6: //1 0f 5
                        buttonId = 2;
                        break;
                    case 7: //2 0f 5
                        //Debug.Log("hello");
                        //start video
                        videoRenderer.SetActive(true);
                        sliderGameObject.SetActive(true);
                        seekSliderGameObject.SetActive(true);
                        if (machineObject)
                            machineObject.SetActive(false);

                        buttonId = -1;
                        videoRenderer.GetComponent<VideoPlayer>().loopPointReached += OnVideoEnd;
                        gameControllerScript.SendData(Constants.TVTherapyAlarmScenarioOcclusionId, -1);
                        break;
                    default:    //14
                        buttonId = -1;
                        break;
                }
                break;
            default:
                buttonId = -1;
                break;
        }

        gameControllerScript.PlayButtonClickSound();
        PatientUIHandler(currentMachineScreenId, buttonId);
    }

    private void OnVideoEnd(VideoPlayer videoPlayer) {
        videoPlayer.loopPointReached -= OnVideoEnd;
        PatientUIHandler(currentMachineScreenId, 1);

        if (endMachineObject) {
            endMachineObject.SetActive(true);
            endMachineObject.transform.GetChild(0).gameObject.SetActive(true);
        }
        videoRenderer.SetActive(false);
        sliderGameObject.SetActive(false);
        seekSliderGameObject.SetActive(false);
        switch (GameManager.Instance.CurrentClariaAlarmScenarioState) {
            case ClariaAlarmScenarioState.Occlusion:
                gameControllerScript.SendData(Constants.TVAlarmScenarioOcclusion, 1);
                break;
            case ClariaAlarmScenarioState.LowDrain:
                gameControllerScript.SendData(Constants.TVAlarmScenarioLowDrain, 1);
                break;
        }

    }


    private void PatientUIHandler(int id, int buttonId)
    {
        if (currentMachineContent != null)
            currentMachineContent.SetActive(false);
        currentMachineContent = transform.GetChild(0).GetChild(id).gameObject;
        currentMachineContent.SetActive(true);
        transform.GetChild(0).GetChild(0).gameObject.SetActive(false); //glow
        if (buttonId != -1)
        {   //if clickable screen
            transform.GetChild(0).GetChild(0).GetComponent<RectTransform>().anchoredPosition = transform.GetChild(0).GetChild(buttonId).GetComponent<RectTransform>().anchoredPosition;
            transform.GetChild(0).GetChild(0).GetComponent<RectTransform>().localScale = buttonId == 4 || buttonId == 5 ? 0.5f * Vector3.one : Vector3.one;
            transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
        }
        //SendData(Constants.TVConnectivityPatientId, id, buttonId); //clickeUIId = content, subscreenId = buttonId
    }

    private void OnDisable()
    {
        GameManager.Instance.CurrentClariaAlarmScenarioState = ClariaAlarmScenarioState.None;
    }

    public void OnPlayPause(bool pausePlayback)
    {
        if (pausePlayback)
        {
            if (videoPlayer.isPlaying)
                videoPlayer.Pause();
            play.SetActive(true);
            pause.SetActive(false);
            gameControllerScript.SendData(Constants.TVVideoControls, 0);
        }
        else
        {
            if (!videoPlayer.isPlaying)
                videoPlayer.Play();
            play.SetActive(false);
            pause.SetActive(true);
            gameControllerScript.SendData(Constants.TVVideoControls, 1);
        }
        gameControllerScript.PlayButtonClickSound();
    }




}
