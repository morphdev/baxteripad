using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class TherapyPage : TargetViewer<TherapyPage>
{
    public HomeOperation homeOperation;


    [Header("Current Open Panel Name ")]
    public string currentpanelopen;

    public Button ivtherapy, alarm, hardwarebtn, conctivitybtn, restartbtn, projectorbtn, homebutton;
    public Vector3 ivtherapyposn, alarmposn, hardwareposn, connectposn;
    public GameObject ivtherapysub, alarmsub, hardwaresub, connectivitysub, morealarmsub;
    public List<GameObject> panels, subpanels;
    public VideoClip patientpreparation_clip, iv_switching, iv_insertblue, iv_loadrubing, iv_closingdoor, iv_checktheline,
                     prog_inf_newpatient, pro_med_surg, pro_rate, prog_vtbi, prog_clamps, pro_vtb_change, pro_bolus_dose,
                     prog_inf_newpatient2, pro_med_surg2, pro_rate2, prog_vtbi2, prog_clamps2, pro_vtb_change2, pro_bolus_dose2,
                     inf_prog_clip, inf_prog_clip_2, infusion_completion_sec, infusion_completion_lneflush, infusion_return_infusion,
                     dsoclip, usoclip, ailclip, morealarmsclip, syringealarmclip, syringemisloadedclip, flushcompleteclip,
                     kvoalarmclip, inactivityclip, hadwaresyringeblockclip, hardwaresyringeexplodeclip,
                     hardwarelvpblockclip,
                     hardwarelvpexplodeclip, connectivitytitlevideo,
                     connectivity_binarydrugclip, connectivity_autoprogramming, connectivity_autodocclip, connectivity_assetclip, svpexplode, lvpexplode;
    public GameObject ivpanel,
                      medicationdelivery,
                      patientpreparation,
                      hardwarepanel,
                      ivaccess,
                      programminginfusion,
        secondaryInfusion,
                      infusionprogress,
                      dsopanel,
                      usopanel,
                      ailpanel,
                      syringemisloadedpanel,
                      tubeloadedpanel,
                      doonotclosepanel,
                      batteryrelatedpanel,
                      haltingerrorpanel,
                      inactivityalarmpanel,
                      syringepumppanel,
                      ivppanel,
                      contbypanel,
                      alarmpanel,
                      morealarmpanel,
                      infusioncompletion;
    public MAppController mappcontrolloer;
    public List<TextMeshProUGUI> alltexts;
    public Color selectedcolor, whitecolor;
    public Sprite minussprite, plussprite;
    public AudioSource audioSource;
    public AudioClip buttonClickSound;

    public List<TextMeshProUGUI> alltextsForAcuteTherapy;
    //public List<TextMeshProUGUI> alltextsForAcuteTherapy2;

    List<TextMeshProUGUI> currentAllTexts;

    public void DisableText(TextMeshProUGUI selectedtext = null)
    {
        foreach (TextMeshProUGUI texta in alltexts)
            texta.color = selectedcolor;
        if (selectedtext != null)
            selectedtext.color = whitecolor;
    }

    public void DisableTextForAcuteTherapy(TextMeshProUGUI selectedtext = null)
    {
        foreach (TextMeshProUGUI texta in alltextsForAcuteTherapy)
            texta.color = selectedcolor;
        if (selectedtext != null)
        {
            selectedtext.color = whitecolor;
        }
    }

    public void SetColorWhite(TextMeshProUGUI selectedtext = null)
    {
        if (selectedtext != null)
        {
            selectedtext.color = whitecolor;
        }
    }

    public void SetColorBlue(TextMeshProUGUI selectedtext = null)
    {
        if (selectedtext != null)
        {
            selectedtext.color = selectedcolor;
        }
    }


    public void PlayPauseVideo(bool isplay)
    {
        if (!isplay)
            vplayer.Pause();
        else
            vplayer.Play();
        MAppData gameDataa = new MAppData
        {
            screenId = isplay ? "PLAY" : "PAUSE",
            videoid = "",
            parameter = ""
        };
        mappcontrolloer.SendDisplayData(gameDataa);
    }
    public void ActivateDeActivateProjectorButton()
    {
        if (

            currentpanelopen.Contains("patientpreparation")
            ||
            currentpanelopen.Contains("ivaccess")
            ||
            currentpanelopen.Contains("programminginfusion")
            ||
            currentpanelopen.Contains("infusionprogress")
            ||
            currentpanelopen.Contains("SecondaryInfusion")
            ||
            string.IsNullOrEmpty(currentpanelopen)
           )
        {
            if (!projectorbtn.gameObject.activeSelf)
                projectorbtn.gameObject.SetActive(true);
        }
        else
        {
            if (projectorbtn.gameObject.activeSelf)
                projectorbtn.gameObject.SetActive(false);
        }
        if (currentpanelopen.Contains("syringepump")
            ||
            currentpanelopen.Contains("lvp"))
        {
            hidedobject.GetComponent<RawImage>().enabled = false;
        }
        else
        {
            hidedobject.GetComponent<RawImage>().enabled = true;

        }
    }
    internal void PlayVideo(string v)
    {
        switch (v)
        {
            case "SVPMODEL":
                MAppData gameDat2aaa = new MAppData
                {
                    screenId = "SVPMODEL",
                    videoid = "",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameDat2aaa);
                break;
            case "LVPMODEL":
                MAppData gameDat2aa = new MAppData
                {
                    screenId = "LVPMODEL",
                    videoid = "",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameDat2aa);
                break;
            //case "SVPINT":
            //    MAppData gameDatSPInt = new MAppData
            //    {
            //        screenid = "SVPINT",
            //        videoid = "",
            //        parameter = ""
            //    };
            //    mappcontrolloer.SendDisplayData(gameDatSPInt);
            //    break;
            //case "LVPINT":
            //    MAppData gameDatLVPInt = new MAppData
            //    {
            //        screenid = "LVPINT",
            //        videoid = "",
            //        parameter = ""
            //    };
            //    mappcontrolloer.SendDisplayData(gameDatLVPInt);
            //    break;

            case "SVPVideo":
                MAppData gameDat2a = new MAppData
                {
                    screenId = "SVPVideo",
                    videoid = "",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameDat2a);
                vplayer.Stop();
                vplayer.clip = svpexplode;
                vplayer.Play();
                break;
            case "LVPVideo":
                MAppData gameDat3a = new MAppData
                {
                    screenId = "LVPVideo",
                    videoid = "",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameDat3a);
                vplayer.Stop();
                vplayer.clip = lvpexplode;
                vplayer.Play();
                break;
            case "ConB1":
                MAppData gameDat1a = new MAppData
                {
                    screenId = "ConB1",
                    videoid = "",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameDat1a);
                break;
            case "ConB2":
                MAppData gameData2 = new MAppData
                {
                    screenId = "ConB2",
                    videoid = "",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData2);
                break;
            case "ConB3":
                MAppData gameData3 = new MAppData
                {
                    screenId = "ConB3",
                    videoid = "",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData3);
                break;
            case "ConB4":
                MAppData gameData4 = new MAppData
                {
                    screenId = "ConB4",
                    videoid = "",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData4);
                break;
            case "ConB5":
                MAppData gameData5 = new MAppData
                {
                    screenId = "ConB5",
                    videoid = "",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData5);
                break;
            case "ConB6":
                MAppData gameData6 = new MAppData
                {
                    screenId = "ConB6",
                    videoid = "",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData6);
                break;
            case "ConB7":
                MAppData gameData7 = new MAppData
                {
                    screenId = "ConB7",
                    videoid = "",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData7);
                break;
            case "infusionprogress_1":
                MAppData gameDataaa = new MAppData
                {
                    screenId = "infusionprogress_1",
                    videoid = "",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameDataaa);

                vplayer.Stop();
                vplayer.clip = inf_prog_clip;
                vplayer.Play();
                ivTherapyBtn.WaitForVidFinish(13.98f, vplayer);
                break;
            case "infusionprogress_2":
                MAppData gameDataaaa = new MAppData
                {
                    screenId = "infusionprogress_2",
                    videoid = "",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameDataaaa);

                vplayer.Stop();
                vplayer.clip = inf_prog_clip_2;
                vplayer.Play();
                Debug.Log("Infusion Progress 2");
                //ivTherapyBtn.WaitForVidFinish(61, vplayer);
                break;
            case "connectivitytitle":
                MAppData gameDataa = new MAppData
                {
                    screenId = MACUTESCREEN.CONNECTIVITYTITLE,
                    videoid = "",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameDataa);
                vplayer.Stop();
                vplayer.clip = connectivitytitlevideo;
                vplayer.Play();
                break;
            case "switching":
                GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)iv_switching.length;
                MAppData gameData = new MAppData
                {
                    screenId = MACUTESCREEN.IVACCESSTITLE,
                    videoid = "switching",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);

                vplayer.Stop();
                vplayer.clip = iv_switching;
                vplayer.Play();

                break;
            case "insertingblue":
                //GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)iv_insertblue.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.IVACCESSTITLE,
                    videoid = "insertingblue",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = iv_insertblue;
                vplayer.Play();
                break;
            case "loadtubing":
                GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)flushcompleteclip.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.IVACCESSTITLE,
                    videoid = "loadtubing",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = iv_loadrubing;
                vplayer.Play();
                break;
            case "closingdoor":
                GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)iv_closingdoor.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.IVACCESSTITLE,
                    videoid = "closingdoor",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = iv_closingdoor;
                vplayer.Play();
                break;
            case "checktheline":
                GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)iv_checktheline.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.IVACCESSTITLE,
                    videoid = "checktheline",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = iv_checktheline;
                vplayer.Play();
                ivTherapyBtn.WaitForVidFinish(16.5f, vplayer);
                break;
            case "newpatient0":
                GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)prog_inf_newpatient.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.PROGRAMMINGINFUSION,
                    videoid = "newpatient0",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = prog_inf_newpatient;
                vplayer.Play();
                break;
            case "medsurg0":
                GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)pro_med_surg.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.PROGRAMMINGINFUSION,
                    videoid = "medsurg0",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = pro_med_surg;
                vplayer.Play();
                break;
            case "rate0":
                GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)pro_rate.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.PROGRAMMINGINFUSION,
                    videoid = "rate0",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = pro_rate;
                vplayer.Play();
                break;
            case "vtbi0":
                GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)prog_vtbi.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.PROGRAMMINGINFUSION,
                    videoid = "vtbi0",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = prog_vtbi;
                vplayer.Play();
                break;
            case "clamps0":
                GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)prog_clamps.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.PROGRAMMINGINFUSION,
                    videoid = "clamps0",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = prog_clamps;
                vplayer.Play();
                break;
            case "vtbichange0":
                GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)pro_vtb_change.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.PROGRAMMINGINFUSION,
                    videoid = "vtbichange0",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = pro_vtb_change;
                vplayer.Play();
                break;
            case "bolusdose0":
                GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)pro_bolus_dose.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.PROGRAMMINGINFUSION,
                    videoid = "bolusdose0",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = pro_bolus_dose;
                vplayer.Play();
                break;

            case "newpatient1":
                GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)prog_inf_newpatient.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.PROGRAMMINGINFUSION,
                    videoid = "newpatient1",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = prog_inf_newpatient;
                vplayer.Play();
                break;
            case "medsurg1":
                GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)pro_med_surg.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.PROGRAMMINGINFUSION,
                    videoid = "medsurg1",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = pro_med_surg;
                vplayer.Play();
                break;
            case "rate1":
                GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)pro_rate.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.PROGRAMMINGINFUSION,
                    videoid = "rate1",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = pro_rate;
                vplayer.Play();
                break;
            case "vtbi1":
                GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)prog_vtbi.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.PROGRAMMINGINFUSION,
                    videoid = "vtbi1",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = prog_vtbi;
                vplayer.Play();
                break;
            case "clamps1":
                GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)prog_clamps.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.PROGRAMMINGINFUSION,
                    videoid = "clamps1",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = prog_clamps;
                vplayer.Play();
                break;
            case "vtbichange1":
                GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)pro_vtb_change.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.PROGRAMMINGINFUSION,
                    videoid = "vtbichange1",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = pro_vtb_change;
                vplayer.Play();
                break;
            case "bolusdose1":
                GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)pro_bolus_dose.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.PROGRAMMINGINFUSION,
                    videoid = "bolusdose1",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = pro_bolus_dose;
                vplayer.Play();
                break;

            case "infseccomplete":
                GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)infusion_completion_sec.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.INFUSIONCOMPLETION,
                    videoid = "infseccomplete",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = infusion_completion_sec;
                vplayer.Play();
                break;
            case "inflineflush":
                GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)infusion_completion_lneflush.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.INFUSIONCOMPLETION,
                    videoid = "inflineflush",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = infusion_completion_lneflush;
                vplayer.Play();
                break;
            case "infreturntofusion":
                GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)infusion_return_infusion.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.INFUSIONCOMPLETION,
                    videoid = "infreturntofusion",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = infusion_return_infusion;
                vplayer.Play();
                break;
            case "conatvideo":
                //GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)connectivity_assetclip.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.CONNECTIVITY,
                    videoid = "conatvideo",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = connectivity_assetclip;
                vplayer.Play();
                break;
            case "conadvideo":
                //GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)connectivity_autodocclip.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.CONNECTIVITY,
                    videoid = "conadvideo",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = connectivity_autodocclip;
                vplayer.Play();
                break;
            case "conapvideo":
                //GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)connectivity_autoprogramming.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.CONNECTIVITY,
                    videoid = "conapvideo",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = connectivity_autoprogramming;
                vplayer.Play();
                break;
            case "conbdvideo":
                //GameObject.FindObjectOfType<MVideoSeek>().gameObject.GetComponent<Slider>().maxValue = (float)connectivity_binarydrugclip.length;

                gameData = new MAppData
                {
                    screenId = MACUTESCREEN.CONNECTIVITY,
                    videoid = "conbdvideo",
                    parameter = ""
                };
                mappcontrolloer.SendDisplayData(gameData);
                vplayer.Stop();
                vplayer.clip = connectivity_binarydrugclip;
                vplayer.Play();
                break;
        }
    }

    [Header("IV THERAPY SUB BUTTONS")]
    public Button prepatientbtn, ivaccessbtn, progrinfusionbtn, secInfusionBtn, infprogressbtn, infcompletionbtn;
    [Header("Alarm")]
    public Button alarmbtn, dsobtn, usobtn, ailbtn, morealarmbtn, syringebtn, flushcomplete, kvoalarmnbtn, inactivebtn;
    public Button syringepump, lvp;
    public Button con1, con2, con3, con4;
    public VideoPlayer vplayer;
    public GameObject lvpInteractive, spInteractive;
    public ProjectionMapHighlight dsoProjMapBtn;
    public ProjectionMapHighlight ivTherapyBtn;

    void IVSubOperation()
    {
        prepatientbtn.onClick.AddListener(PreparaOperation);
        ivaccessbtn.onClick.AddListener(IVOperation);
        progrinfusionbtn.onClick.AddListener(ProgInfOperation);
        secInfusionBtn.onClick.AddListener(SecInfOperation);
        infprogressbtn.onClick.AddListener(InfusionOperation);
        infcompletionbtn.onClick.AddListener(InfComOperation);
        AlarmInitiate();
    }

    void AlarmInitiate()
    {
        alarmbtn.onClick.AddListener(AlarmOperationNew);
        dsobtn.onClick.AddListener(DsoOperation);
        usobtn.onClick.AddListener(UsoOperation);
        ailbtn.onClick.AddListener(AilOperation);
        morealarmbtn.onClick.AddListener(MoreAlarmOperation);

        syringebtn.onClick.AddListener(AlarmSyringeOperation);
        flushcomplete.onClick.AddListener(FlushComplete);
        kvoalarmnbtn.onClick.AddListener(KVOAlarmOperation);
        inactivebtn.onClick.AddListener(InActiveOperation);
        //haltingbtn.onClick.AddListener(HaltingOperation);
        //inactivealarmbtn.onClick.AddListener(InActiveOperation);

        syringepump.onClick.AddListener(HardwareSyringeOperation);
        lvp.onClick.AddListener(LvpOperation);


        con1.onClick.AddListener(Connection1Operation);
        con2.onClick.AddListener(Connection2Operation);
        con3.onClick.AddListener(Connection3Operation);
        con4.onClick.AddListener(Connection4Operation);

    }

    private void AlarmOperationNew()
    {
        //DisablePanels();
        //alarmpanel.SetActive(true);
        //MAppData gameData = new MAppData
        //{
        //    screenId = MACUTESCREEN.ALARMS,
        //    videoid = "",
        //    parameter = ""
        //};
        //mappcontrolloer.SendDisplayData(gameData);
    }

    void Connection1Operation()
    {

    }
    void Connection2Operation()
    {

    }
    void Connection3Operation()
    {

    }
    void Connection4Operation()
    {

    }

    private void LvpOperation()
    {
        DisablePanels();
        ivppanel.gameObject.SetActive(true);
        //lvpInteractive.SetActive(true);
        spInteractive.SetActive(false);
    }
    private void AlarmSyringeOperation()
    {
        MAppData gameData = new MAppData
        {
            screenId = MACUTESCREEN.MOREALARMTITLE,
            videoid = MACUTESCREEN.SYRIGNE,
            parameter = ""
        };
        mappcontrolloer.SendDisplayData(gameData);
        DisablePanels();
        syringemisloadedpanel.SetActive(true);
        vplayer.Stop();
        vplayer.clip = syringealarmclip;
        vplayer.Play();
    }
    private void HardwareSyringeOperation()
    {
        MAppData gameData = new MAppData
        {
            screenId = MACUTESCREEN.HARDWARETITLE,
            videoid = "",
            parameter = ""
        };
        mappcontrolloer.SendDisplayData(gameData);
        DisablePanels();
        syringepumppanel.gameObject.SetActive(true);
        lvpInteractive.SetActive(false);
    }

    private void InActiveOperation()
    {
        MAppData gameData = new MAppData
        {
            screenId = MACUTESCREEN.MOREALARMTITLE,
            videoid = "INACTIVITY",
            parameter = ""
        };
        mappcontrolloer.SendDisplayData(gameData);
        DisablePanels();
        inactivityalarmpanel.SetActive(true);

        vplayer.Stop();
        vplayer.clip = inactivityclip;
        vplayer.Play();
    }

    private void HaltingOperation()
    {
        //MAppData gameData = new MAppData
        //{
        //    screenid = MACUTESCREEN.MOREALARMTITLE,
        //    videoid = MACUTESCREEN.HALTINGERROR,
        //    parameter = ""
        //};
        //mappcontrolloer.SendDisplayData(gameData);
        //DisablePanels();
        //haltingerrorpanel.SetActive(true);
        //vplayer.Stop();
        //vplayer.clip = haltingerrorclip;
        //vplayer.Play();
    }

    private void BatteryOperation()
    {

        //MAppData gameData = new MAppData
        //{
        //    screenid = MACUTESCREEN.MOREALARMTITLE,
        //    videoid = MACUTESCREEN.BATTERYRELATED,
        //    parameter = ""
        //};
        //mappcontrolloer.SendDisplayData(gameData);
        //DisablePanels();
        //batteryrelatedpanel.SetActive(true);
        //vplayer.Stop();
        //vplayer.clip = batteryrelatedclip;
        //vplayer.Play();
    }

    private void KVOAlarmOperation()
    {
        MAppData gameData = new MAppData
        {
            screenId = MACUTESCREEN.MOREALARMTITLE,
            videoid = "KVOALARMOPERATION",
            parameter = ""
        };
        mappcontrolloer.SendDisplayData(gameData);
        DisablePanels();
        doonotclosepanel.SetActive(true);
        vplayer.Stop();
        vplayer.clip = kvoalarmclip;
        vplayer.Play();
    }

    private void FlushComplete()
    {
        MAppData gameData = new MAppData
        {
            screenId = MACUTESCREEN.MOREALARMTITLE,
            videoid = "FLUSHCOMPLETE",
            parameter = ""
        };
        mappcontrolloer.SendDisplayData(gameData);
        DisablePanels();
        tubeloadedpanel.SetActive(true);
        vplayer.Stop();
        vplayer.clip = flushcompleteclip;
        vplayer.Play();
    }

    private void SyringeOperation()
    {
        MAppData gameData = new MAppData
        {
            screenId = MACUTESCREEN.MOREALARMTITLE,
            videoid = MACUTESCREEN.SYRIGNE,
            parameter = ""
        };
        mappcontrolloer.SendDisplayData(gameData);
        DisablePanels();
        syringepumppanel.SetActive(true);
        vplayer.Stop();
        vplayer.clip = syringemisloadedclip;
        vplayer.Play();

    }

    private void MoreAlarmOperation()
    {
        DisablePanels();
        bool isactivate = morealarmsub.gameObject.activeSelf;
        isactivate = !isactivate;
        MAppData gameData = new MAppData
        {
            screenId = MACUTESCREEN.MOREALARMTITLE,
            videoid = "",
            parameter = ""
        };
        mappcontrolloer.SendDisplayData(gameData);
        vplayer.Stop();
        vplayer.clip = morealarmsclip;
        vplayer.Play();
        if (isactivate)
        {
            morealarmsub.gameObject.SetActive(isactivate);
            morealarmpanel.SetActive(true);
            alarm.transform.localPosition = alarmposn;//- new Vector3(0f, 400f, 0f);
            hardwarebtn.transform.localPosition = hardwareposn - new Vector3(0f, 500f, 0f);
            conctivitybtn.transform.localPosition = connectposn - new Vector3(0f, 500f, 0f);
            morealarmsub.gameObject.SetActive(isactivate);
            //morealarmpanel.SetActive(true);
            //alarm.transform.localPosition = alarmposn;// - new Vector3(0f, 100f, 0f);
            //hardwarebtn.transform.localPosition = hardwareposn - new Vector3(0f, 250f, 0f);
            //conctivitybtn.transform.localPosition = connectposn - new Vector3(0f, 250f, 0f);
        }
        else
        {
            morealarmsub.gameObject.SetActive(isactivate);
            morealarmpanel.SetActive(true);
            alarm.transform.localPosition = alarmposn;// - new Vector3(0f, 100f, 0f);
            hardwarebtn.transform.localPosition = hardwareposn - new Vector3(0f, 250f, 0f);
            conctivitybtn.transform.localPosition = connectposn - new Vector3(0f, 250f, 0f);
            //ivpanel.SetActive(true);
        }

        Invoke("OtherCueDataSend", 0.1f);
    }



    private void AilOperation()
    {
        MAppData gameData = new MAppData
        {
            screenId = MACUTESCREEN.ALARMS,
            videoid = "AIL",
            parameter = ""
        };
        mappcontrolloer.SendDisplayData(gameData);
        DisablePanels();
        ailpanel.SetActive(true);
        vplayer.Stop();
        vplayer.clip = ailclip;
        vplayer.Play();
        Invoke("OtherCueDataSend", 0.1f);
    }

    private void UsoOperation()
    {
        MAppData gameData = new MAppData
        {
            screenId = MACUTESCREEN.ALARMS,
            videoid = "USO",
            parameter = ""
        };
        mappcontrolloer.SendDisplayData(gameData);
        DisablePanels();
        usopanel.SetActive(true);
        vplayer.Stop();
        vplayer.clip = usoclip;
        vplayer.Play();
        Invoke("OtherCueDataSend", 0.1f);
    }

    private void DsoOperation()
    {
        MAppData gameData = new MAppData
        {
            screenId = MACUTESCREEN.ALARMS,
            videoid = "DSO",
            parameter = ""
        };
        Debug.Log("DSO Open");
        mappcontrolloer.SendDisplayData(gameData);
        DisablePanels();
        dsopanel.SetActive(true);
        vplayer.Stop();
        vplayer.clip = dsoclip;
        vplayer.Play();
        dsoProjMapBtn.WaitForVidFinish(20.5f, vplayer);
    }



    private void InfComOperation()
    {
        DisablePanels();
        //medicationdelivery.SetActive(true);
        infusioncompletion.SetActive(true);

        OtherCueDataSend();                                 // added 31-03-22 for sending data on btn click
    }

    private void InfusionOperation()
    {
        DisablePanels();
        infusionprogress.GetComponent<progressvideo>().PassVideo(vplayer, inf_prog_clip, inf_prog_clip_2);
        infusionprogress.SetActive(true);



    }

    private void ProgInfOperation()
    {
        DisablePanels();
        programminginfusion.SetActive(true);
    }

    private void SecInfOperation()
    {
        DisablePanels();
        secondaryInfusion.SetActive(true);
    }

    private void IVOperation()
    {
        DisablePanels();
        ivaccess.SetActive(true);

    }

    private void PreparaOperation()
    {
        MAppData gameData = new MAppData
        {
            screenId = MACUTESCREEN.IVTHERAPYTITLE,
            videoid = MACUTESCREEN.PATIENTPREPARATIONVIDEO,
            parameter = ""
        };
        mappcontrolloer.SendDisplayData(gameData);
        DisablePanels();
        patientpreparation.SetActive(true);
        vplayer.Stop();
        vplayer.clip = patientpreparation_clip;
        vplayer.Play();
        ivTherapyBtn.WaitForVidFinish(69.5f, vplayer);
    }

    // Start is called before the first frame update
    protected override void Show(TherapyPage current)
    {
        base.Show(this);
        DisablePanels();
        medicationdelivery.SetActive(true);
        if (vplayer == null)
            vplayer = GameObject.FindObjectOfType<VideoPlayer>();

        Invoke("DelayedMedTitle", 0.55f);                   //0.55

    }

    void DelayedMedTitle()
    {
        MAppData gameData = new MAppData
        {
            screenId = MACUTESCREEN.MEDICATIONDELIVERYTITLE,
            videoid = "",
            parameter = ""
        };
        mappcontrolloer.SendDisplayData(gameData);
    }

    void Start()
    {
        ivtherapy.onClick.AddListener(IVPanelOpen);
        alarm.onClick.AddListener(AlarmPanelOpen);
        hardwarebtn.onClick.AddListener(HardwarePanelOpen);
        conctivitybtn.onClick.AddListener(ConnectivityOpen);
        restartbtn.onClick.AddListener(RestartOperation);
        projectorbtn.onClick.AddListener(ProjectorOperation);
        homebutton.onClick.AddListener(HomeOperation);

        panels.Add(ivpanel);
        panels.Add(hardwarepanel);
        panels.Add(medicationdelivery);
        panels.Add(patientpreparation);
        panels.Add(ivaccess);
        panels.Add(programminginfusion);
        panels.Add(secondaryInfusion);
        panels.Add(infusionprogress);
        panels.Add(dsopanel);
        panels.Add(usopanel);
        panels.Add(ailpanel);
        panels.Add(syringemisloadedpanel);
        panels.Add(tubeloadedpanel);
        panels.Add(doonotclosepanel);
        panels.Add(batteryrelatedpanel);
        panels.Add(haltingerrorpanel);
        panels.Add(inactivityalarmpanel);
        panels.Add(syringepumppanel);
        panels.Add(ivppanel);
        panels.Add(contbypanel);
        panels.Add(alarmpanel);
        panels.Add(morealarmpanel);
        panels.Add(infusioncompletion);
        subpanels.Add(ivtherapysub);
        subpanels.Add(alarmsub);
        subpanels.Add(hardwaresub);
        subpanels.Add(connectivitysub);
        subpanels.Add(morealarmsub);

        ivtherapyposn = ivtherapy.transform.localPosition;
        alarmposn = alarm.transform.localPosition;
        hardwareposn = hardwarebtn.transform.localPosition;
        connectposn = conctivitybtn.transform.localPosition;
        IVSubOperation();
    }
    void HomeOperation()
    {
        OtherCueDataSend();


        Application.LoadLevel(Application.loadedLevel);

        DisablePanels();
        if (ivtherapysub.activeSelf)
        {
            //IVPanelOpen();
            IVPanelOpen2MD();
            Debug.LogError("IV PANEL OPEN: " + ivtherapysub.activeSelf);
            
        }
        if (alarmsub.activeSelf)
        {
            //AlarmPanelOpen();
            AlarmPanelOpen2MD();
            Debug.LogError("alarm PANEL OPEN: " + alarmsub.activeSelf);
            
        }
        if (hardwaresub.activeSelf)
        {
           // HardwarePanelOpen();
            HardwarePanelOpen2MD();
            //homeOperation.LateEnabler();
            Debug.LogError("alarm PANEL OPEN: " + hardwaresub.activeSelf);
            
        }

        #region commented
        /*if (ivtherapysub.activeSelf)
            IVPanelOpen();
        if (alarmsub.activeSelf)
            AlarmPanelOpen();
        if (hardwaresub.activeSelf)
            HardwarePanelOpen();*/
        //            ivtherapy.onClick.AddListener(IVPanelOpen);
        //alarm.onClick.AddListener(AlarmPanelOpen);
        //hardwarebtn.onClick.AddListener(HardwarePanelOpen);
        //conctivitybtn.onClick.AddListener(ConnectivityOpen);
        #endregion

        medicationdelivery.SetActive(true);
        if (vplayer == null)
            vplayer = GameObject.FindObjectOfType<VideoPlayer>();

        homeOperation.LateEnabler();                                                    // added 30-03-22
        

    }
    void ProjectorOperation()
    {
        /*
         * /cues/selected/cues/by_cell/col_1/row_3
/cues/selected/cues/by_cell/col_2/row_3
/cues/selected/cues/by_cell/col_3/row_3
/cues/selected/cues/by_cell/col_4/row_3
/cues/selected/cues/by_cell/col_5/row_3
/cues/selected/cues/by_cell/col_6/row_3
/cues/selected/cues/by_cell/col_7/row_3iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
/cues/selected/cues/by_cell/col_8/row_3
         */
        if (currentpanelopen.Contains("medicationdelivey"))
        {
            //mappcontrolloer.SendLeftRghtData("/cues/selected/cues/by_cell/col_1/row_3");
            // mappcontrolloer.SendLeftRghtData("/cues/selected/cues/by_cell/col_2/row_3");
        }
        if (currentpanelopen.Contains("ivtherapy"))
        {
            //mappcontrolloer.SendLeftRghtData("/cues/selected/cues/by_cell/col_2/row_3");
            //  mappcontrolloer.SendLeftRghtData("/cues/selected/cues/by_cell/col_4/row_3");
        }
        if (currentpanelopen.Contains("patientpreparation"))
        {
            mappcontrolloer.SendLeftRghtData("/cues/selected/cues/by_cell/col_1/row_3");

        }
        if (currentpanelopen.Contains("ivaccess"))
        {
            mappcontrolloer.SendLeftRghtData("/cues/selected/cues/by_cell/col_2/row_3");

        }
        if (currentpanelopen.Contains("programminginfusion"))
        {
            mappcontrolloer.SendLeftRghtData("/cues/selected/cues/by_cell/col_3/row_3");

        }

        if (currentpanelopen.Contains("SecondaryInfusion"))
        {
            mappcontrolloer.SendLeftRghtData("/cues/selected/cues/by_cell/col_3/row_3");

        }
        if (currentpanelopen.Contains("infusionprogress"))
        {
            mappcontrolloer.SendLeftRghtData("/cues/selected/cues/by_cell/col_4/row_3");

        }

    }

    public void DSOProjMapDataSend()
    {
        mappcontrolloer.SendLeftRghtData("/cues/Bank-1/cues/by_cell/col_5/row_3");
    }

    void OtherCueDataSend()
    {
        mappcontrolloer.SendLeftRghtData("/cues/Bank-1/cues/by_cell/col_1/row_2");

    }

    void RestartOperation()
    {
        OtherCueDataSend();

        MAppData gameData = new MAppData
        {
            //screenId = "RESTART",
            screenId = "HOME",
            videoid = "",
            parameter = ""
        };
        mappcontrolloer.SendDisplayData(gameData);
        mappcontrolloer.SendLeftRghtData(gameData);           // commented

        homeOperation.OnRestartPressed();                    // restart button- baxter video plays

        Application.LoadLevel(0);

    }

    public void ScreenSaverDataDelayer()
    {

    }

    private void ConnectivityOpen()
    {
        OtherCueDataSend();

        DisablePanels();
        contbypanel.SetActive(true);
    }

    private void HardwarePanelOpen()
    {

        OtherCueDataSend();

        DisablePanels();
        bool isactivate = hardwaresub.gameObject.activeSelf;
        isactivate = !isactivate;
        SpecificOff(hardwaresub);
        MAppData gameDat2aaa = new MAppData
        {
            screenId = "HARDWARE",
            videoid = "",
            parameter = ""
        };
        mappcontrolloer.SendDisplayData(gameDat2aaa);
        //mappcontrolloer.SendLeftRghtData(gameData);
        if (isactivate)
        {
            hardwaresub.gameObject.SetActive(isactivate);
            alarm.transform.localPosition = alarmposn;
            hardwarebtn.transform.localPosition = hardwareposn;// - new Vector3(0f, 400f, 0f);
            conctivitybtn.transform.localPosition = connectposn - new Vector3(0f, 150f, 0f);
            hardwarepanel.SetActive(true);
        }
        else
        {
            hardwaresub.gameObject.SetActive(isactivate);
            alarm.transform.localPosition = alarmposn;// - new Vector3(0f, 100f, 0f);
            hardwarebtn.transform.localPosition = hardwareposn;// - new Vector3(0f, 100f, 0f);
            conctivitybtn.transform.localPosition = connectposn;// - new Vector3(0f, 100f, 0f);
        }
        hardwarebtn.transform.GetChild(0).gameObject.GetComponent<Image>().sprite =
            ivtherapysub.gameObject.activeSelf ? minussprite : plussprite;

    }

    private void AlarmPanelOpen()
    {
        OtherCueDataSend();

        SpecificOff(alarmsub);
        bool isactivate = alarmsub.gameObject.activeSelf;

        DisableSubPanels();

        isactivate = !isactivate;
        if (isactivate)
        {
            DisablePanels();
            alarmsub.gameObject.SetActive(isactivate);
            alarm.transform.localPosition = alarmposn;//- new Vector3(0f, 400f, 0f);
            hardwarebtn.transform.localPosition = hardwareposn - new Vector3(0f, 250f, 0f);
            conctivitybtn.transform.localPosition = connectposn - new Vector3(0f, 250f, 0f);
            alarmpanel.SetActive(true);
            medicationdelivery.gameObject.SetActive(false);
            MAppData gameData = new MAppData
            {
                screenId = MACUTESCREEN.ALARMS,
                videoid = "",
                parameter = ""
            };
            mappcontrolloer.SendDisplayData(gameData);
        }
        else
        {
            alarmpanel.SetActive(true);
            Debug.Log("Close Sub panel");
            alarmsub.gameObject.SetActive(isactivate);
            alarm.transform.localPosition = alarmposn;// - new Vector3(0f, 100f, 0f);
            hardwarebtn.transform.localPosition = hardwareposn;// - new Vector3(0f, 100f, 0f);
            conctivitybtn.transform.localPosition = connectposn;// - new Vector3(0f, 100f, 0f);
            DisablePanels();
            medicationdelivery.gameObject.SetActive(true);
            MAppData gameData = new MAppData
            {
                screenId = MACUTESCREEN.MEDICATIONDELIVERYTITLE,
                videoid = "",
                parameter = ""
            };
            mappcontrolloer.SendDisplayData(gameData);
        }

        alarm.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = alarmsub.gameObject.activeSelf ? minussprite : plussprite;


    }

    private void AlarmPanelOpen2MD()
    {
        OtherCueDataSend();

        SpecificOff(alarmsub);
        bool isactivate = alarmsub.gameObject.activeSelf;

        DisableSubPanels();

        isactivate = !isactivate;
        if (isactivate)
        {
            DisablePanels();
            alarmsub.gameObject.SetActive(isactivate);
            alarm.transform.localPosition = alarmposn;//- new Vector3(0f, 400f, 0f);
            hardwarebtn.transform.localPosition = hardwareposn - new Vector3(0f, 250f, 0f);
            conctivitybtn.transform.localPosition = connectposn - new Vector3(0f, 250f, 0f);
            alarmpanel.SetActive(true);
            medicationdelivery.gameObject.SetActive(false);
            MAppData gameData = new MAppData
            {
                screenId = MACUTESCREEN.ALARMS,
                videoid = "",
                parameter = ""
            };
            mappcontrolloer.SendDisplayData(gameData);
        }
        else
        {
            alarmpanel.SetActive(true);
            Debug.Log("Close Sub panel");
            alarmsub.gameObject.SetActive(isactivate);
            alarm.transform.localPosition = alarmposn;// - new Vector3(0f, 100f, 0f);
            hardwarebtn.transform.localPosition = hardwareposn;// - new Vector3(0f, 100f, 0f);
            conctivitybtn.transform.localPosition = connectposn;// - new Vector3(0f, 100f, 0f);
            DisablePanels();
            medicationdelivery.gameObject.SetActive(true);
            MAppData gameData = new MAppData
            {
                //screenId = MACUTESCREEN.MEDICATIONDELIVERYTITLE,
                screenId = "HOME",
                videoid = "",
                parameter = ""
            };
            mappcontrolloer.SendDisplayData(gameData);
        }

        alarm.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = alarmsub.gameObject.activeSelf ? minussprite : plussprite;


    }

    private void IVPanelOpen2MD()
    {
        OtherCueDataSend();
        SpecificOff(ivtherapysub);
        DisablePanels();
        // ivpanel.SetActive(true);
        bool isactivate = ivtherapysub.gameObject.activeSelf;
        isactivate = !isactivate;
        if (isactivate)
        {
            ivtherapysub.gameObject.SetActive(isactivate);
            alarm.transform.localPosition = alarmposn - new Vector3(0f, 455f, 0f);
            hardwarebtn.transform.localPosition = hardwareposn - new Vector3(0f, 455f, 0f);
            conctivitybtn.transform.localPosition = connectposn - new Vector3(0f, 455f, 0f);
            DisablePanels();
            ivpanel.SetActive(true);
            //patientpreparation.SetActive(true);
            medicationdelivery.gameObject.SetActive(false);
            MAppData gameData = new MAppData
            {
                screenId = MACUTESCREEN.IVTHERAPYTITLE,
                videoid = "",
                parameter = ""
            };
            mappcontrolloer.SendDisplayData(gameData);
            mappcontrolloer.SendLeftRghtData(gameData);
        }
        else
        {
            ivtherapysub.gameObject.SetActive(isactivate);
            alarm.transform.localPosition = alarmposn;// - new Vector3(0f, 100f, 0f);
            hardwarebtn.transform.localPosition = hardwareposn;// - new Vector3(0f, 100f, 0f);
            conctivitybtn.transform.localPosition = connectposn;// - new Vector3(0f, 100f, 0f);
            DisablePanels();
            ivpanel.SetActive(false);
            medicationdelivery.gameObject.SetActive(true);
            MAppData gameData = new MAppData
            {
                //screenId = MACUTESCREEN.MEDICATIONDELIVERYTITLE,
                screenId = "HOME",
                videoid = "",
                parameter = ""
            };
            mappcontrolloer.SendDisplayData(gameData);

        }


    }

    private void HardwarePanelOpen2MD()
    {

        OtherCueDataSend();

        DisablePanels();
        bool isactivate = hardwaresub.gameObject.activeSelf;
        isactivate = !isactivate;
        SpecificOff(hardwaresub);
       /* MAppData gameDat2aaa = new MAppData
        {
            screenId = "HARDWARE",
            videoid = "",
            parameter = ""
        };
        mappcontrolloer.SendDisplayData(gameDat2aaa);*/
        //mappcontrolloer.SendLeftRghtData(gameData);
        if (isactivate)
        {
            hardwaresub.gameObject.SetActive(isactivate);
            alarm.transform.localPosition = alarmposn;
            hardwarebtn.transform.localPosition = hardwareposn;// - new Vector3(0f, 400f, 0f);
            conctivitybtn.transform.localPosition = connectposn - new Vector3(0f, 150f, 0f);
            hardwarepanel.SetActive(true);
        }
        else
        {
            hardwaresub.gameObject.SetActive(isactivate);
            alarm.transform.localPosition = alarmposn;// - new Vector3(0f, 100f, 0f);
            hardwarebtn.transform.localPosition = hardwareposn;// - new Vector3(0f, 100f, 0f);
            conctivitybtn.transform.localPosition = connectposn;// - new Vector3(0f, 100f, 0f);
        }
        hardwarebtn.transform.GetChild(0).gameObject.GetComponent<Image>().sprite =
            ivtherapysub.gameObject.activeSelf ? minussprite : plussprite;

        MAppData gameData = new MAppData
        {
            //screenId = MACUTESCREEN.MEDICATIONDELIVERYTITLE,
            screenId = "HOME",
            videoid = "",
            parameter = ""
        };
        mappcontrolloer.SendDisplayData(gameData);
    }


    public void SpecificOff(GameObject obja)
    {
        foreach (GameObject obj in subpanels)
        {
            if (!obja.gameObject.GetHashCode().Equals(obj.GetHashCode()))
                obj.SetActive(false);
        }
    }
    private void IVPanelOpen()
    {
        OtherCueDataSend();
        SpecificOff(ivtherapysub);
        DisablePanels();
        // ivpanel.SetActive(true);
        bool isactivate = ivtherapysub.gameObject.activeSelf;
        isactivate = !isactivate;
        if (isactivate)
        {
            ivtherapysub.gameObject.SetActive(isactivate);
            alarm.transform.localPosition = alarmposn - new Vector3(0f, 455f, 0f);
            hardwarebtn.transform.localPosition = hardwareposn - new Vector3(0f, 455f, 0f);
            conctivitybtn.transform.localPosition = connectposn - new Vector3(0f, 455f, 0f);
            DisablePanels();
            ivpanel.SetActive(true);
            //patientpreparation.SetActive(true);
            medicationdelivery.gameObject.SetActive(false);
            MAppData gameData = new MAppData
            {
                screenId = MACUTESCREEN.IVTHERAPYTITLE,
                videoid = "",
                parameter = ""
            };
            mappcontrolloer.SendDisplayData(gameData);
            mappcontrolloer.SendLeftRghtData(gameData);
        }
        else
        {
            ivtherapysub.gameObject.SetActive(isactivate);
            alarm.transform.localPosition = alarmposn;// - new Vector3(0f, 100f, 0f);
            hardwarebtn.transform.localPosition = hardwareposn;// - new Vector3(0f, 100f, 0f);
            conctivitybtn.transform.localPosition = connectposn;// - new Vector3(0f, 100f, 0f);
            DisablePanels();
            ivpanel.SetActive(false);
            medicationdelivery.gameObject.SetActive(true);
            MAppData gameData = new MAppData
            {
                screenId = MACUTESCREEN.MEDICATIONDELIVERYTITLE,
                videoid = "",
                parameter = ""
            };
            mappcontrolloer.SendDisplayData(gameData);

        }


    }
    void DisablePanels()
    {
        foreach (GameObject obj in panels)
            obj.SetActive(false);

    }

    void DisableSubPanels()
    {
        foreach (GameObject obj in subpanels)
            obj.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
        if (hidedobject.activeSelf)
        {
            ivtherapy.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = ivtherapysub.gameObject.activeSelf ? minussprite : plussprite;
            alarm.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = alarmsub.gameObject.activeSelf ? minussprite : plussprite;
            hardwarebtn.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = hardwaresub.gameObject.activeSelf ? minussprite : plussprite;
            morealarmbtn.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = morealarmsub.gameObject.activeSelf ? minussprite : plussprite;
            hardwarebtn.GetComponent<TextMeshProUGUI>().color = hardwaresub.gameObject.activeSelf ? whitecolor : selectedcolor;
            ivtherapy.GetComponent<TextMeshProUGUI>().color = ivtherapysub.gameObject.activeSelf ? whitecolor : selectedcolor;
            alarm.GetComponent<TextMeshProUGUI>().color = alarmsub.gameObject.activeSelf ? whitecolor : selectedcolor;
            if (panels.Where(mda => mda.activeSelf).ToList().Count > 0)
                currentpanelopen = panels.Where(mda => mda.activeSelf).ToList()[0].name;
            ActivateDeActivateProjectorButton();
        }
    }

    // 21-03-22 created
    /* public void AcuteTherapyOperationsAT(string textt)
     {
         MAppData gameData = new MAppData
         {
             screenId = MACUTESCREEN.PROGRAMMINGINFUSION,
             videoid = "vtbi0",
             parameter = ""
         };
         mappcontrolloer.SendDisplayData(gameData);
        // mappcontrolloer.SendLeftRghtData(gameData);
         Debug.LogError("data sent:");
     }*/
}
