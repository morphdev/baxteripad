using Com.Morph.Baxter;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClariaNurseHandler : MonoBehaviour
{
    public GameObject coachMark;
    public Transform tooltipParent;
    private GameObject currentTooltip;
    public GameObject help;
    public GameObject helpButton;
    public GameController gameControllerScript;


    private void OnEnable()
    {
        //helpButton.SetActive(true);
        help.SetActive(false);
        Invoke("DisableCoachMork", 5f);
    }

    private void DisableCoachMork() {
        coachMark.SetActive(false);
    }

    public void OnActionClick(int id) {
        if (currentTooltip)
            currentTooltip.SetActive(false);
        gameControllerScript.SendNurseActionData(id);
        //no tooltip
        if (id == -1) {
            tooltipParent.gameObject.SetActive(false);
            return;
        }   
        currentTooltip = tooltipParent.GetChild(0).GetChild(id).gameObject;
        currentTooltip.SetActive(true);
        tooltipParent.gameObject.SetActive(true);
    }

    public void OnBackClick()
    {
        if (currentTooltip)
            currentTooltip.SetActive(false);
        currentTooltip = null;
        tooltipParent.gameObject.SetActive(false);
        //helpButton.SetActive(true);

    }




}
