using Com.Morph.Baxter;
using UnityEngine;
using UnityEngine.Video;

public class SolutionHandler : MonoBehaviour
{

    public VideoClip inClip;
    public VideoClip loopClip;
    private VideoPlayer videoPlayer;
    private VideoController videoControllerScript;
    public GameObject otherObject;

    public GameObject play;
    public GameObject pause;
    public GameController gameControllerScript;


    private void Awake()
    {
        videoPlayer = GetComponentInChildren<VideoPlayer>();
        videoControllerScript = GetComponentInChildren<VideoController>();
    }

    private void OnEnable()
    {
        if (otherObject)
            otherObject.SetActive(false);

        videoPlayer.clip = inClip;
        videoPlayer.isLooping = false;
        videoPlayer.loopPointReached += OnVideoEnd;
        videoPlayer.Play();

        if (videoControllerScript == null)
            return;
        videoControllerScript.enabled = false;
        videoControllerScript.slider.maxValue = (float)videoPlayer.length;
        videoControllerScript.enabled = true;

    }

    private void OnVideoEnd(VideoPlayer video) {
        videoPlayer.loopPointReached -= OnVideoEnd;
        videoPlayer.isLooping = true;
        videoPlayer.clip = loopClip;
        videoPlayer.Play();

        if (videoControllerScript == null)
            return;
        videoControllerScript.enabled = false;
        videoControllerScript.slider.maxValue = (float)videoPlayer.length;
        videoControllerScript.enabled = true;
    }

    private void OnDisable()
    {
        if(!videoPlayer.isLooping)
            videoPlayer.loopPointReached -= OnVideoEnd;
    }

    public void OnPlayPause(bool pausePlayback)
    {
        if (pausePlayback)
        {
            if (videoPlayer.isPlaying)
                videoPlayer.Pause();
            play.SetActive(true);
            pause.SetActive(false);

            gameControllerScript.SendData(Constants.TVVideoControls, 0);
        }
        else
        {
            if (!videoPlayer.isPlaying)
                videoPlayer.Play();
            play.SetActive(false);
            pause.SetActive(true);

            gameControllerScript.SendData(Constants.TVVideoControls, 1);
        }
    }


}
