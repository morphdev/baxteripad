using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class ModelInteractorAT : MonoBehaviour
{
    public VideoPlayer explodedVideo;

    public GameObject modelAT;

    public void SetRotation()
    {
        modelAT.transform.rotation = Quaternion.identity;
    }

    public void ReleaseTexture()
    {
        explodedVideo.targetTexture.Release();
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
