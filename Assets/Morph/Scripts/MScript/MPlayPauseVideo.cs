using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MPlayPauseVideo : MonoBehaviour
{
    public Image play, pause, replay;
    public GameObject videoplayparent;
    public MVideoSeek mvs;
    public TherapyPage thpage;
    private void OnEnable()
    {
        PlayPause(false);
        Invoke("PlayPauseAgain", 0.1f);
    }
    void PlayPauseAgain()
    {
        PlayPause(false);
        mvs.slider.maxValue =(float) thpage.vplayer.length;
    }
    public void PlayPause(bool isplay)
    {
        thpage.PlayPauseVideo(isplay);
        if (isplay)
        {
            pause.gameObject.SetActive(true);
            play.gameObject.SetActive(false);
            replay.gameObject.SetActive(false);

        }
        else
        {
            pause.gameObject.SetActive(false);
            play.gameObject.SetActive(true);
            replay.gameObject.SetActive(false);

        }
        videoplayparent.SetActive(!isplay);
    }
    public void Replay()
    {
        bool isplay = true;
        thpage.PlayPauseVideo(isplay);
        if (isplay)
        {
            pause.gameObject.SetActive(true);
            play.gameObject.SetActive(false);
            replay.gameObject.SetActive(false);
        }
        videoplayparent.SetActive(!isplay);

    }
}
