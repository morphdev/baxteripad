using Com.Morph.Baxter;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class ActiveTherapyHandler : MonoBehaviour
{

    private Transform myTransform;
    public Transform sidebarButtons;

    private GameObject lastSidebarButton;

    public VideoPlayer videoPlayer;
    public VideoPlayer cassetteVideoPlayer;

    public VideoClip[] graphVideos;
    public VideoClip[] cassetteVideos;

    public VideoClip[] cassetteLoopVideos;


    public GameObject[] sliders;

    private GameObject lastSlider;

    public GameObject[] machineContent;
    public GameObject[] machineSelection;

    public GameObject videoRenderer;

    private int currentButtonId;
    private GameObject currentTherapyGameObject;
    private GameObject currentMachineContent;
    private GameObject currentMachineSelection;
    private int currentMachineScreenId;
    public VideoClip[] osmosis;
    public GameController gameControllerScript;

    public GameObject osmosisGameObject;
    public ProjectionMapHighlight ManProjectionMapHighlight;

    private int osmosisId;

    private VideoHandler videoHandlerScript;
    private int lastButtonId;

    public GameObject[] therapyButtons;
    private GameObject currentTherapy;

    public RectTransform osmosisToggleRT;

    private float frameCount;
    private bool videoPaused;

    /***** --- MY CODE -----****/
    public int currentOsmosisID;
    public int currentosmosisBtnID = -1;
    public List<Button> currentOsmosisBtns;
    public List<Button> activeTherapybuttons;                       // 3 btns at the bottom

    private void Awake()
    {
        myTransform = transform;
        videoHandlerScript = GetComponent<VideoHandler>();
    }

    private void OnEnable()
    {
        //sidebarButtons
        //select graph
        //play fill animation
        GameManager.Instance.CurrentClariaTherapiesState = ClariaTherapiesState.Fill;
        osmosisGameObject.SetActive(GameManager.Instance.CurrentClariaTherapiesState == ClariaTherapiesState.Dwell);
        osmosisGameObject.transform.GetChild(0).Find("Off").gameObject.SetActive(true);
        osmosisGameObject.transform.GetChild(0).Find("On").gameObject.SetActive(false);
        osmosisGameObject.transform.GetChild(1).Find("Off").gameObject.SetActive(true);
        osmosisGameObject.transform.GetChild(1).Find("On").gameObject.SetActive(false);
        osmosisId = 0;
        //osmosisToggleRT.pivot = new Vector2(0, 0.5f);
        //osmosisToggleRT.anchoredPosition = Vector2.zero;
        //osmosisToggleRT.anchorMin = osmosisId == 0 ? new Vector2(0, 0.5f) : new Vector2(1f, 0.5f);
        //osmosisToggleRT.anchorMax = osmosisId == 0 ? new Vector2(0, 0.5f) : new Vector2(1f, 0.5f);

        SidebarHandler(0);
        //Debug.Log("Enable");
        videoPlayer.loopPointReached += OnVideoEnd;

        SetOnClickBtn();                        // added 16-08-22

        MakeInteractableBtns();
    }

    private void OnDisable()
    {
        videoPlayer.loopPointReached -= OnVideoEnd;
    }

    private void SidebarHandler(int buttonId)
    {
        if (lastSidebarButton != null)
        {
            lastSidebarButton.transform.Find("Off").gameObject.SetActive(true);
            lastSidebarButton.transform.Find("On").gameObject.SetActive(false);
        }
        lastSidebarButton = sidebarButtons.GetChild(buttonId).gameObject;
        lastSidebarButton.transform.Find("Off").gameObject.SetActive(false);
        lastSidebarButton.transform.Find("On").gameObject.SetActive(true);

        currentButtonId = buttonId;

        ManageTherapy();

    }

    private void ManageTherapy()
    {
        if (lastSlider != null)
            lastSlider.SetActive(false);
        if (currentTherapyGameObject != null)
            currentTherapyGameObject.SetActive(false);
        if (currentMachineSelection != null)
            currentMachineSelection.SetActive(false);
        if (currentTherapy != null)
        {
            currentTherapy.transform.Find("Off").gameObject.SetActive(true);
            currentTherapy.transform.Find("On").gameObject.SetActive(false);
        }
        //Debug.Log(GameManager.Instance.CurrentClariaTherapiesState);

        //Debug.Log(currentButtonId);
        switch (currentButtonId)
        {
            case 0: //graph
                cassetteVideoPlayer.gameObject.SetActive(false);
                videoPlayer.enabled = true;
                myTransform.GetChild(0).gameObject.SetActive(true);
                videoPlayer.clip = graphVideos[(int)GameManager.Instance.CurrentClariaTherapiesState];
                //lastSlider = sliders[(int)GameManager.Instance.CurrentClariaTherapiesState];
                currentTherapy = therapyButtons[(int)GameManager.Instance.CurrentClariaTherapiesState];
                currentTherapyGameObject = videoRenderer;
                gameControllerScript.SendData(Constants.TVActiveTherapyGraph[(int)GameManager.Instance.CurrentClariaTherapiesState], -1);

                if (videoPaused)
                {
                    videoPaused = false;
                    videoPlayer.frame = (long)frameCount;
                }

                ManProjectionMapHighlight.WaitForVidFinish((float)videoPlayer.clip.length-0.15f, videoPlayer);
                videoPlayer.Play();
                videoHandlerScript.OnVideoChange();

                osmosisGameObject.transform.GetChild(osmosisId).Find("Off").gameObject.SetActive(true);
                osmosisGameObject.transform.GetChild(osmosisId).Find("On").gameObject.SetActive(false);
                break;
            case 2: //cassette
                videoPaused = false;

                //Debug.Log(myTransform.GetChild(0).name);
                videoPlayer.enabled = false;
                myTransform.GetChild(0).gameObject.SetActive(false);
                cassetteVideoPlayer.gameObject.SetActive(false);

                //videoPlayer.clip = cassetteVideos[(int)GameManager.Instance.CurrentClariaTherapiesState];
                //lastSlider = sliders[(int)GameManager.Instance.CurrentClariaTherapiesState];
                currentTherapy = therapyButtons[(int)GameManager.Instance.CurrentClariaTherapiesState];
                cassetteVideoPlayer.GetComponent<SolutionHandler>().inClip = cassetteVideos[(int)GameManager.Instance.CurrentClariaTherapiesState];
                cassetteVideoPlayer.GetComponent<SolutionHandler>().loopClip = cassetteLoopVideos[(int)GameManager.Instance.CurrentClariaTherapiesState];
                cassetteVideoPlayer.gameObject.SetActive(true);
                currentTherapyGameObject = null;
                gameControllerScript.SendData(Constants.TVActiveTherapyCassette[(int)GameManager.Instance.CurrentClariaTherapiesState], -1);
                //videoPlayer.Play();
                //videoHandlerScript.OnVideoChange();
                osmosisGameObject.transform.GetChild(osmosisId).Find("Off").gameObject.SetActive(true);
                osmosisGameObject.transform.GetChild(osmosisId).Find("On").gameObject.SetActive(false);

                //Debug.Log(myTransform.GetChild(0).gameObject.activeInHierarchy);

                break;
            case 3: //mannequin
                videoPaused = true;
                frameCount = videoPlayer.frame;

                videoPlayer.enabled = false;
                myTransform.GetChild(0).gameObject.SetActive(false);
                cassetteVideoPlayer.gameObject.SetActive(false);

                cassetteVideoPlayer.GetComponent<SolutionHandler>().inClip = cassetteVideos[(int)GameManager.Instance.CurrentClariaTherapiesState];
                cassetteVideoPlayer.GetComponent<SolutionHandler>().loopClip = cassetteLoopVideos[(int)GameManager.Instance.CurrentClariaTherapiesState];

                //cassetteVideoPlayer.gameObject.SetActive(true);
                currentTherapy = therapyButtons[(int)GameManager.Instance.CurrentClariaTherapiesState];
                currentTherapyGameObject = null;
                gameControllerScript.SendData(Constants.TVEmpty, (int)GameManager.Instance.CurrentClariaTherapiesState);

                osmosisGameObject.transform.GetChild(osmosisId).Find("Off").gameObject.SetActive(true);
                osmosisGameObject.transform.GetChild(osmosisId).Find("On").gameObject.SetActive(false);

                gameControllerScript.SendOSCMessage(Constants.MannTherapyTherapiesAddress[(int)GameManager.Instance.CurrentClariaTherapiesState]);

                break;
            case 1: //machine content
                videoPaused = false;

                cassetteVideoPlayer.gameObject.SetActive(false);
                //videoPlayer.gameObject.SetActive(false);
                videoPlayer.enabled = false;
                myTransform.GetChild(0).gameObject.SetActive(false);
                currentTherapyGameObject = machineContent[(int)GameManager.Instance.CurrentClariaTherapiesState];
                currentMachineScreenId = 5; //1 is added in the machine click button    //last one's title
                for (int i = currentMachineScreenId + 1; i < currentTherapyGameObject.transform.childCount - 1; i++)
                {
                    currentTherapyGameObject.transform.GetChild(i).gameObject.SetActive(false);
                }
                OnMachineButtonClick();
                //lastSlider = null;
                currentTherapy = therapyButtons[(int)GameManager.Instance.CurrentClariaTherapiesState];
                videoPlayer.Stop();
                osmosisGameObject.transform.GetChild(osmosisId).Find("Off").gameObject.SetActive(true);
                osmosisGameObject.transform.GetChild(osmosisId).Find("On").gameObject.SetActive(false);
                break;
            case 4:
                cassetteVideoPlayer.gameObject.SetActive(false);
                //videoPlayer.gameObject.SetActive(true);
                myTransform.GetChild(0).gameObject.SetActive(true);
                videoPlayer.enabled = true;
                videoPlayer.clip = osmosis[osmosisId];
                videoPlayer.Play();
                ManProjectionMapHighlight.WaitForVidFinish((float)videoPlayer.clip.length-0.15f, videoPlayer);
                videoHandlerScript.OnVideoChange();
                currentTherapy = therapyButtons[(int)GameManager.Instance.CurrentClariaTherapiesState];
                //lastSlider = sliders[(int)GameManager.Instance.CurrentClariaTherapiesState];
                currentTherapyGameObject = videoRenderer;
                gameControllerScript.SendData(Constants.TVActiveTherapyOsmosis[osmosisId], -1);
                break;
        }
        //Debug.Log(myTransform.GetChild(0).gameObject.activeInHierarchy);
        if (lastSlider != null)
            lastSlider.SetActive(true);
        //Debug.Log(myTransform.GetChild(0).gameObject.activeInHierarchy);
        if (currentTherapy != null)
        {
            currentTherapy.transform.Find("Off").gameObject.SetActive(false);
            currentTherapy.transform.Find("On").gameObject.SetActive(true);
        }
        if (currentTherapyGameObject)
            currentTherapyGameObject.SetActive(true);

        //Debug.Log(myTransform.GetChild(0).gameObject.activeInHierarchy);


    }

    //Graph, Content, Cassette, Mannequin
    public void OnSidebarButtonClick(int buttonId)
    {
        gameControllerScript.PlayButtonClickSound();
        SidebarHandler(buttonId);
    }

    //Fill, Dwell, Drain
    public void OnBottomBarButtonClick(int buttonId)
    {
        if (currentButtonId == 4)
        {
            currentButtonId = lastButtonId;
            SidebarHandler(currentButtonId);
        }
        //Debug.Log("Clicked");
        GameManager.Instance.CurrentClariaTherapiesState = (ClariaTherapiesState)buttonId;
        osmosisGameObject.SetActive(GameManager.Instance.CurrentClariaTherapiesState == ClariaTherapiesState.Dwell);
        ManageTherapy();
        gameControllerScript.PlayButtonClickSound();
    }

    public void OnMachineButtonClick()
    {
        int buttonId;
        currentMachineScreenId++;
        switch (GameManager.Instance.CurrentClariaTherapiesState)
        {
            case ClariaTherapiesState.Fill:
            case ClariaTherapiesState.Dwell:
                switch (currentMachineScreenId)
                {
                    case 6: //1 0f 5
                        buttonId = 5;
                        break;
                    default:    //14
                        buttonId = -1;
                        break;
                }
                break;
            case ClariaTherapiesState.Drain:
                switch (currentMachineScreenId)
                {
                    case 6: //1 0f 5
                        buttonId = 5;
                        break;
                    default:    //14
                        buttonId = -1;
                        break;
                }
                break;
            default:
                buttonId = -1;
                break;
        }

        gameControllerScript.PlayButtonClickSound();
        PatientUIHandler(currentMachineScreenId, buttonId);
    }


    private void PatientUIHandler(int id, int buttonId)
    {
        if (currentMachineContent != null)
            currentMachineContent.SetActive(false);
        currentMachineContent = currentTherapyGameObject.transform.GetChild(id).gameObject;
        currentMachineContent.SetActive(true);
        currentTherapyGameObject.transform.GetChild(0).gameObject.SetActive(false); //glow

        if (currentMachineSelection != null)
            currentMachineSelection.SetActive(false);
        //currentMachineSelection = machineSelection[(int)GameManager.Instance.CurrentClariaTherapiesState];
        //currentMachineSelection.SetActive(true);

        if (buttonId != -1)
        {   //if clickable screen
            currentTherapyGameObject.transform.GetChild(0).GetComponent<RectTransform>().anchoredPosition = currentTherapyGameObject.transform.GetChild(buttonId).GetComponent<RectTransform>().anchoredPosition;
            currentTherapyGameObject.transform.GetChild(0).GetComponent<RectTransform>().localScale = buttonId == 4 || buttonId == 5 ? 0.5f * Vector3.one : Vector3.one;
            currentTherapyGameObject.transform.GetChild(0).gameObject.SetActive(true);
        }
        //SendData(Constants.TVConnectivityPatientId, id, buttonId); //clickeUIId = content, subscreenId = buttonId
        gameControllerScript.SendData(Constants.TVActiveTherapyMachine[(int)GameManager.Instance.CurrentClariaTherapiesState], currentMachineScreenId);
    }

    public void OsmosisButtonHandler(int id)
    {
        if (currentosmosisBtnID == id)
            return;

        if (currentButtonId != 4)
            lastButtonId = currentButtonId;
        osmosisGameObject.transform.GetChild(osmosisId).Find("Off").gameObject.SetActive(true);
        osmosisGameObject.transform.GetChild(osmosisId).Find("On").gameObject.SetActive(false);
        osmosisGameObject.transform.GetChild(id).Find("Off").gameObject.SetActive(false);
        osmosisGameObject.transform.GetChild(id).Find("On").gameObject.SetActive(true);
        osmosisId = id;
        OnSidebarButtonClick(4);                              // commented 16-08-22 to check

       

        //osmosisToggleRT.pivot = osmosisId == 0 ? new Vector2(0, 0.5f) : new Vector2(1f, 0.5f);
        //osmosisToggleRT.anchoredPosition = Vector2.zero;
        //osmosisToggleRT.anchorMin = osmosisId == 0 ? new Vector2(0, 0.5f) : new Vector2(1f, 0.5f);
        //osmosisToggleRT.anchorMax = osmosisId == 0 ? new Vector2(0, 0.5f) : new Vector2(1f, 0.5f);

        videoPlayer.clip = osmosis[osmosisId];
        videoPlayer.Play();
        ManProjectionMapHighlight.WaitForVidFinish((float)videoPlayer.clip.length-0.15f, videoPlayer);

        /*  if(currentOsmosisID != id)                                                                // if condition added 16-08-22
          {
              gameControllerScript.SendData(Constants.TVActiveTherapyOsmosis[osmosisId], -1);
          }*/

       // gameControllerScript.SendData(Constants.TVActiveTherapyOsmosis[osmosisId], -1);               // commented 16-08-22 to check
        gameControllerScript.PlayButtonClickSound();

        currentOsmosisID = id;                              // ADDED 16-08-22
                                                            //currentMachineSelection = machineSelection[(int)GameManager.Instance.CurrentClariaTherapiesState];
                                                            //currentMachineSelection.SetActive(true);

        currentosmosisBtnID = id;                                           //added 16-08-22

    }

    //unused
    public void OnOsmosisToggleClick()
    {
        osmosisGameObject.transform.GetChild(osmosisId).Find("Off").gameObject.SetActive(true);
        osmosisGameObject.transform.GetChild(osmosisId).Find("On").gameObject.SetActive(false);
        osmosisId = osmosisId == 0 ? 1 : 0;
        osmosisGameObject.transform.GetChild(osmosisId).Find("Off").gameObject.SetActive(false);
        osmosisGameObject.transform.GetChild(osmosisId).Find("On").gameObject.SetActive(true);
        OsmosisButtonHandler(osmosisId);
    }

    public void OnReplay()
    {
        //if (currentButtonId == 4)
        //{
        //    gameControllerScript.SendData(Constants.TVActiveTherapyOsmosis[osmosisId], -1);
        //}
        //else {
        //    gameControllerScript.SendData(Constants.TVActiveTherapyGraph[(int)GameManager.Instance.CurrentClariaTherapiesState], -1);
        //}

        switch (currentButtonId)
        {
            case 0:
                gameControllerScript.SendData(Constants.TVActiveTherapyGraph[(int)GameManager.Instance.CurrentClariaTherapiesState], -1);
                break;
            case 1:
                break;
            case 2:
                gameControllerScript.SendData(Constants.TVActiveTherapyCassette[(int)GameManager.Instance.CurrentClariaTherapiesState], -1);
                break;
            case 3:
                gameControllerScript.SendData(Constants.TVActiveTherapyCassette[(int)GameManager.Instance.CurrentClariaTherapiesState], -1);
                break;
            case 4:
                gameControllerScript.SendData(Constants.TVActiveTherapyOsmosis[osmosisId], -1);
                break;
        }

    }

    private void OnVideoEnd(VideoPlayer videoPlayer)
    {
        OnReplay();
    }


    public void SetcurrentosmosisBtnID(int val)             // added 16-08-22
    {
        currentosmosisBtnID = val;
    }

    public void SetOnClickBtn()
    {
        for (int i = 0; i < currentOsmosisBtns.Count; i++)
        {
            currentOsmosisBtns[i].onClick.AddListener(() => SetcurrentosmosisBtnID(-1));
        }
    }

    public void MakeInteractableBtns()                                                              // once Btn is pressed, it should not press again.
    {
        for (int i = 1; i < activeTherapybuttons.Count; i++)
        {

            activeTherapybuttons[i].interactable = true;
        }
    }

}
