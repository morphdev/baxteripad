using Com.Morph.Baxter;
using System;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.XR.ARFoundation;
using DG.Tweening;
using UnityEngine.UI;
using Sfs2X.Entities.Data;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI.Extensions;
using UnityEngine.UI.Extensions.Examples;

public class GameController : MonoBehaviour
{
    public HomeOperation homeOperation;

    #region PublicProperties
    public ARSession arSession;
    public GameObject[] screens;
    //public GameObject clariaModel;
    public GameObject[] clariaConnectivityMenuObjects;
    public GameObject[] clariaHardwareMenuObjects;
    public GameObject[] clariaTherapyMenuObjects;
    public GameObject[] clariaBaxterSubMenuObjects;
    public GameObject[] clariaPatientSubMenuObjects;
    public GameObject[] subMenuScreens;
    public GameObject[] solutionsScreens;
    public GameObject hardwareStateObject;
    public GameObject therapiesStateObject;
    public GameObject alarmScenarioStateObject;
    public GameObject typesOfTherapiesStateObject;
    public OSC tvOSC;   //to send data to the display
    public OSC mannOSC; // to send data to the mannequin
    public OSC leftProjectorOSC;   //to send data to the leftProjector
    public OSC rightProjectorOSC; // to send data to the rightProjector
    public Transform currentHardwareTitle;
    public GameObject worldCamera;
    public GameObject bgCamera;
    public DataHandler dataScript;
    public ScrollPositionController scrollPositionController;
    public GameObject arObject;
    public GameObject interactiveModel;
    public AudioSource audioSource;
    public AudioClip buttonClickSound;
    public Button clariaConnectivity, clariaHardware, clariaTherapy;

    //public VoiceOverManager voiceOverScript;
    #endregion

    #region PrivateProperties
    private GameObject currentScreen;   //reference to the active screen (For ex, Start screen)
    private GameObject[] currentMenuObjects = { };  //reference to the list of gameobjects of the selected sidebar title(For ex, if Connectivity is selected, this array will have ShareSource,Nurse, Patient and Baxter gameObjects)
    private GameObject[] currentSubMenuObjects = { };   //reference to the list of gameobjects of the currently selected sidebar submenu title(For ex, if Baxter is selected, this array will have sotware gameObject)
    private Transform currentActiveSidebar; //reference to the active SideBar title (For ex, Connectivity)
    private Transform currentActiveSubMenuSidebar;  //reference to the active SideBar SubMenu title (For ex, ShareSOurce)
    private Transform currentActiveSubSubMenuSidebar;  //reference to the active SideBar SubMenu' SubMenu title (For ex, Software)
    private GameObject currentSubMenuScreen;    //reference to the active SubMenu screen (For ex, ShareSource screen)
    private GameObject currentClariaSolutionsScreen;    //reference to the active SubMenu screen (For ex, ShareSource screen)
    private int currentSolutionId = 0;
    private GameObject currentBottomBar;
    //private List<GameObject> nurseActions = new List<GameObject>();
    private GameObject currentNurseToolTip;
    private int currentNurseScreenId = 0;
    private int currentPatientScreenId = -1;
    private GameObject currentPatientContent;
    private Coroutine uiRoutine;
    private GameObject currentHelpScreen;
    private GameObject helpButton;
    #endregion

    #region UnityCallbacks
    // Start is called before the first frame update
    void Start()
    {
        StartGame();
      //  Invoke("OnClariaProceedButtonClick", 0.1f);                            //added 31-03-22
    }

    public void SendMedidateScene()
    {
        MAppData gameData = new MAppData
        {
            screenId = "MEDICATESCENELOAD",
            videoid = "",
            parameter = ""
        };
        dataScript.SendData(gameData.SaveToString(), tvOSC);
        Invoke("secondsceneload",1f);
    }
   void secondsceneload()
    {
        Application.LoadLevel(1);
    }
    private void OnEnable()
    {
        try
        {
            scrollPositionController.OnItemSelected.AddListener(CellSelected);
        }catch(Exception exa) { }
    }
    #endregion

    #region ScreenHandlers
    private void StartGame() {
        SendData(0);                          // commented 31-03-22
        SwitchEnvironment(TheEnvironement.NONE);
        SendOSCMessage(mannOSC, Constants.MannSelectAddress);
        SendOSCMessage(mannOSC, Constants.Mann1SelectAddress);
        SendOSCMessage(mannOSC, Constants.Mann2SelectAddress);
        SendOSCMessage(mannOSC, Constants.MaskSelectAddress);
        SendOSCMessage(mannOSC, Constants.Mask1SelectAddress);
        SendOSCMessage(mannOSC, Constants.Mask2SelectAddress);
        tvOSC.SetAddressHandler("/Data", OnOSCMessageReceived);
        if(Application.loadedLevel==0)
        SwitchScreen(GameScreen.Start);
        if (Application.loadedLevel == 1)
        {

            Invoke("OnClariaProceedButtonClick", 0.1f);
            Invoke("ActivateCamera", 0.1f);
            #region commented to check
            /*arSession.enabled = false;
            arObject.SetActive(false);
            //PlayButtonClickSound();
            //SwitchScreen(GameScreen.Scan);
          //  OnClariaProceedButtonClick();                             // commented
            arSession.gameObject.SetActive(true);
            arSession.enabled = true;
            arObject.SetActive(true);

            bgCamera.SetActive(false);
            arObject.SetActive(true);
            Invoke("ActivateCamera", 0.1f);
            //Invoke("ActivateCamera", 2f);

            GameObject.Find("ClariaLanding").SetActive(false);
            //  GameObject.Find("ClariaHome").SetActive(false);  */                             // commented     30-03-22
            #endregion

        }
    }
    void ActivateCamera()
    {

        CameraHandler(false);
    }
    #endregion
    #region ButtonClicks
        [ContextMenu("Camera Scan Success")]

    public void OnStartButtonClick() {
        arObject.SetActive(false);
        arSession.enabled = false;
        //PlayButtonClickSound();
        SwitchScreen(GameScreen.ScanSuccess);
        //OnScanSuccess(); // Commented Manish 
            //if (Application.isEditor)
            //{
            //    //OnScanSuccess();
            //    Invoke("OnScanSuccess", Constants.DelayDuration);
            //}
        }
        public void OnStartLoadScene2()
    {
        //arObject.SetActive(true);
        //arSession.enabled = true;
        PlayButtonClickSound();
        Application.LoadLevel(2);
        //SwitchScreen(GameScreen.Scan);
        //if (Application.isEditor)
        //{
        //    //OnScanSuccess();
        //    Invoke("OnScanSuccess", Constants.DelayDuration);
        //}
    }

    public void OnClariaProceedButtonClick() {
      //  PlayButtonClickSound();                   //COMMENTED

        //arObject.SetActive(true);
        //arSession.enabled = true;

        CameraHandler(false);            
        SwitchScreen(GameScreen.ClariaHome);                     //1
        SwitchEnvironment(TheEnvironement.Home);


        //clariaModel.SetActive(true);
        //Debug.Log("clariaModel activated");
        //Debug.Log("clariaModel position: "+clariaModel.transform.position);
        //Debug.Log("clariaModel scale: " + clariaModel.transform.localScale);

        SwitchScreen(SubMenuScreen.ClariaLanding);
        SendData(Constants.TVClariaLanding);
        
    }

    public void OnClariaCloseButtonClick() {
        OnStartButtonClick();
    }

    /// <summary>
    /// OnClariaConnectivityClick
    /// </summary>
    /// <param name="clickedTransform">ClickedTransform</param>
    public void OnClariaConnectivityClick(Transform clickedTransform) {
        CameraHandler(false);
        //clariaModel.SetActive(true);
        //DisableThisButton(clariaConnectivity);

        ResetHardware(null, ClariaHardwareState.None);
        if (GameManager.Instance.CurrentClariaState == ClariaState.Connectivity) {
            ResetSidebarMenu();
            PlayButtonClickSound();
            //OnClariaHomeClick();
            return;
        }
        ResetSidebarMenu();
        GameManager.Instance.CurrentClariaState = ClariaState.Connectivity;
        //ClariaSidebarHandler(ClariaState.Connectivity);
        foreach (GameObject g in clariaConnectivityMenuObjects)
            g.SetActive(true);
        currentMenuObjects = clariaConnectivityMenuObjects;
        ResetSideBar(clickedTransform);
        if(currentBottomBar)
            currentBottomBar.SetActive(false);
        SwitchEnvironment(TheEnvironement.Home);
        OnClariaConnectivityShareSourceClick(clickedTransform.parent.GetChild(1));

        //SendOSCMessage(mannOSC, Constants.MannTherapyAddress);
        SendOSCMessage(mannOSC, Constants.RestartExperience);
    }

    void DisableThisButton(Button btnToDisable)
    {
        clariaConnectivity.enabled = true;
        clariaHardware.enabled = true;
        clariaTherapy.enabled = true;

        btnToDisable.enabled = false;
    }

    public void OnClariaHardwareClick(Transform clickedTransform)
    {
        CameraHandler(false);
        //clariaModel.SetActive(true);
            ResetHardware(null, ClariaHardwareState.None);

        //DisableThisButton(clariaHardware);
        if (GameManager.Instance.CurrentClariaState == ClariaState.Hardware)
        {
            PlayButtonClickSound();
            ResetSidebarMenu();
            //OnClariaHomeClick();
            return;
        }
        ResetSidebarMenu();
        OnHardwareBlockClick(hardwareStateObject.transform.GetChild(0));
        GameManager.Instance.CurrentClariaState = ClariaState.Hardware;
        //ClariaSidebarHandler(ClariaState.Hardware);
        foreach (GameObject g in clariaHardwareMenuObjects)
            g.SetActive(true);
        currentMenuObjects = clariaHardwareMenuObjects;
        ResetSideBar(clickedTransform);

        if (currentBottomBar)
            currentBottomBar.SetActive(false);

        hardwareStateObject.SetActive(true);
        currentBottomBar = hardwareStateObject;
        SwitchEnvironment(TheEnvironement.Home);
        OnClariaHardwareClariaClick(clickedTransform.parent.GetChild(7));

        //SendOSCMessage(mannOSC, Constants.MannTherapyAddress);
        SendOSCMessage(mannOSC, Constants.RestartExperience);
    }

    public void OnClariaTherapyClick(Transform clickedTransform)
    {
        CameraHandler(false);
        //clariaModel.SetActive(false);
        //DisableThisButton(clariaTherapy);

        PlayButtonClickSound();
        ResetHardware(null, ClariaHardwareState.None);
        if (GameManager.Instance.CurrentClariaState == ClariaState.Therapy)
        {
            ResetSidebarMenu();
            //OnClariaHomeClick();
            return;
        }
        ResetSidebarMenu();
        GameManager.Instance.CurrentClariaState = ClariaState.Therapy;
        //ClariaSidebarHandler(ClariaState.Therapy);
        foreach (GameObject g in clariaTherapyMenuObjects)
            g.SetActive(true);
        currentMenuObjects = clariaTherapyMenuObjects;
        ResetSideBar(clickedTransform);
        if (currentBottomBar)
            currentBottomBar.SetActive(false);
        SwitchEnvironment(TheEnvironement.OperationTheatre);
        //OnClariaTherapyPatientSystemPrepClick(clickedTransform.parent.GetChild(12));
        SendOSCMessage(mannOSC, Constants.MannTherapyAddress);

        SendData(Constants.TVTherapyHome);

        SwitchScreen(SubMenuScreen.TherapyHome);
    }

    public void OnClariaConnectivityShareSourceClick(Transform clickedTransform) {
        if (GameManager.Instance.CurrentClariaSubState == ClariaSubState.Patient)
            ResetSubMenu();
        GameManager.Instance.CurrentClariaSubState = ClariaSubState.None;
        //play sharesource animation in the model
        SwitchScreen(SubMenuScreen.ShareSource);    //switch to ShareSource screen
        ResetSubMenuSideBar(clickedTransform);  //activate/deactivate sidebar selection

        PlayButtonClickSound();
        OnShareSourceArchitectureClick(null);
        //clariaModel.SetActive(false);
    }

    public void OnClariaConnectivityNurseClick(Transform clickedTransform)
    {
        if (GameManager.Instance.CurrentClariaSubState == ClariaSubState.Patient)
            ResetSubMenu();
        GameManager.Instance.CurrentClariaSubState = ClariaSubState.None;
        //play nurse animation in the model
        SwitchScreen(SubMenuScreen.Nurse);
        ResetSubMenuSideBar(clickedTransform);

        OnNurseSubMenuClick(0); //resets everything and activates the dashboard screen

        PlayButtonClickSound();
        SendData(Constants.TVConnectivityNurseId, -1, 0);
        //clariaModel.SetActive(false);
    }

    public void OnClariaConnectivityPatientClick(Transform clickedTransform)
    {
        //clariaModel.SetActive(false);
        if (GameManager.Instance.CurrentClariaSubState == ClariaSubState.Patient) {
            ResetSubMenu();
            ResetSubMenuSideBar(null);
            PlayButtonClickSound();
            return;
        }

        GameManager.Instance.CurrentClariaSubState = ClariaSubState.Patient;
        foreach (GameObject g in clariaPatientSubMenuObjects)
            g.SetActive(true);
        currentSubMenuObjects = clariaPatientSubMenuObjects;

        SwitchScreen(SubMenuScreen.Patient);
        ResetSubMenuSideBar(clickedTransform);

        OnClariaConnectivityPatientConfirmProgramConfigClick(clickedTransform.parent.GetChild(4));

        //currentPatientScreenId = 6; //first content - (Enter activation code)
        //PatientUIHandler(currentPatientScreenId, 3);

    }

    public void OnClariaConnectivityPatientConfirmProgramConfigClick(Transform clickedTransform) {
        SwitchScreen(SubMenuScreen.Patient);
        ResetSubSubMenuSideBar(clickedTransform);
        currentPatientScreenId = 6; //first content - (Enter activation code)
        PatientUIHandler(currentPatientScreenId, 3);
        PlayButtonClickSound();
    }

    public void OnClariaConnectivityPatientOrderSuppliesClick(Transform clickedTransform)
    {
        SwitchScreen(SubMenuScreen.PatientOrderSupplies);
        foreach (Transform t in currentSubMenuScreen.transform) {
            t.gameObject.SetActive(false);
        }
        currentSubMenuScreen.transform.GetChild(0).gameObject.SetActive(true);
        ResetSubSubMenuSideBar(clickedTransform);
        SendData(Constants.TVConnectivityPatientOrderSuppliesId, 0);
        PlayButtonClickSound();
    }

    //Unused (Removed as requested by client)
    public void OnClariaConnectivityBaxterClick(Transform clickedTransform)
    {
        //clariaModel.SetActive(false);
        if (GameManager.Instance.CurrentClariaSubState == ClariaSubState.Baxter) {
            ResetSubMenu();
            ResetSubMenuSideBar(null);
            return;
        }

        GameManager.Instance.CurrentClariaSubState = ClariaSubState.Baxter;
        foreach (GameObject g in clariaBaxterSubMenuObjects)
            g.SetActive(true);
        currentSubMenuObjects = clariaBaxterSubMenuObjects;

        //play baxter animation in the model
        SwitchScreen(SubMenuScreen.Baxter);
        ResetSubMenuSideBar(clickedTransform);
        SendData(Constants.TVConnectivityBaxterId);
    }

    //Unused (Removed as requested by client)
    public void OnClariaConnectivitySoftwareClick(Transform clickedTransform)
    {
        //play software animation in the model
        SwitchScreen(SubMenuScreen.SoftwareUpdate);
        ResetSubSubMenuSideBar(clickedTransform);
        //clariaModel.SetActive(true);
    }

    public void OnClariaHardwareHeatPadClick(Transform clickedTransform) {
        CameraHandler(false);
        //OnHardwareBlockClick(hardwareStateObject.transform.GetChild(0));
        //play heatpad animation in the model
        SwitchScreen(SubMenuScreen.HeatPad);
        ResetSubMenuSideBar(clickedTransform);
        ResetHardware(null, ClariaHardwareState.None);
        SendData(Constants.TVHardwareHeatPadId);
        PlayButtonClickSound();

        //clariaModel.SetActive(false);
        hardwareStateObject.SetActive(false);
        currentBottomBar = hardwareStateObject;
        //OnHardwareBlockClick(hardwareStateObject.transform.GetChild(0));
    }

    public void OnClariaHardwareSolutionsClick(Transform clickedTransform)
    {
        PlayButtonClickSound();
        CameraHandler(false);
        //play solutions animation in the model
        ResetSubMenuSideBar(clickedTransform);
        SwitchScreen(SubMenuScreen.Solutions);
        SolutionsHandler();
        ResetHardware(null, ClariaHardwareState.None);
        //clariaModel.SetActive(false);
        if (currentBottomBar)
            currentBottomBar.SetActive(false);
    }

    public void OnClariaHardwareCassetteClick(Transform clickedTransform)
    {
        PlayButtonClickSound();
        CameraHandler(false);
        OnHardwareBlockClick(hardwareStateObject.transform.GetChild(0));
        //play cassette animation in the model
        ResetSubMenuSideBar(clickedTransform);
        SwitchScreen(SubMenuScreen.Cassette);
        ResetHardware(null, ClariaHardwareState.None);
        SendData(Constants.TVHardwareCasetteId);
        //clariaModel.SetActive(false);
        hardwareStateObject.SetActive(false);
        currentBottomBar = hardwareStateObject;
    }

    public void OnClariaHardwareClariaClick(Transform clickedTransform)
    {
        //PlayButtonClickSound();
        OnHardwareBlockClick(hardwareStateObject.transform.GetChild(0));
        //play cassette animation in the model
        ResetSubMenuSideBar(clickedTransform);
        SwitchScreen(SubMenuScreen.ClariaBlock);
        SendData(Constants.TVHardwareClariaBlockId);
        //clariaModel.SetActive(false);
        hardwareStateObject.SetActive(true);
        currentBottomBar = hardwareStateObject;

        CameraHandler(true);

    }

    public void OnClariaTherapyPatientSystemPrepClick(Transform clickedTransform)   //renamed to PreTherapy
    { //renamed to PreTherapy
        //play patientsystemprep animation in the model
        PlayButtonClickSound();
        ResetSubMenuSideBar(clickedTransform);
        SwitchScreen(SubMenuScreen.PreTherapy);
        SendOSCMessage(mannOSC, Constants.MannTherapyPatientSystemPrepAddress);
        //clariaModel.SetActive(false);
        if (currentBottomBar)
            currentBottomBar.SetActive(false);
        SwitchEnvironment(TheEnvironement.OperationTheatre);
    }

    public void OnClariaTherapyTherapiesClick(Transform clickedTransform)   //renamed to ActiveTherapy
    {
        //play therapies animation in the model
        ResetSubMenuSideBar(clickedTransform);
        SwitchScreen(SubMenuScreen.ActiveTherapy);
        //clariaModel.SetActive(false);
        SetSideBarActive(currentHardwareTitle, false);
        //GameManager.Instance.CurrentClariaTherapiesState = ClariaTherapiesState.None;

        if (currentBottomBar)
            currentBottomBar.SetActive(false);
        //therapiesStateObject.SetActive(true);
        //currentBottomBar = therapiesStateObject;

        //OnTherapiesDrainClick(clickedTransform.parent.parent.GetChild(3).GetChild(0));
        SwitchEnvironment(TheEnvironement.OperationTheatre);
    }

    public void OnClariaTypesOfTherapiesClick(Transform clickedTransform) {
        ResetSubMenuSideBar(clickedTransform);
        SwitchScreen(SubMenuScreen.None);
        //clariaModel.SetActive(false);
        SetSideBarActive(currentHardwareTitle, false);
        GameManager.Instance.CurrentClariaTypesOfTherapiesState = ClariaTypesOfTherapiesState.None;

        if (currentBottomBar)
            currentBottomBar.SetActive(false);
        typesOfTherapiesStateObject.SetActive(true);
        currentBottomBar = typesOfTherapiesStateObject;

        //SendOSCMessage(mannOSC, Constants.MannTherapyTherapyDefaultAddress);
        SendOSCMessage(mannOSC, Constants.RestartExperience);

        OnTypesOfTherapiesNormalClick(clickedTransform.parent.parent.GetChild(5).GetChild(0));
        SwitchEnvironment(TheEnvironement.OperationTheatre);
    }

    public void OnClariaTherapyAlarmScenarioClick(Transform clickedTransform)
    {
        //play alarmscenario animation in the model
        ResetSubMenuSideBar(clickedTransform);
        //SwitchScreen(SubMenuScreen.None);
        //clariaModel.SetActive(false);
        SetSideBarActive(currentHardwareTitle, false);
        //GameManager.Instance.CurrentClariaAlarmScenarioState = ClariaAlarmScenarioState.Occlusion;
        //SendData(Constants.TVAlarmScenario);

        //SendOSCMessage(mannOSC, Constants.MannTherapyTherapyDefaultAddress);
        SendOSCMessage(mannOSC, Constants.RestartExperience);

        if (currentBottomBar)
            currentBottomBar.SetActive(false);
        alarmScenarioStateObject.SetActive(true);
        currentBottomBar = alarmScenarioStateObject;
        OnAlarmScenarioOcclusionClick(clickedTransform.parent.parent.GetChild(4).GetChild(0));
        SwitchEnvironment(TheEnvironement.OperationTheatre);
    }

    public void OnClariaTherapyPostTherapyClick(Transform clickedTransform)
    {
        PlayButtonClickSound();
        //play posttherapy animation in the model
        ResetSubMenuSideBar(clickedTransform);
        SwitchScreen(SubMenuScreen.PostTherapy);
        SendOSCMessage(mannOSC, Constants.MannTherapyTherapyDefaultAddress);
        //clariaModel.SetActive(false);
        if (currentBottomBar)
            currentBottomBar.SetActive(false);
        SwitchEnvironment(TheEnvironement.Home);
    }

    public void OnHardwareBlockClick(Transform clickedTransform)
    {
        if (GameManager.Instance.CurrentClariaHardwareState == ClariaHardwareState.Block)
            return;
        ResetHardware(clickedTransform, ClariaHardwareState.Block);
        switch (GameManager.Instance.CurrentSubMenuScreen) {
            case SubMenuScreen.None:    //hardware
            case SubMenuScreen.HardwareExploaded:
            case SubMenuScreen.HardwareWireframe:
                PlayButtonClickSound();
                SwitchScreen(SubMenuScreen.None);
                SendData(Constants.TVHardwareId);
                //clariaModel.SetActive(true);
                return;
            case SubMenuScreen.HeatPad:
            case SubMenuScreen.HeatPadExploaded:
            case SubMenuScreen.HeatPadWireframe:
                PlayButtonClickSound();
                SwitchScreen(SubMenuScreen.HeatPad);
                SendData(Constants.TVHardwareHeatPadId);
                //clariaModel.SetActive(false);
                return;
            case SubMenuScreen.Cassette:
            case SubMenuScreen.CassetteExploaded:
            case SubMenuScreen.CassetteWireframe:
                PlayButtonClickSound();
                SwitchScreen(SubMenuScreen.Cassette);
                SendData(Constants.TVHardwareCasetteId);
                //clariaModel.SetActive(false);
                return;
            case SubMenuScreen.ClariaExploded:
                PlayButtonClickSound();
                SwitchScreen(SubMenuScreen.ClariaBlock);
                SendData(Constants.TVHardwareClariaBlockId);
                CameraHandler(true);
                //clariaModel.SetActive(false);
                return;
            default:
                return;
        }
    }

    //Unused
    public void OnHardwareWireframeClick(Transform clickedTransform)
    {
        if (GameManager.Instance.CurrentClariaHardwareState == ClariaHardwareState.Wireframe)
            return;
        ResetHardware(clickedTransform, ClariaHardwareState.Wireframe);
        switch (GameManager.Instance.CurrentSubMenuScreen)
        {
            case SubMenuScreen.None:    //hardware
            case SubMenuScreen.HardwareExploaded:
                SwitchScreen(SubMenuScreen.HardwareWireframe);
                SendData(Constants.TVHardwareWireframeId);
                //clariaModel.SetActive(false);
                return;
            case SubMenuScreen.HeatPad:
            case SubMenuScreen.HeatPadExploaded:
                SwitchScreen(SubMenuScreen.HeatPadWireframe);
                SendData(Constants.TVHardwareHeatPadWireframeId);
                //clariaModel.SetActive(false);
                return;
            case SubMenuScreen.Cassette:
            case SubMenuScreen.CassetteExploaded:
                SwitchScreen(SubMenuScreen.CassetteWireframe);
                SendData(Constants.TVHardwareCasetteWireframeId);
                //clariaModel.SetActive(false);
                return;
            default:
                return;
        }
    }

    //Unused
    public void OnHardwareExplodedClick(Transform clickedTransform) {
        if (GameManager.Instance.CurrentClariaHardwareState == ClariaHardwareState.Exploded)
            return;
        ResetHardware(clickedTransform, ClariaHardwareState.Exploded);
        switch (GameManager.Instance.CurrentSubMenuScreen)
        {
            case SubMenuScreen.None:    //hardware
            case SubMenuScreen.HardwareWireframe:
                PlayButtonClickSound();
                SwitchScreen(SubMenuScreen.HardwareExploaded);
                SendData(Constants.TVHardwareExploadedId);
                //clariaModel.SetActive(false);
                return;
            case SubMenuScreen.HeatPad:
            case SubMenuScreen.HeatPadWireframe:
                PlayButtonClickSound();
                SwitchScreen(SubMenuScreen.HeatPadExploaded);
                SendData(Constants.TVHardwareHeatPadExploadedId);
                //clariaModel.SetActive(false);
                return;
            case SubMenuScreen.Cassette:
            case SubMenuScreen.CassetteWireframe:
                PlayButtonClickSound();
                SwitchScreen(SubMenuScreen.CassetteExploaded);
                SendData(Constants.TVHardwareCasetteExploadedId);
                //clariaModel.SetActive(false);
                return;
            case SubMenuScreen.ClariaBlock:
                PlayButtonClickSound();
                CameraHandler(false);
                SwitchScreen(SubMenuScreen.ClariaExploded);
                SendData(Constants.TVHardwareClariaExplodedId);
                //clariaModel.SetActive(true);
                //play exploded animation
                return;
            default:
                return;
        }
    }

    public void OnHardwareInteractiveClick(Transform clickedTransform) {
        if (GameManager.Instance.CurrentClariaHardwareState == ClariaHardwareState.Interactive)
            return;
        PlayButtonClickSound();
        ResetHardware(clickedTransform, ClariaHardwareState.Interactive);
        //switch to interactive screen
        //activate exploded model
        SwitchScreen(SubMenuScreen.None);
        interactiveModel.SetActive(true);
    }

    //unused
    public void OnTherapiesDrainClick(Transform clickedTransform) {
        if (GameManager.Instance.CurrentClariaTherapiesState == ClariaTherapiesState.Drain)
            return;
        ResetTherapies(clickedTransform, ClariaTherapiesState.Drain);

        //ResetSubMenuSideBar(clickedTransform);
        SwitchScreen(SubMenuScreen.TherapiesDrain);
        //clariaModel.SetActive(false);
        TherapiesHandler(0);
    }

    //unused
    public void OnTherapiesFillClick(Transform clickedTransform)
    {
        if (GameManager.Instance.CurrentClariaTherapiesState == ClariaTherapiesState.Fill)
            return;
        ResetTherapies(clickedTransform, ClariaTherapiesState.Fill);
        //ResetSubMenuSideBar(clickedTransform);
        SwitchScreen(SubMenuScreen.TherapiesFill);
        //clariaModel.SetActive(false);
        TherapiesHandler(1);

    }

    //used
    public void OnTherapiesDwellClick(Transform clickedTransform)
    {
        if (GameManager.Instance.CurrentClariaTherapiesState == ClariaTherapiesState.Dwell)
            return;
        ResetTherapies(clickedTransform, ClariaTherapiesState.Dwell);
        //ResetSubMenuSideBar(clickedTransform);
        SwitchScreen(SubMenuScreen.TherapiesDwell);
        //clariaModel.SetActive(false);
        TherapiesHandler(2);
    }


    public void OnSolutionsArrowClick(bool isNext) {
        Debug.Log("OnSolutionsArrowClick");
        PlayButtonClickSound();

        currentSolutionId = isNext ? (currentSolutionId + 1):(currentSolutionId - 1);
        currentSolutionId = GameManager.Mod(currentSolutionId, solutionsScreens.Length);
        SolutionsHandler();
    }

    public void OnAlarmScenarioOcclusionClick(Transform clickedTransform) {
        if (GameManager.Instance.CurrentClariaAlarmScenarioState == ClariaAlarmScenarioState.Occlusion)
            return;
        ResetAlarmScenario(clickedTransform, ClariaAlarmScenarioState.Occlusion);

        //Debug.Log("Hello");
        PlayButtonClickSound();

        SwitchScreen(SubMenuScreen.AlarmScenarioOcclusion);

        //SendData(Constants.TVEmpty);
        SendData(Constants.TVAlarmScenarioOcclusion, 0);
        SendOSCMessage(mannOSC, Constants.MannTherapyAddress);

    }

    public void OnAlarmScenarioLowDrainClick(Transform clickedTransform)
    {
        if (GameManager.Instance.CurrentClariaAlarmScenarioState == ClariaAlarmScenarioState.LowDrain)
            return;
        ResetAlarmScenario(clickedTransform, ClariaAlarmScenarioState.LowDrain);

        PlayButtonClickSound();
        SwitchScreen(SubMenuScreen.AlarmScenarioLowDrain);
        //SendData(Constants.TVEmpty);
        SendData(Constants.TVAlarmScenarioLowDrain, 0);
        SendOSCMessage(mannOSC, Constants.MannTherapyAddress);

    }

    public void OnTypesOfTherapiesNormalClick(Transform clickedTransform)
    {
        SendOSCMessage(mannOSC, Constants.MannTherapyAddress);
        SendData(Constants.TVTherapyTypeOfTherapiesNormalId);
        if (GameManager.Instance.CurrentClariaTypesOfTherapiesState == ClariaTypesOfTherapiesState.Normal)
            return;
        ResetTypesOfTherapies(clickedTransform, ClariaTypesOfTherapiesState.Normal);

        PlayButtonClickSound();
        SwitchScreen(SubMenuScreen.TypeNormal);
    }

    public void OnTypesOfTherapiesTidalClick(Transform clickedTransform)
    {
        SendOSCMessage(mannOSC, Constants.MannTherapyAddress);
        SendData(Constants.TVTherapyTypeOfTherapiesTidalId);
        if (GameManager.Instance.CurrentClariaTypesOfTherapiesState == ClariaTypesOfTherapiesState.Tidal)
            return;
        ResetTypesOfTherapies(clickedTransform, ClariaTypesOfTherapiesState.Tidal);

        PlayButtonClickSound();
        SwitchScreen(SubMenuScreen.TypeTidal);
    }

    public void OnNurseDashobardButtonClick(Transform clickedTransform) {
        //get child(1) and activate it
        //store it in an array
        //clickedTransform.GetChild(0).gameObject.SetActive(false);
        //clickedTransform.GetChild(1).gameObject.SetActive(true);

        if (currentNurseToolTip)
            currentNurseToolTip.SetActive(false);
        currentNurseToolTip = clickedTransform.GetChild(2).gameObject;
        currentNurseToolTip.SetActive(true);

        PlayButtonClickSound();
        SendData(Constants.TVConnectivityNurseId, Convert.ToInt32(clickedTransform.name), 0);
    }

    public void OnNurseActionButtonClick(Transform clickedTransform)
    {
        if (currentNurseToolTip)
            currentNurseToolTip.SetActive(false);
        currentNurseToolTip = clickedTransform.GetChild(0).gameObject;
        currentNurseToolTip.SetActive(true);

        PlayButtonClickSound();
        SendData(Constants.TVConnectivityNurseId, Convert.ToInt32(clickedTransform.name), 0, true);
    }


    public void OnNurseSubMenuClick(int clickedId) {
        foreach (Transform t in currentSubMenuScreen.transform)
        {
            t.gameObject.SetActive(false);
        }
        if (currentNurseToolTip)
            currentNurseToolTip.SetActive(false);
        currentSubMenuScreen.transform.GetChild(clickedId).gameObject.SetActive(true);
        currentNurseScreenId = clickedId;
        PlayButtonClickSound();
        SendData(Constants.TVConnectivityNurseId, -1, currentNurseScreenId);
    }

    public void OnPatientUIButtonClick(bool autoState) {
        /*
        switch (GameManager.Instance.CurrentClariaPatientState) {
            case ClariaPatientState.EnterActivation:
                GameManager.Instance.CurrentClariaPatientState = ClariaPatientState.ConfirmConfigurations;
                PatientUIHandler((int)GameManager.Instance.CurrentClariaPatientState, 3);
                return;
            case ClariaPatientState.ConfirmConfigurations:
                GameManager.Instance.CurrentClariaPatientState = ClariaPatientState.AreYouMarySmith;
                PatientUIHandler((int)GameManager.Instance.CurrentClariaPatientState, 3);
                return;
            case ClariaPatientState.AreYouMarySmith:
                GameManager.Instance.CurrentClariaPatientState = ClariaPatientState.ShareClinic;
                PatientUIHandler((int)GameManager.Instance.CurrentClariaPatientState, 3);
                return;
            case ClariaPatientState.ShareClinic:
                GameManager.Instance.CurrentClariaPatientState = ClariaPatientState.NewProgram;
                PatientUIHandler((int)GameManager.Instance.CurrentClariaPatientState, 3);
                return;
            case ClariaPatientState.NewProgram:
                GameManager.Instance.CurrentClariaPatientState = ClariaPatientState.IsProgramCorrect;
                PatientUIHandler((int)GameManager.Instance.CurrentClariaPatientState, 3);
                return;
            case ClariaPatientState.IsProgramCorrect:
                GameManager.Instance.CurrentClariaPatientState = ClariaPatientState.ProgramAccepted;
                PatientUIHandler((int)GameManager.Instance.CurrentClariaPatientState, 3);
                return;
            case ClariaPatientState.ProgramAccepted:
                GameManager.Instance.CurrentClariaPatientState = ClariaPatientState.PressGoToStart;
                PatientUIHandler((int)GameManager.Instance.CurrentClariaPatientState, 3);
                return;
            default:
                currentSubMenuScreen.GetComponent<ClariaPatientHandler>().enabled = true;
                return;
        }
        */

        int buttonId;
        currentPatientScreenId++;
        if(!autoState)
            PlayButtonClickSound();
        switch (currentPatientScreenId) {
            case 7: //code 207 7842 00
            case 8: //code 207 7842 01
            case 9: // code 207 7842 02
            case 11: // code 207 7842 030
            case 12: // code 207 7842 031
            case 13: // code 207 7842 032
                buttonId = 4;
                break;
            case 10:    //code 207 7842 030
            case 14:    //code 207 7842 033
            case 18:    //confirm configurations
            case 21:    //review program
                buttonId = 3;
                break;
            case 19:    //Are you Brandon?
            case 20:    //Share clinic and Baxter
            case 29:    //is program correct
                buttonId = 1;
                break;
            case 22:    //therapy
            case 23:    //total vol
            case 24:    //therapy time
            case 25:    //fill vol
            case 26:    //last fill vol
            case 27:    //dextrose
            case 28:    //mode
                buttonId = 5;
                break;
            default:    //14
                buttonId = -1;
                break;
        }

        PatientUIHandler(currentPatientScreenId, buttonId);


        /*
        currentPatientScreenId++;
        float duration;
        int buttonId;
        switch (currentPatientScreenId) {
            case 6: //confirm configurations
                duration = 9f;  //12
                buttonId = 3;
                break;
            case 7: //15    //are you jane jones
                duration = 3f;
                buttonId = 2;
                break;
            case 8: //22    //id rejected
                duration = 7f;
                buttonId = 2;
                break;
            case 9: //30     //enter activation code
                duration = 8f;
                buttonId = 3;
                break;
            case 10: //42    //enter activation code
                duration = 12f;
                buttonId = 3;
                break;
            case 11://54 are you mary smith
                duration = 12f;
                buttonId = 3;
                break;
            case 12://62 Share
                duration = 8f;
                buttonId = 1;
                break;
            case 13://79 New program received
                duration = 15f;
                buttonId = 3;
                break;
            case 14://83 Therapy
                duration = 4f;
                buttonId = 4;
                break;
            case 15:    //87 //total vol
                duration = 4f;
                buttonId = 3;
                break;
            case 16:    //90 //therapy time
                duration = 3f;
                buttonId = 4;
                break;
            case 17:    //99 //fill vol
                duration = 9f;
                buttonId = 3;
                break;
            case 18:    //104 //last fill vol
                duration = 5f;
                buttonId = 3;
                break;
            case 19:    //107 //dextrose
                duration = 3f;
                buttonId = 3;
                break;
            case 20:    //111 //min drain vol
                duration = 4f;
                buttonId = 3;
                break;
            case 21:    //115 //standard
                duration = 4f;
                buttonId = 3;
                break;
            case 22:    //118 //low fill
                duration = 3f;
                buttonId = 3;
                break;
            case 23:    //122 //neg uf limit
                duration = 4f;
                buttonId = 3;
                break;
            case 24:    //124 //pos uf limit
                duration = 2f;
                buttonId = 3;
                break;
            case 25:    //132 //accepted
                duration = 8f;
                buttonId = 3;
                break;
            case 26:    //147 //weight
                duration = 15f;
                buttonId = 3;
                break;
            case 27:    //157 //weight 70
                duration = 10f;
                buttonId = 2;
                break;
            case 28:    //159 //blood pres
                duration = 2f;
                buttonId = 3;
                break;
            case 29:    //161 //press go to start
                duration = 19f;
                buttonId = 3;
                break;
            default:
                duration = 3f;
                buttonId = 1;
                break;
        }
        uiRoutine = StartCoroutine(UIHandler(duration, currentPatientScreenId, buttonId));
        SendData(Constants.TVConnectivityPatientId, duration, false);
        */
    }

    //unused
    public void OnPatientActionCodeActivationButtonClick() {
        SendData(Constants.TVConnectivityPatientCodeActivationId);
    }

    //unused
    public void OnPatientActionAcceptTherapyButtonClick()
    {
        SendData(Constants.TVConnectivityPatientAcceptTherapyId);
    }

    //unused
    public void OnPatientActionSoftwareUpdateButtonClick()
    {
        SendData(Constants.TVConnectivityPatientSoftwareUpdateId);
    }

    //unused
    public void OnPatientActionOrderSuppliesButtonClick()
    {
        SendData(Constants.TVConnectivityPatientOrderSuppliesId);
    }

    public void OnRestartButtonClick() {

        Invoke("RestartBtnDelayer", 0.2f);
        /*PlayButtonClickSound();
        GameManager.Instance.OnReset();
        SendData(0);

        SendOSCMessage(mannOSC, Constants.RestartExperience);

        SceneManager.LoadScene(0);*/
    }

    public void RestartBtnDelayer()                                                         // added 01-04-22
    {
        PlayButtonClickSound();
        GameManager.Instance.OnReset();
        SendData(0);

        SendOSCMessage(mannOSC, Constants.RestartExperience);

        SceneManager.LoadScene(0);
    }


    public void OnHomeBtnClickedForPD()                                                   // home button click
    {

       // Invoke("HomeBtnClickDelayer", 0.05f);

        PlayButtonClickSound();
        GameManager.Instance.OnReset();
        SendData(0);                              // commented 01-04-22

        SendOSCMessage(mannOSC, Constants.RestartExperience);
       // Application.LoadLevel(2);                               // added from onclariahomeclick

       // SceneManager.LoadScene(0);
    }

    public void HomeBtnClickDelayer()
    {
        PlayButtonClickSound();
        GameManager.Instance.OnReset();
        SendData(0);                              // commented 01-04-22

        SendOSCMessage(mannOSC, Constants.RestartExperience);

        Application.LoadLevel(2);                               // added from onclariahomeclick
    }


    public void OnShareSourceFeatureClick(int id) {
        foreach (Transform t in currentSubMenuScreen.transform.GetChild(0)) {
            t.GetComponent<Animator>().enabled = false;
            t.GetComponent<Image>().color = new Color(1f, 1f, 1f, 0f);
        }
        currentSubMenuScreen.transform.GetChild(0).GetChild(id).GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
        SendData(Constants.TVConnectivityShareScreenId, id);
        PlayButtonClickSound();
    }

    public void OnShareSourceArchitectureClick(GameObject offObject) {
        //activate button
        //show ui
        //send data
        PlayButtonClickSound();
        bool activate = offObject==null?false:offObject.activeInHierarchy;  //architecture
        currentSubMenuScreen.transform.GetChild(0).gameObject.SetActive(!activate); //base image
        currentSubMenuScreen.transform.GetChild(2).gameObject.SetActive(activate);  //architecture
        currentSubMenuScreen.transform.GetChild(3).GetChild(0).gameObject.SetActive(activate);  //on button
        currentSubMenuScreen.transform.GetChild(3).GetChild(1).gameObject.SetActive(!activate); //off button
        if (!activate) {
            foreach (Transform t in currentSubMenuScreen.transform.GetChild(0))
            {
                t.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
                t.GetComponent<Animator>().enabled = true;
            }
        }
        SendData(Constants.TVConnectivityShareScreenId, activate? 4:-1);
    }

    public void OnHelpClick(GameObject helpObject) {
        PlayButtonClickSound();
        if (helpObject == null) {
            HelpHandler(false);
            helpButton = null;
            return;
        }
        helpButton = helpObject;
        HelpHandler(helpObject.transform.GetChild(1).gameObject.activeInHierarchy);
    }

    public void OnClariaHomeClick() {
        if (GameManager.Instance.CurrentClariaState != ClariaState.None)
        {
            ResetSidebarMenu();
            //return;
        }
        CameraHandler(false);
        //clariaModel.SetActive(false);
        SwitchScreen(SubMenuScreen.ClariaLanding);
        SendData(Constants.TVClariaLanding);
        SwitchEnvironment(TheEnvironement.Home);

        //SendOSCMessage(mannOSC, Constants.MannTherapyAddress);
        SendOSCMessage(mannOSC, Constants.RestartExperience);

        PlayButtonClickSound();

         Application.LoadLevel(2);                     // commented and added in home Btn delayer
    }

    //public void 
    #endregion

    #region ARCallbacks
    public void OnScanSuccess() {

        //Debug.Log(GameManager.Instance.CurrentScreen);

        if (GameManager.Instance.CurrentScreen == GameScreen.Scan)
        {
            PlayButtonClickSound();
            worldCamera.SetActive(true);
            bgCamera.SetActive(true);
            SwitchScreen(GameScreen.ScanSuccess);
        }
        else if (GameManager.Instance.CurrentScreen == GameScreen.ClariaHome)
        {

        }
    }
    #endregion

    #region VideoCallbacks
    private void OnVideoEnd(VideoPlayer videoPlayer) {
        videoPlayer.loopPointReached -= OnVideoEnd;
        SwitchScreen(SubMenuScreen.None);
    }
    #endregion

    #region HelperFunctions
    private void SwitchScreen(GameScreen gameState)
    {
        int index = (int)gameState;
        if (currentScreen)
            currentScreen.SetActive(false);
        GameManager.Instance.CurrentScreen = gameState;
        currentScreen = screens[index];
        currentScreen.SetActive(true);
    }

    private void SwitchScreen(SubMenuScreen gameState)
    {
        int index = (int)gameState;
        if (currentSubMenuScreen)
            currentSubMenuScreen.SetActive(false);
        GameManager.Instance.CurrentSubMenuScreen = gameState;
        currentSubMenuScreen = subMenuScreens[index];
        if (currentSubMenuScreen)
            currentSubMenuScreen.SetActive(true);
        OnHelpClick(null);
    }


    private void SolutionsHandler()
    {
        if (currentClariaSolutionsScreen)
            currentClariaSolutionsScreen.SetActive(false);
        currentClariaSolutionsScreen = solutionsScreens[currentSolutionId];
        if (currentClariaSolutionsScreen)
            currentClariaSolutionsScreen.SetActive(true);
        SendData(Constants.TVHardwareSolutionsId[currentSolutionId]);
    }


    private void ResetSidebarMenu() {
        foreach (GameObject g in currentMenuObjects) {
            g.SetActive(false);
            //Debug.Log(g.name + " - " + g.activeInHierarchy);
        }
        currentMenuObjects = new GameObject[] { };
        hardwareStateObject.SetActive(false);
        therapiesStateObject.SetActive(false);
        alarmScenarioStateObject.SetActive(false);
        typesOfTherapiesStateObject.SetActive(false);

        GameManager.Instance.CurrentClariaState = ClariaState.None;
        ResetSideBar(null);
        ResetSubMenu();
    }

    private void ResetSubMenu() {
        foreach (GameObject g in currentSubMenuObjects) {

            g.SetActive(false);
        }
        currentSubMenuObjects = new GameObject[] { };

        if (currentSubMenuScreen)
            currentSubMenuScreen.SetActive(false);
        GameManager.Instance.CurrentClariaSubState = ClariaSubState.None;
        GameManager.Instance.CurrentSubMenuScreen = SubMenuScreen.None;
        ResetSubSubMenuSideBar(null);
    }

    private void ResetSideBar(Transform clickedTransform) {
        if (currentActiveSidebar != null)
            SetSideBarActive(currentActiveSidebar, false);
        if (clickedTransform == null)
        {
            currentActiveSidebar = null;
            return;
        }
        currentActiveSidebar = clickedTransform;
        SetSideBarActive(currentActiveSidebar, true);
        ResetSubMenuSideBar(null);
    }

    private void ResetSubMenuSideBar(Transform clickedTransform)
    {
        if (currentActiveSubMenuSidebar != null)
            SetSideBarActive(currentActiveSubMenuSidebar, false);
        if (clickedTransform == null) {
            currentActiveSubMenuSidebar = null;
            return;
        }
        currentActiveSubMenuSidebar = clickedTransform;
        SetSideBarActive(currentActiveSubMenuSidebar, true);
    }

    private void ResetSubSubMenuSideBar(Transform clickedTransform)
    {
        if (currentActiveSubSubMenuSidebar != null)
            SetSideBarActive(currentActiveSubSubMenuSidebar, false);
        if (clickedTransform == null)
        {
            currentActiveSubSubMenuSidebar = null;
            return;
        }
        currentActiveSubSubMenuSidebar = clickedTransform;
        SetSideBarActive(currentActiveSubSubMenuSidebar, true);
    }

    private void ResetHardware(Transform clickedTransform, ClariaHardwareState nextState)
    {
        interactiveModel.SetActive(false);
        if (GameManager.Instance.CurrentClariaHardwareState != nextState) {
            if (currentHardwareTitle != null)
                SetSideBarActive(currentHardwareTitle, false);
            GameManager.Instance.CurrentClariaHardwareState = nextState;
            if (clickedTransform == null)
                return;
            currentHardwareTitle = clickedTransform;
            SetSideBarActive(currentHardwareTitle, true);
        }
    }

    private void ResetTherapies(Transform clickedTransform, ClariaTherapiesState nextState)
    {
        if (GameManager.Instance.CurrentClariaTherapiesState != nextState)
        {
            if (currentHardwareTitle != null)
                SetSideBarActive(currentHardwareTitle, false);
            if (clickedTransform == null)
                return;
            currentHardwareTitle = clickedTransform;
            SetSideBarActive(currentHardwareTitle, true);
            Debug.Log("Assign");
            GameManager.Instance.CurrentClariaTherapiesState = nextState;
        }
    }

    private void ResetAlarmScenario(Transform clickedTransform, ClariaAlarmScenarioState nextState)
    {
        if (GameManager.Instance.CurrentClariaAlarmScenarioState != nextState)
        {
            if (currentHardwareTitle != null)
                SetSideBarActive(currentHardwareTitle, false);
            if (clickedTransform == null)
                return;
            currentHardwareTitle = clickedTransform;
            SetSideBarActive(currentHardwareTitle, true);
            GameManager.Instance.CurrentClariaAlarmScenarioState = nextState;
        }
    }

    private void ResetTypesOfTherapies(Transform clickedTransform, ClariaTypesOfTherapiesState nextState)
    {
        if (GameManager.Instance.CurrentClariaTypesOfTherapiesState != nextState)
        {
            if (currentHardwareTitle != null)
                SetSideBarActive(currentHardwareTitle, false);
            if (clickedTransform == null)
                return;
            currentHardwareTitle = clickedTransform;
            SetSideBarActive(currentHardwareTitle, true);
            GameManager.Instance.CurrentClariaTypesOfTherapiesState = nextState;
        }
    }

    //private void ResetPatient(Transform clickedTransform, ClariaPatientState nextState)
    //{
    //    if (GameManager.Instance.CurrentClariaPatientState != nextState)
    //    {
    //        if (currentHardwareTitle != null)
    //            SetSideBarActive(currentHardwareTitle, false);
    //        if (clickedTransform == null)
    //            return;
    //        currentHardwareTitle = clickedTransform;
    //        SetSideBarActive(currentHardwareTitle, true);
    //        GameManager.Instance.CurrentClariaPatientState = nextState;

    //        if (currentPatientContent != null)
    //            currentPatientContent.SetActive(false);
    //        currentPatientScreenId = (int)nextState;
    //        currentPatientContent = currentSubMenuScreen.transform.GetChild(currentPatientScreenId).gameObject;
    //        currentPatientContent.SetActive(true);

    //    }
    //}

    private void SetSideBarActive(Transform t, bool setActive) {
        if (t != null) {
            t.Find("On").gameObject.SetActive(setActive);
            t.Find("Off").gameObject.SetActive(!setActive);
        }
        if (t.Find("PlusOn") == null)
            return;
        try
        {
            t.Find("PlusOn").gameObject.SetActive(setActive);
            t.Find("PlusOff").gameObject.SetActive(!setActive);
        }
        catch (Exception) {
        }
    }

    private void SendOSCMessage(OSC osc, string messageAddress)
    {
        OscMessage message;

        message = new OscMessage
        {
            address = messageAddress
        };

        Debug.Log("OSC Message : " + message);

        message.values.Add(1);
        osc.Send(message);
    }

    private void SendData(int clickedDataId, int clickedUIId = -1, int subMenuScreenId = -1, bool isAction = false) {
        GameData gameData = new GameData
        {
            gameId = Constants.GameId,
            dataId = clickedDataId,
            uiId = clickedUIId,
            screenId = subMenuScreenId,
            isSpecialAction = isAction,
            modelData = null
        };
        dataScript.SendData(gameData.SaveToString(), tvOSC);
    }

    public void SendData(int clickedDataId, float videoDuration)
    {
        GameData gameData = new GameData
        {
            gameId = Constants.GameId,
            dataId = clickedDataId,
            isSpecialAction = true,
            stopDuration = videoDuration,
            screenId = -1
        };
        dataScript.SendData(gameData.SaveToString(), tvOSC);
    }

    private void SendData(int clickedDataId, Vector2 currentScrollValue, int clickedUIId = -1)
    {
        GameData gameData = new GameData
        {
            gameId = Constants.GameId,
            dataId = clickedDataId,
            uiId = clickedUIId,
            screenId = currentNurseScreenId,
            scrollPosition = currentScrollValue
        };
        dataScript.SendData(gameData.SaveToString(), tvOSC);
    }

    public void SendData(int clickedDataId, int screenId) {
        SendData(clickedDataId, -1, screenId);
    }

    //public void SendData(int clickedDataId, float normalizedTime)
    //{
    //    SendData(clickedDataId, normalizedTime);
    //}

    public void SendNurseActionData(int actionId) {
        PlayButtonClickSound();
        SendData(Constants.TVConnectivityNurseId, actionId, 0, true);
    }




    public void SendData(int clickedDataId, ModelData modelInfo)
    {
        GameData gameData = new GameData
        {
            gameId = Constants.GameId,
            dataId = clickedDataId,
            uiId = 0,
            screenId = 0,
            modelData = modelInfo
        };
        dataScript.SendData(gameData.SaveToString(), tvOSC);

    }
  
    public void SendOSCMessage(string messageAddress)
    {
        SendOSCMessage(mannOSC, messageAddress);
    }



    public void TherapiesHandler(int currentTherapyId)
    {
        SendOSCMessage(mannOSC, Constants.MannTherapyTherapiesAddress[currentTherapyId]);
        SendData(Constants.TVTherapyTherapiesId[currentTherapyId]);
    }

    public void SwitchEnvironment(TheEnvironement nextEnvironment) {
        //Debug.Log("Enter switch environment");
        if (GameManager.Instance.CurrentEnvironement == nextEnvironment)
            return;
        //Debug.Log("Return skipped");
        GameManager.Instance.CurrentEnvironement = nextEnvironment;

        GameData gameData = new GameData
        {
            gameId = Constants.GameId,
            sideProjectorVideoId = (int)nextEnvironment
        };
        dataScript.SendData(gameData.SaveToString(), true, leftProjectorOSC, rightProjectorOSC);


        //dataScript.SendData()

        //ISFSObject obj = new SFSObject();
        //obj.PutInt("gameId", Constants.GameId);
        //obj.PutInt("environemntId", (int)GameManager.Instance.CurrentEnvironement);
        //serverScript.SendData(obj);
    }

    public void OnNurseScrollValueChange(RectTransform contentTransform) {
        SendData(Constants.TVConnectivityNurseId, contentTransform.anchoredPosition, - 1);
    }

    public void OnPatientScrollValueChange(RectTransform contentTransform)
    {
        SendData(Constants.TVConnectivityPatientOrderSuppliesId, contentTransform.anchoredPosition, -10);
    }


    private IEnumerator UIHandler(float stopSeconds, int id, int buttonId) {
        for (int i = 1; i < 4; i++)
            currentSubMenuScreen.transform.GetChild(0).GetChild(i).GetComponent<Button>().interactable = false;
        if (id > currentSubMenuScreen.transform.GetChild(0).childCount)
            yield break;
        if (currentPatientContent != null)
            currentPatientContent.SetActive(false);
        currentPatientContent = currentSubMenuScreen.transform.GetChild(0).GetChild(id).gameObject;
        currentPatientContent.SetActive(true);
        currentSubMenuScreen.transform.GetChild(0).GetChild(0).gameObject.SetActive(false);
        yield return new WaitForSeconds(stopSeconds);
        currentSubMenuScreen.transform.GetChild(0).GetChild(0).GetComponent<RectTransform>().anchoredPosition = currentSubMenuScreen.transform.GetChild(0).GetChild(buttonId).GetComponent<RectTransform>().anchoredPosition;
        currentSubMenuScreen.transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
        currentSubMenuScreen.transform.GetChild(0).GetChild(buttonId).GetComponent<Button>().interactable = true;
    }

    private void PatientUIHandler(int id, int buttonId) {
        if (currentPatientContent != null)
            currentPatientContent.SetActive(false);
        currentPatientContent = currentSubMenuScreen.transform.GetChild(0).GetChild(id).gameObject;
        currentPatientContent.SetActive(true);
        currentSubMenuScreen.transform.GetChild(0).gameObject.SetActive(true);  //bg
        currentSubMenuScreen.transform.GetChild(0).GetChild(0).gameObject.SetActive(false); //glow
        if (buttonId != -1) {   //if clickable screen
            currentSubMenuScreen.transform.GetChild(0).GetChild(0).GetComponent<RectTransform>().anchoredPosition = currentSubMenuScreen.transform.GetChild(0).GetChild(buttonId).GetComponent<RectTransform>().anchoredPosition;
            currentSubMenuScreen.transform.GetChild(0).GetChild(0).GetComponent<RectTransform>().localScale = buttonId == 4 || buttonId == 5 ?0.5f*Vector3.one:Vector3.one;
            currentSubMenuScreen.transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
            //currentSubMenuScreen.transform.GetChild(0).GetChild(buttonId).GetComponent<Button>().interactable = true;
        }
        SendData(Constants.TVConnectivityPatientId, id, -1); //clickeUIId = content, subscreenId = buttonId
    }

    private void CameraHandler(bool isARMode) {
        //Enable AR Session
        
        arObject.SetActive(isARMode);
        arSession.enabled = isARMode;
        worldCamera.SetActive(!isARMode);
        bgCamera.SetActive(!isARMode);
    }

    private void HelpHandler(bool isHelpOn) {
        if (helpButton == null)
            return;
        helpButton.transform.GetChild(0).gameObject.SetActive(isHelpOn);
        helpButton.transform.GetChild(1).gameObject.SetActive(!isHelpOn);
        if (currentHelpScreen)
            currentHelpScreen.SetActive(false);
        if (isHelpOn)
            if (currentSubMenuScreen.transform.Find("Help"))
                currentHelpScreen = currentSubMenuScreen.transform.Find("Help").gameObject;
        if (currentHelpScreen)
            currentHelpScreen.SetActive(isHelpOn);
    }

    public void PlayButtonClickSound() {
        audioSource.PlayOneShot(buttonClickSound);
    }

    #endregion

    #region ScrollCallbacks
    private void CellSelected(int cellIndex)
    {
        Debug.Log("OnSolutionsScrollEnd: " + cellIndex);
        SendData(Constants.TVHardwareSolutionsId[cellIndex%4]);
    }

    #endregion

    #region OSC
    public void OnOSCMessageReceived(OscMessage message)
    {
        //Debug.Log("OSC");
        Debug.Log(message.values[0].ToString());
        GameData gameData = GameData.CreateFromJSON(message.values[0].ToString());
        //OnDataReceived?.Invoke(gameData);

        switch (gameData.gameId) {
            case 1: //display tv
                switch (gameData.dataId) {
                    case 1: //share screen video end
                        currentSubMenuScreen.GetComponent<ClariaShareSourceHandler>().OnVideoEnd();
                        return;
                }
                return;
        }

    }
    #endregion
}
