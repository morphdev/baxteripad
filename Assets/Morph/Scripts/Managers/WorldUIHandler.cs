using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldUIHandler : MonoBehaviour
{

    public Camera uiCamera;
    private Transform myTransform;

    // Start is called before the first frame update
    void Start()
    {
        myTransform = transform;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 targetPoint = uiCamera.transform.position;

        // project camera position onto xz plane
        targetPoint.y = myTransform.position.y;

        // Vector3.up is a normal of the xz plane
        myTransform.LookAt(targetPoint, Vector3.up);
    }
}
