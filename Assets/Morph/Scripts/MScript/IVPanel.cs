using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class IVPanel : MonoBehaviour
{
    public Sprite select, nonselect;
    public Button btn1, btn2, btn3, btn4, btn5;
    public List<GameObject> objects;
    public List<Button> btns;
    public TherapyPage thpage;
    // Start is called before the first frame update
    void Start()
    {
        btn1.onClick.AddListener(Button1Op);
        btn2.onClick.AddListener(Button2Op);
        btn3.onClick.AddListener(Button3Op);
        btn4.onClick.AddListener(Button4Op);
        btn5.onClick.AddListener(Button5Op);
        btns.Add(btn1);
        btns.Add(btn2);
        btns.Add(btn3);
        btns.Add(btn4);
        btns.Add(btn5);
    }
    private void OnEnable()
    {
        Button1Op();
        PlayPause(false);
        //mvs.video.isLooping = false;
        mvs.video.loopPointReached += EndReach;
    }

    private void EndReach(VideoPlayer source)
    {
        //PlayState();
    }

    public Image play, pause,replay;
    public GameObject videoplayparent;
    public MVideoSeek mvs;
    public void PlayPause(bool isplay)
    {
        thpage.PlayPauseVideo(isplay);
        if(isplay)
        {
            pause.gameObject.SetActive(true);
            play.gameObject.SetActive(false);
            replay.gameObject.SetActive(false);
            
        }
        else
        {
            pause.gameObject.SetActive(false);
            play.gameObject.SetActive(true);
            replay.gameObject.SetActive(false);

        }
        videoplayparent.SetActive(!isplay);
    }
    public void Replay()
    {
        bool isplay = true;
        thpage.PlayPauseVideo(isplay);
        if (isplay)
        {
            pause.gameObject.SetActive(true);
            play.gameObject.SetActive(false);
            replay.gameObject.SetActive(false);
        }
        videoplayparent.SetActive(!isplay);

    }
    void OffSkills(int index)
    {
        for (int i = 0; i < objects.Count; i++)
        {
            objects[i].transform.parent.GetChild(0).gameObject.SetActive(true);
            objects[i].transform.parent.GetChild(1).gameObject.SetActive(false);
        }
        objects[index].transform.parent.GetChild(0).gameObject.SetActive(false);
        objects[index].transform.parent.GetChild(1).gameObject.SetActive(true);

    }
    // Update is called once per frame
    public void Button1Op()
    {
        //foreach (Button btn in btns)
        //    btn.image.sprite = nonselect;
        //btn1.image.sprite = select;
        thpage.PlayVideo("switching");
        currentvideostate = "switching";
        objects[0].gameObject.SetActive(true);
        OffSkills(0);
    }
    void Button2Op()
    {
        //foreach (Button btn in btns)
        //    btn.image.sprite = nonselect;
        //btn2.image.sprite = select;
        thpage.PlayVideo("insertingblue");
        currentvideostate = "insertingblue";
        objects[1].gameObject.SetActive(true);
        OffSkills(1);

    }
    void Button3Op()
    {
        //foreach (Button btn in btns)
        //    btn.image.sprite = nonselect;
        //btn3.image.sprite = select;
        thpage.PlayVideo("loadtubing");
        currentvideostate = "loadtubing";
        objects[2].gameObject.SetActive(true);
        OffSkills(2);

    }
    void Button4Op()
    {
        //foreach (Button btn in btns)
        //    btn.image.sprite = nonselect;
       // btn4.image.sprite = select;
        thpage.PlayVideo("closingdoor");
        currentvideostate = "closingdoor";
        objects[3].gameObject.SetActive(true);
        OffSkills(3);

    }
    void Button5Op()
    {
        //foreach (Button btn in btns)
        //    btn.image.sprite = nonselect;
        //btn5.image.sprite = select;
        thpage.PlayVideo("checktheline");
        currentvideostate = "checktheline";
        objects[4].gameObject.SetActive(true);
        OffSkills(4);

    }

    public string currentvideostate = "";
    float dta = 0f;
    void PlayState()
    {
        if ((Time.time - dta) > 1f)
        {
            dta = Time.time;
            switch (currentvideostate)
            {
                case "switching":
                    Button2Op();
                    break;
                case "insertingblue":
                    Button3Op();
                    break;
                case "loadtubing":
                    Button4Op();
                    break;
                case "closingdoor":
                    Button5Op();
                    break;
            }
        }
    }
    private void OnDisable()
    {
        thpage.vplayer.loopPointReached -= EndReach;
    }
}
