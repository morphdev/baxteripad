using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Lean.Gui;

public class AcuteTherapyOperations : TargetViewer<AcuteTherapyOperations>
{
    public List<BtnInfo> btnInfos;

    public List<ATParentToggle> pt;

    // public List<LeanToggle> parentStateToggles;
    // public List<LeanToggle> childToggle;


    public List<AcuteTherapyDatas> acuteTherapyDatas;
    public AcuteTherapyDatas currentAcuteTherapyData;

    public List<TextMeshProUGUI> alltextsForAcuteTherapy;
    public Color selectedcolor, whitecolor;

    public MAppController mappcontroller;

    public LeanToggle startingStep;

    public LeanToggle currentLT;
    public string sID, sbID, vID;                        // sID- screenID, sbID- subscreenID, vID- videoID, sbcID- subscreenchildID
    public string sbcID;
    public string sbscID;
    public string imgID;

    public string cvID;
    public string cimgID;
    //public string csbID;

    public ProjectionMapHighlight projMapBtn;

    public void Start()
    {
       
        // startingStep.TurnOn();
        for (int i = 0; i < pt.Count; i++)
        {
            int j = i;
            pt[j].pToggle.OnOn.AddListener(() =>
            {

                sID = pt[j].pToggle.gameObject.name;
                sbID = "";
                sbcID = "";
                vID = "";
                imgID = "";
                sbscID = "";
                AcuteTherapyOperationsATCheck();
            });

            for (int k = 0; k < pt[j].cToggle.Count; k++)
            {
                int l = k;
                pt[j].cToggle[l].spToggle.OnOn.AddListener(() =>
                {
                    sbID = pt[j].cToggle[l].spToggle.gameObject.name;
                    sbcID = "";
                    vID = "";
                    imgID = "";
                    sbscID = "";
                    AcuteTherapyOperationsATCheck();

                });

                for (int m = 0; m < pt[j].cToggle[l].scToggle.Count; m++)
                {
                    int n = m;
                    pt[j].cToggle[l].scToggle[n].OnOn.AddListener(() =>
                    {
                        sbcID = pt[j].cToggle[l].scToggle[n].gameObject.name;
                        vID = "";
                        imgID = "";
                        sbscID = "";
                        AcuteTherapyOperationsATCheck();
                                // Debug.LogError("sbcccc: " + n + " :l: " + l);
                    });

                   


                }
               
                for (int r = 0; r < pt[j].cToggle[l].sbcToggle.Count; r++)                                  // added 16-04-22
                {
                    int t = r;
                    pt[j].cToggle[l].sbcToggle[t].OnOn.AddListener(() =>
                    {
                        sbscID = pt[j].cToggle[l].sbcToggle[t].gameObject.name;
                        vID = "";
                        imgID = "";
                        AcuteTherapyOperationsATCheck();
                        // Debug.LogError("sbcccc: " + n + " :l: " + l);
                    });

                }


            }


        }

        for (int a = 0; a < btnInfos.Count; a++)
        {
            int b = a;
            btnInfos[b].btn.onClick.AddListener(() =>
            {
                if (!string.IsNullOrEmpty(btnInfos[b].imageID))
                {
                    vID = "";
                    imgID = btnInfos[b].imageID;
                    if (cimgID != imgID)
                        AcuteTherapyOperationsATCheck();

                    cimgID = imgID;
                }
                else if(!string.IsNullOrEmpty(btnInfos[b].videoID))
                {

                    imgID = "";
                    vID = btnInfos[b].videoID;
                    if (cvID != vID)
                         AcuteTherapyOperationsATCheck();
                    cvID = vID;
                }
                else                                                                    // if condition added 21-04-22
                {
                    imgID = "";
                    vID = "";

                    if (cvID != vID || cimgID != imgID)
                        AcuteTherapyOperationsATCheck();

                    cvID = vID;
                    cimgID = imgID;
                    /* vID = btnInfos[b].videoID;

                     if (cvID != vID)
                         AcuteTherapyOperationsATCheck();


                     cvID = vID;*/


                    /*  imgID = "";
                      vID = btnInfos[b].videoID;

                      if (cvID != vID)
                          AcuteTherapyOperationsATCheck();

                      cvID = vID;*/
                }
                // AcuteTherapyOperationsATCheck();
            });
        }

        #region COMMENTED 02-04-22
        /*   
        for (int i = 0; i < pt.Count; i++)
        {
            int j = i;
            pt[j].pToggle.OnOn.AddListener(() =>
            {

                sID = pt[j].pToggle.gameObject.name;
                sbID = "";
                sbcID = "";
                vID = "";
                imgID = "";

                for (int k = 0; k < pt[j].cToggle.Count; k++)
                {
                    int l = k;
                    pt[j].cToggle[l].spToggle.OnOn.AddListener(() =>
                    {
                        sbID = pt[j].cToggle[l].spToggle.gameObject.name;
                        sbcID = "";
                        vID = "";
                        imgID = "";

                        for (int m = 0; m < pt[j].cToggle[l].scToggle.Count; m++)
                        {
                            int n = m;
                            pt[j].cToggle[l].scToggle[n].OnOn.AddListener(() =>
                            {
                                sbcID = pt[j].cToggle[l].scToggle[n].gameObject.name;
                                vID = "";
                                imgID = "";
                                AcuteTherapyOperationsATCheck();
                                // Debug.LogError("sbcccc: " + n + " :l: " + l);
                            });

                        }
                        Debug.LogError(l + " :CHILD: " + pt[j].cToggle[l].spToggle.gameObject.name);
                        AcuteTherapyOperationsATCheck();

                    });

                }


                AcuteTherapyOperationsATCheck();
            });
        }*/
        #endregion

        #region commented
        /* for (int i = 0; i < pt.Count; i++)
         {
             int j = i;
             pt[j].pToggle.OnOn.AddListener(() =>
             {

                 sID = pt[j].pToggle.gameObject.name;
                 sbID = "";
                 vID = "";
                 for (int k = 0; k < pt[j].cToggle.childToggle.Count; k++)
                 {
                     int l = k;
                     pt[j].cToggle.childToggle[l].OnOn.AddListener(() =>
                     {
                         sbID = pt[j].cToggle.childToggle[l].gameObject.name;
                         vID = "";
                         AcuteTherapyOperationsATCheck();
                     });
                 }
                 AcuteTherapyOperationsATCheck();
             });
         }*/
        #endregion

    }

    /* protected override void Show(AcuteTherapyOperations current)
     {
         base.Show(this);
        /* DisablePanels();
         medicationdelivery.SetActive(true);
         if (vplayer == null)
             vplayer = GameObject.FindObjectOfType<VideoPlayer>();

         MAppData gameData = new MAppData
         {
             screenId = "",
             videoid = "",
             parameter = ""
         };
         mappcontroller.SendDisplayData(gameData);

     }*/


    public void DisableTextForAcuteTherapy(TextMeshProUGUI selectedtext = null)
    {
        foreach (TextMeshProUGUI texta in alltextsForAcuteTherapy)
            texta.color = selectedcolor;
        if (selectedtext != null)
        {
            selectedtext.color = whitecolor;
        }
    }

    public void SetImageColorWhite(Image image)
    {
        image.color = whitecolor;
    }

    public void SetImageColorBlue(Image image)
    {
        image.color = selectedcolor;
    }

    public void SetColorWhite(TextMeshProUGUI selectedtext = null)
    {
        if (selectedtext != null)
        {
            selectedtext.color = whitecolor;
        }
    }

    public void SetColorBlue(TextMeshProUGUI selectedtext = null)
    {
        if (selectedtext != null)
        {
            selectedtext.color = selectedcolor;
        }
    }

    public void PlayPauseVideo(bool isplay)
    {
        if (!string.IsNullOrEmpty(vID))
        {
            MAppData gameDataa = new MAppData
            {
                screenId = sID,
                subScreenId = sbID,
                subScreenChildId = sbcID,
                subScreenSubChildId = sbscID,
                imageId = imgID,
                videomode = isplay ? "PLAY" : "PAUSE",
                videoid = vID,
                parameter = ""
            };
            mappcontroller.SendDisplayData(gameDataa);
        }
    }


    public void AcuteTherapyOperationsATCheck()
    {

        /*  for (int i = 0; i < pt.Count; i++)
          {
              if (pt[i].pToggle.On)
              {
                  Debug.LogError("ParentName: " + pt[i].pToggle.gameObject.name);
                  sID = pt[i].pToggle.gameObject.name;
                  vID = "";
                  for (int j = 0; j < pt[i].cToggle.childToggle.Count; j++)
                  {
                      if (pt[i].cToggle.childToggle[j].On)
                      {
                          Debug.LogError(":childName: " + pt[i].cToggle.childToggle[j].gameObject.name);

                          vID = pt[i].cToggle.childToggle[j].gameObject.name;
                      }
                  }

              }

          }*/
        
        if(string.IsNullOrEmpty(imgID))
        {
            imgID = "";
            cimgID = "";
        }
        if (string.IsNullOrEmpty(vID))
        {
            vID = "";
            cvID = "";
        }
        if (string.IsNullOrEmpty(sID))
        {
            sID = "";
        }

        MAppData gameData = new MAppData
        {
            // screenId = MACUTESCREEN.PROGRAMMINGINFUSION,
            // videoid = "vtbi0",
            screenId = sID,
            subScreenId = sbID,
            subScreenChildId = sbcID,
            subScreenSubChildId = sbscID,
            imageId = imgID,
            videoid = vID,
            parameter = ""
        };
        mappcontroller.SendDisplayData(gameData);
        // mappcontrolloer.SendLeftRghtData(gameData);
        Debug.LogError("data sent: " + gameData);
    }


    public void AcuteTherapyOperationsAT(int c)
    {
        /* currentAcuteTherapyData = acuteTherapyDatas[c];
         // Debug.Log("c: " + c + " : css.sid: " + currentAcuteTherapyData.sID + " : css.vid: " + currentAcuteTherapyData.vID);

         string cvID = currentAcuteTherapyData.vID.ToString();
         if (cvID.Equals("NONE"))
         {
             cvID = "";
         }

         MAppData gameData = new MAppData
         {
             // screenId = MACUTESCREEN.PROGRAMMINGINFUSION,
             // videoid = "vtbi0",
             screenId = currentAcuteTherapyData.sID.ToString(),
             videoid = cvID,
             parameter = ""
         };
         mappcontroller.SendDisplayData(gameData);*/
        // mappcontrolloer.SendLeftRghtData(gameData);
        // Debug.LogError("data sent: " + gameData);
    }

    public void AcuteTherapyCheckForImage()
    {
        if(string.IsNullOrEmpty(imgID))
        {
            AcuteTherapyOperationsATCheck();
            Debug.LogError("test:");
        }
    }

    public void DisposablePortfolio(Vector2 currentScrollValue)                                       // added 28-06-22
    {
        MAppData gameData = new MAppData
        {
            screenId = sID,
            subScreenId = sbID,
            subScreenChildId = sbcID,
            subScreenSubChildId = sbscID,
            imageId = imgID,
            videoid = vID,
            parameter = "",
            scrollPosition = currentScrollValue
        };
        mappcontroller.SendDisplayData(gameData);
    }

    public void OnDisposableScrollValueChange(RectTransform contentTransform)                               //added 28-06-22
    {
        DisposablePortfolio(contentTransform.anchoredPosition);
    }


    public void HomeOperation()
    {
        // OtherCueDataSend();   //29-03-22


        Application.LoadLevel(Application.loadedLevel);

    }

    void OtherCueDataSend()
    {
        mappcontroller.SendLeftRghtData("/cues/Bank-1/cues/by_cell/col_1/row_2");

    }

    public void SetVideoID(string val)
    {
        vID = val;
        AcuteTherapyOperationsATCheck();
    }

    public void SetImageID(string val)
    {
        imgID = val;
        AcuteTherapyOperationsATCheck();
    }

    public void SetSubscreenSubchildID()
    {
        if (sbscID != "")
        {
            sbscID = "";

            AcuteTherapyOperationsATCheck();
        }

    }

    public void SetSubscreenSubchildID(string val)
    {
        sbscID = val;

        AcuteTherapyOperationsATCheck();
        
    }

    public void SetImageIDforECCO2R(string val)                                                 // 16-04-22
    {
        sbscID = "";
        imgID = val;
        AcuteTherapyOperationsATCheck();
    }


    public void SetSubscreenchildID(string val)
    {
        if (sbcID != "")
        {
            sbcID = "";

            AcuteTherapyOperationsATCheck();
        }

    }


    public void WaitForVideoFinish(float val)
    {
        projMapBtn.WaitForVidFinishForAT(val);
       // projMapBtn.WaitForVidFinish(20.5f);
    }

    public void PrintDetails()                                                          // 12-07-22 added
    {
        Debug.LogWarning("tt:block clicked");
    }

}

public enum ATStates
{
    ACUTETHERAPY,
    SETUPMODE,
    THERAPY,
    MONITOR,
    DISPOSABLES,
    SOLUTIONS,
    CONNECTIVITY
}

public enum ATSubStates
{
    NONE,
    AKI,
    SEPSIS,
    CORRELATION,
    SETUPMODEINITIAL,
    PREPARINGTHEPATIENT,
    THERAPYSELECTION,
    ENTERPRESCRIPTION,
    LOADINGTHESET,
    CONNECTPATIENT,

}

[System.Serializable]
public class AcuteTherapyDatas
{
    public ATStates sID;
    public ATSubStates vID;
}


[System.Serializable]
public class ATParentToggle
{
    public LeanToggle pToggle;
    public List<ATChildToggle> cToggle;
    //public ATChildToggle cToggle;
}

[System.Serializable]
public class ATChildToggle
{
    public LeanToggle spToggle;
    public List<LeanToggle> scToggle;
    public List<LeanToggle> sbcToggle;                              //16-04-22
}

[System.Serializable]
public class AcuteTherapyToggles
{
    public LeanToggle ltt;
    public List<ATParentToggle> PToggle1;
}

[System.Serializable]
public class BtnInfo
{
    public Button btn;
    public string videoID;
    public string imageID;

}
