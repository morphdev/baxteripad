using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public static  class BaxterUtils
{
    public static List<T> GetInterfacesInChildrenA<T>(this GameObject gObj)
    {
        List<T> datas = new List<T>();
        var mObjs = gObj.GetComponentsInChildren<MonoBehaviour>();
        foreach (var ti in mObjs)
        {
                if (ti.GetType().GetInterfaces().Contains(typeof(T)))
            {
                datas.Add((T)(object)ti);
            }
        }
        return datas;
    }

   
}
