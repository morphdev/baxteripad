using Com.Morph.Baxter;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class ClariaShareSourceHandler : MonoBehaviour
{

    public GameObject play;
    public GameObject pause;

    public GameObject[] videoEndObjects;

    private GameController gameControllerScript;

    public Transform featureBase;

    public VideoPlayer hiddenVideoPlayer;
    public VideoClip[] clips;

    public VideoController videoControllerScript;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Initialize() {
        foreach (GameObject g in videoEndObjects)
            g.SetActive(false);
        if (play)
            play.SetActive(false);
        if (pause)
            pause.SetActive(true);
        videoControllerScript.enabled = false;
        hiddenVideoPlayer.gameObject.SetActive(true);
        hiddenVideoPlayer.clip = clips[0];
        videoControllerScript.slider.maxValue = (float)hiddenVideoPlayer.length;
        videoControllerScript.enabled = true;
        hiddenVideoPlayer.Play();
    }

    private void OnEnable()
    {
        if (gameControllerScript == null)
            gameControllerScript = GameObject.FindGameObjectWithTag(Constants.GameControllerTag).GetComponent<GameController>();
        Initialize();
    }

    public void OnPlayPause(bool pausePlayback)
    {
        if (pausePlayback)
        {
            if (hiddenVideoPlayer.isPlaying)
                hiddenVideoPlayer.Pause();

            play.SetActive(true);
            pause.SetActive(false);

            gameControllerScript.SendData(Constants.TVVideoControls, 0);
        }
        else
        {
            if (!hiddenVideoPlayer.isPlaying)
                hiddenVideoPlayer.Play();

            play.SetActive(false);
            pause.SetActive(true);

            gameControllerScript.SendData(Constants.TVVideoControls, 1);
        }
    }

    public void OnVideoEnd() {
        foreach (GameObject g in videoEndObjects)
            g.SetActive(true);
        if (play)
            play.SetActive(false);
        if (pause)
            pause.SetActive(false);
    }

    public void OnReplay() {
        Initialize();
        //gameControllerScript.SendData(Constants.TVVideoControls, 1);
        gameControllerScript.SendData(Constants.TVConnectivityShareScreenId, -1);
        foreach (Transform t in featureBase)
        {
            t.GetComponent<Animator>().enabled = true;
        }

    }

    public void OnFeatureClick(int clipId) {
        OnVideoEnd();

        videoControllerScript.enabled = false;
        hiddenVideoPlayer.clip = clips[clipId];
        videoControllerScript.slider.maxValue = (float)hiddenVideoPlayer.length;
        videoControllerScript.enabled = true;

    }

    public void OnArchitectureClick(GameObject offObject) {
        bool activate = offObject == null ? false : offObject.activeInHierarchy;  //architecture    
        foreach (GameObject g in videoEndObjects)
            g.SetActive(false);
        if (play)
            play.SetActive(false);
        if (pause)
            pause.SetActive(activate);

        hiddenVideoPlayer.gameObject.SetActive(activate);
        if (activate) {
            videoControllerScript.enabled = false;
            hiddenVideoPlayer.clip = clips[0];
            videoControllerScript.slider.maxValue = (float)hiddenVideoPlayer.length;
            videoControllerScript.enabled = true;
            hiddenVideoPlayer.Play();
        }

    }

}
