using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class ATButtonHighlighter : MonoBehaviour
{

    public VideoPlayer vplayer;
    public VideoClip clip;
    public ProjectionMapHighlight dsoProjMapBtn;
    
    public VideoPlayer vplayer2;
    public VideoClip clip2;
    public ProjectionMapHighlight dsoProjMapBtn2;
    public void SetupModeOperation()
    {
        Debug.Log("SetupMode: Connect patient");
        vplayer.Stop();
        vplayer.clip = clip;
        vplayer.Play();
        dsoProjMapBtn.WaitForVidFinish(52.5f, vplayer);
    }

    public void RunTherapyOperation()
    {
        Debug.Log("Run Therapy");
        vplayer2.Stop();
        vplayer2.clip = clip2;
        vplayer2.Play();
        dsoProjMapBtn2.WaitForVidFinish(61f, vplayer2);
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
