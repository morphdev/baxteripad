using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConnectivityPanel : MonoBehaviour
{
    public Button btn1, btn2, btn3, btn4;
    public Sprite s1, s2, s3, s4, s5, ns1, ns2, ns3, ns4, ns5;
    public TherapyPage thpage;
    public GameObject connectivitytitleobject, playPauseParent;
    public Button b1, b2, b3, b4, b5, b6, b7;

    public MPlayPauseVideo playPauseVideo;

    // Start is called before the first frame update
    void Start()
    {
        btn1.onClick.AddListener(BdlOperation);
        btn2.onClick.AddListener(ApOperation);
        btn3.onClick.AddListener(AdOperation);
        btn4.onClick.AddListener(AtOperation);
        b1.onClick.AddListener(B1Operation);
        b2.onClick.AddListener(B2Operation);
        b3.onClick.AddListener(B3Operation);
        b4.onClick.AddListener(B4Operation);
        b5.onClick.AddListener(B5Operation);
        b6.onClick.AddListener(B6Operation);
        b7.onClick.AddListener(B7Operation);

    }
    void B1Operation()
    {
        Reset();
        thpage.PlayVideo("ConB1" +
            "" +
            "");
    }
    void B2Operation()
    {
        Reset();
        thpage.PlayVideo("ConB2");
    }
    void B3Operation()
    {
        Reset();
        thpage.PlayVideo("ConB3");
    }
    void B4Operation()
    {
        Reset();
        thpage.PlayVideo("ConB4");
    }
    void B5Operation()
    {
        Reset();
        thpage.PlayVideo("ConB5");
    }
    void B6Operation()
    {
        Reset();
        b6.image.sprite = s5;
        print("b6");
        thpage.PlayVideo("ConB6");
        //playPauseVideo.PlayPause(true);
    }
    void B7Operation()
    {
        Reset();
        thpage.PlayVideo("ConB7");
    }
    private void OnEnable()
    {
        Reset();
        connectivitytitleobject.gameObject.SetActive(true);
        playPauseParent.SetActive(false);
        thpage.PlayVideo("connectivitytitle");

        //BdlOperation();
        //btn1.image.sprite = s1;
    }
    private void Reset()
    {
        btn1.image.sprite = ns1;
        btn2.image.sprite = ns2;
        btn3.image.sprite = ns3;
        btn4.image.sprite = ns4;
        b6.image.sprite = ns5;
    }
    private void AtOperation()
    {
        Reset();
        btn4.image.sprite = s4;
        connectivitytitleobject.gameObject.SetActive(false);
        playPauseParent.SetActive(true);

        thpage.PlayVideo("conatvideo");
        //playPauseVideo.PlayPause(true);

    }

    private void AdOperation()
    {
        Reset();
        btn3.image.sprite = s3;
        connectivitytitleobject.gameObject.SetActive(false);
        playPauseParent.SetActive(true);

        thpage.PlayVideo("conadvideo");
        //playPauseVideo.PlayPause(true);
    }

    private void ApOperation()
    {
        Reset();
        btn2.image.sprite = s2;
        connectivitytitleobject.gameObject.SetActive(false);
        playPauseParent.SetActive(true);

        thpage.PlayVideo("conapvideo");
        //playPauseVideo.PlayPause(true);
    }

    private void BdlOperation()
    {
        Reset();
        btn1.image.sprite = s1;
        connectivitytitleobject.gameObject.SetActive(false);
        playPauseParent.SetActive(true);

        thpage.PlayVideo("conbdvideo");
        //playPauseVideo.PlayPause(true);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
