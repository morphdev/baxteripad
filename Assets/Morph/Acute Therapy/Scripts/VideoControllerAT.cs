using Lean.Gui;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoControllerAT : MonoBehaviour
{

    public GameObject MainPanel, textPanel, videoPanel;
    public int count;

    public LeanSwitch switchState;
    public int currentIndex = 0;

    public void IncreaseSwitch()
    {
        if(currentIndex < switchState.States.Count-1)
        {
            currentIndex++;
            switchState.Switch(currentIndex);
        }
       // Debug.LogError(switchState.States.Count + " :Total:count:" + currentIndex);
    }

    public void DecreaseSwitch()
    {
        if (currentIndex > 0)
        {
            currentIndex--;
            switchState.Switch(currentIndex);
          //  Debug.LogError(switchState.States.Count + " :Decreased:count:" + currentIndex);
        }
    }
    public void SetState()
    {
        switchState.Switch(currentIndex);
    }

    public void ResetSwitchState()
    {
        currentIndex = 0;
        switchState.Switch(currentIndex);    
    }

    public void OnMainPanelBtnClicked()
    {
        MainPanel.SetActive(false);
        textPanel.SetActive(true);
        count = 1;
    }

    public void OnPlayVideoBtnClicked()
    {
        textPanel.SetActive(false);
        videoPanel.SetActive(true);
        count = 2;
    }

    public void Back()
    {
        if (count == 2)
        {
            textPanel.SetActive(true);
            videoPanel.SetActive(false);
        }
        else
        {
            textPanel.SetActive(false);
            MainPanel.SetActive(true);
        }
        if (count > 0)
        {
            count -= 1;
        }
    }


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
