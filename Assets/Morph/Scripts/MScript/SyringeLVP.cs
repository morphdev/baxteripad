using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SyringeLVP : MonoBehaviour
{
    public string SYRLVPTYPE = "SVP";
    public Button block, explode, interactive;
    public Sprite blockspr, unblockspr, explodespr, unexplodespr, interactiveSpr, uninteractiveSpr;
    public GameObject model, picobject, interactiveModel;
    public TherapyPage thpage;
    private void OnEnable()
    {
        BlockOperation();
        model.transform.rotation = Quaternion.identity;
    }
    // Start is called before the first frame update
    void Start()
    {
        block.onClick.AddListener(BlockOperation);
        explode.onClick.AddListener(ExplodeOperation);
        interactive.onClick.AddListener(InteractiveOperation);
    }
    void BlockOperation()
    {
        picobject.SetActive(false);
        interactiveModel.SetActive(false);
        model.SetActive(true);
        block.image.sprite = blockspr;
        explode.image.sprite = unexplodespr;
        interactive.image.sprite = uninteractiveSpr;
        thpage.PlayVideo(SYRLVPTYPE + "MODEL");
    }
    void ExplodeOperation()
    {
        interactiveModel.SetActive(false);
        picobject.SetActive(true);
        model.SetActive(false);
        block.image.sprite = unblockspr;
        explode.image.sprite = explodespr;
        interactive.image.sprite = uninteractiveSpr;
        thpage.PlayVideo(SYRLVPTYPE + "Video");
    }

    void InteractiveOperation()
    {
        explode.image.sprite = unexplodespr;
        block.image.sprite = unblockspr;
        interactive.image.sprite = interactiveSpr;
        picobject.SetActive(false);
        model.SetActive(false);
        interactiveModel.SetActive(true);
        //thpage.PlayVideo(SYRLVPTYPE + "INT");
    }

    private void OnDisable()
    {
        model.SetActive(false);
        interactiveModel.SetActive(false);
    }
}
