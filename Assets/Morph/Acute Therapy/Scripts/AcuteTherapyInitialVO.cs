using Lean.Gui;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcuteTherapyInitialVO : MonoBehaviour
{
    [SerializeField]
    LeanToggle startingStep;

    private void OnEnable()
    {
        Invoke("EnableToggleWithDelay", 0.5f);                  // added 29-04-22
    }

    public void OnDisable()
    {
        startingStep.TurnOff();
    }

    public void EnableToggleWithDelay()
    {
        startingStep.TurnOn();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
