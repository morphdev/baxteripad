using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragHandler : MonoBehaviour
{

    private Vector3 clickedPosition;
    private Transform parentTransform;
    private Vector3 screenPoint;
    private BoxCollider myCollider;
    private Camera renderCamera;

    // Start is called before the first frame update
    void Start()
    {
        parentTransform = transform.parent;
        myCollider = GetComponent<BoxCollider>();
        renderCamera = GetComponentInParent<ExplodedHandler>().camTransform.GetComponent<Camera>();

        Debug.Log(" DRAG HANDLEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE "+transform.name);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        //Debug.Log("Clicked");
        clickedPosition = parentTransform.position;
        screenPoint = renderCamera.ScreenToWorldPoint(Input.mousePosition);
    }

    private void OnMouseDrag()
    {
        Vector3 dragPos = renderCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
        Debug.Log(dragPos.x);
        parentTransform.position = new Vector3(clickedPosition.x + (dragPos.x-clickedPosition.x), clickedPosition.y +( dragPos.y -clickedPosition.y), parentTransform.position.z);
    }

    private void OnMouseUp()
    {
        myCollider.enabled = false;
        Ray ray = renderCamera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out RaycastHit hit, 100.0f))
        {
            Debug.Log("You selected the " + hit.transform.name); // ensure you picked right object
            if (hit.transform.name.Equals("Magnet")) {
                //parentTransform.position = hit.transform.parent.position;
                parentTransform.gameObject.SetActive(false);
                hit.transform.parent.Find("Assemble/" + transform.name.Replace("Cube", "")).gameObject.SetActive(true);
                enabled = false;
                return;
            }
        }
        myCollider.enabled = true;

        //parentTransform.position = clickedPosition;
    }

}
