using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OSCManager : MonoBehaviour
{
    public OSC tvOSC;

    // Start is called before the first frame update
    void Start()
    {
        //SendOSCMessage("/medias/1659849905.mp4/assign");
    }


    private void OnVideoEnd(OscMessage message)
    {
        //water video end
        //play HoldingVisual and LED blank
        //Go back to the HomeScreen

        Debug.Log("VideoEnd");

    }

    private void SendOSCMessage(OSC osc, string messageAddress)
    {
        OscMessage message;

        message = new OscMessage
        {
            address = messageAddress
        };
        message.values.Add(1);
        osc.Send(message);
    }

    public void OnButtonClick(int id) {
        switch (id) {
            case 0:
                //SendOSCMessage(tvOSC, "/medias/1659849905.mp4/assign");
                SendOSCMessage(tvOSC, "/composition/layers/1/clips/1/connect");
                return;
            case 1:
                //SendOSCMessage(osc2, "/composition/layers/1/clips/2/connect");
                return;
        }
    }



}
