using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.Events;

public class ProjectionMapHighlight : MonoBehaviour
{
    Coroutine LerpCgrpCR;
    Coroutine waitForVidCR;
    CanvasGroup cgrp;
    Button projMapButton;
    VideoPlayer thPage;
    float timer;
    float duration;
    Vector3 intialScale;

    public Button[] highlightEndButtons;

    public UnityEvent OnVideoCompleted;
    public UnityEvent OnVideoExited;

    private void Awake()
    {
        projMapButton = GetComponent<Button>();
        cgrp = GetComponent<CanvasGroup>();
        intialScale = cgrp.transform.localScale;
        projMapButton.onClick.AddListener(EndHighlight);

        for (int i = 0; i < highlightEndButtons.Length; i++)
        {
            int j = i;
            highlightEndButtons[j].onClick.AddListener(EndHighlight);
        }
    }

    void EndHighlight()
    {
        if (LerpCgrpCR != null)
        {
            StopCoroutine(LerpCgrpCR);
            LerpCgrpCR = null;
        }
        cgrp.alpha = 1;
        cgrp.transform.localScale = intialScale;
        if (waitForVidCR != null)
        {
            StopCoroutine(waitForVidCR);
            waitForVidCR = null;
        }

        OnVideoExited?.Invoke();                                    // added 03-05-22
    }


    public void WaitForVidFinish(float _duration, VideoPlayer _thpage)
    {


        duration = _duration;

        if (thPage == null)
            thPage = _thpage;

        Invoke("DelayedWaitForVidFinish", 0.1f);
    }

    public void WaitForVidFinishForAT(float _duration)
    {

        duration = _duration;

        Invoke("DelayedWaitForVidFinish", 0.1f);
    }


    void DelayedWaitForVidFinish()
    {
        EndHighlight();

       // Debug.LogError("entered");
        if (waitForVidCR != null)
        {
            StopCoroutine(waitForVidCR);
            waitForVidCR = null;
        }

        waitForVidCR = StartCoroutine(DSOButtonHighlighter(cgrp, duration));
    }

    IEnumerator DSOButtonHighlighter(CanvasGroup dsoButton, float vidDuration)
    {
        timer = vidDuration;

        while (timer > 0)
        {
            if (thPage.isPlaying)
            {
                timer -= Time.deltaTime;
            }

            yield return null;
        }

        if (LerpCgrpCR != null)
        {
            StopCoroutine(LerpCgrpCR);
            LerpCgrpCR = null;
        }

        LerpCgrpCR = StartCoroutine(LerpLightRepeat(dsoButton));

        OnVideoCompleted?.Invoke();                                             // added 03-05-22
        yield break;
    }



    IEnumerator LerpLightRepeat(CanvasGroup dsoButton)
    {
        while (true)
        {
            //Lerp to intensity1
            yield return LerpLight(dsoButton, 0.7f, Vector3.one * 0.9f, 0.7f);
            //Lerp to intensity2
            yield return LerpLight(dsoButton, 1.1f, Vector3.one * 1.05f, 0.5f);
        }
    }

    IEnumerator LerpLight(CanvasGroup targetLight, float toIntensity, Vector3 toScale, float duration)
    {
        float currentIntensity = targetLight.alpha;
        Vector3 currentScale = targetLight.transform.localScale;

        float counter = 0;
        while (counter < duration)
        {
            counter += Time.deltaTime;
            var step = counter / duration;
            targetLight.alpha = Mathf.Lerp(currentIntensity, toIntensity, step);
            targetLight.transform.localScale = Vector3.Lerp(currentScale, toScale, step);
            yield return null;
        }
    }

    private void OnDisable()
    {

        EndHighlight();
    }
}
