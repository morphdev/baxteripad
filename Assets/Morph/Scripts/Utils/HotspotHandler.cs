using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotspotHandler : MonoBehaviour
{

    private Quaternion initialRotation;
    private Transform myTransform;
    private Transform uiCamera;
    private Canvas myCanvas;

    private void Awake()
    {
        uiCamera = GameObject.Find("UICamera").transform;
        myTransform = transform;
        initialRotation = myTransform.rotation;
        myCanvas = GetComponentInChildren<Canvas>();
        if (myCanvas != null)
            myCanvas.worldCamera = uiCamera.GetComponent<Camera>();
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //myTransform.rotation = initialRotation;
        myTransform.LookAt(uiCamera);
        transform.rotation = Quaternion.LookRotation(uiCamera.forward);
    }
}
