using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundVideoControllerAT : MonoBehaviour
{
    public GameObject bgCanvas, bgCamera;

    private void OnEnable()
    {
        bgCanvas.SetActive(true);
        bgCamera.SetActive(true);
    }

    public void OnDisable()
    {
      //  bgCanvas.SetActive(false);
       // bgCamera.SetActive(false);
    }

}
