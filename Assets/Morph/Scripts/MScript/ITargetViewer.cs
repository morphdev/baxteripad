﻿using System;

public interface ITargetViewer
{
    void Show(object currentobject);
    void Hide();
    Type GetType { get; }
    bool IsHideObject { get; }
}