using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

public class ChooseTherapyPage : TargetViewer<ChooseTherapyPage>
{
    public Button peritbtn, acutebtn, medibtn,restartbtn,restartBtn1;
    public MAppController mappcontrolloer;
    public GameObject arcamera;
    public ARSession arsession;
    // Start is called before the first frame update
    void Start()
    {
        peritbtn.onClick.AddListener(PeritOperation);
        acutebtn.onClick.AddListener(Acubtn);
        medibtn.onClick.AddListener(MediOp);
        restartbtn.onClick.AddListener(RestartOp);
        restartBtn1.onClick.AddListener(RestartOp);
    }
    protected override void Show(ChooseTherapyPage current)
    {
        base.Show(this);
       // CameraSetUp(true);
        CameraSetUp(false);


    }
    void CameraSetUp(bool isactive)
    {
        arcamera.gameObject.SetActive(isactive);
        arsession.enabled = isactive;
       // Debug.LogError("session:" + arsession.enabled);
    }
    private void OnEnable()
    {
        if(hidedobject.activeSelf)
        {
            arcamera.SetActive(true);
           // Debug.Log(hidedobject.activeSelf);
        }
    }

    private void RestartOp()
    {
        MAppData gameData = new MAppData
        {
            //screenId = "RESTART",
            screenId = "HOME",
            videoid = "",
            parameter = ""
        };
        mappcontrolloer.SendDisplayData(gameData);
        mappcontrolloer.SendLeftRghtData(gameData);
        arcamera.SetActive(false);
          
       // ScreenSaverDataDelayer();
        Invoke("ScreenSaverDataDelayer", 0.2f);

       // Application.LoadLevel(0);

    }

    private void MediOp()
    {
        MAppData gameData = new MAppData
        {
            screenId = "MEDICATESCENELOAD",
            videoid = "",
            parameter = ""
        };
        if (mappcontrolloer != null)
        {
        
            mappcontrolloer.SendDisplayData(gameData);
            mappcontrolloer.SendLeftRghtData(gameData);
        }
       
        Debug.Log("Therapy Page SHOWWWWWWWWWW ");
        UIManager.Instance.Show("TherapyPage");
        CameraSetUp(false);

        base.Hide();
    }

    private void Acubtn()
    {
        MAppData gameData = new MAppData
        {
            screenId = "ACUTESCENELOAD",
            videoid = "",
            parameter = ""
        };
        if (mappcontrolloer != null)
        {

            mappcontrolloer.SendDisplayData(gameData);
            mappcontrolloer.SendLeftRghtData(gameData);
        }

        Debug.Log("Acute Page SHOWWWWWWWWWW ");
        UIManager.Instance.Show("AcuteTherapyOperations");
        CameraSetUp(false);

        base.Hide();
    }

    private void PeritOperation()
    {
        MAppData gameData = new MAppData
        {
            screenId = "PERITSCENELOAD",
            videoid = "",
            parameter = ""
        };
        mappcontrolloer.SendDisplayData(gameData);
        mappcontrolloer.SendLeftRghtData(gameData);

        Invoke("CallMenu", 1f);
    }
    void CallMenu()
    {
        Application.LoadLevel(1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // while pressing restart btn, data to send to mannosc   - 31-03-22
    public void ScreenSaverDataDelayer()
    {
        mappcontrolloer.SendLeftRghtData("/cues/Bank-1/cues/by_cell/col_1/row_2");
        Debug.LogError("screensaverDelay:");
        Application.LoadLevel(0);
    }

    public void ScreenSaverDataaDelayerForManOSC()                                              // 09-06-22 added
    {
        mappcontrolloer.SendLeftRghtData("/cues/Bank-1/cues/by_cell/col_1/row_2");
        Debug.LogError("delayer added:");
    }

    public void ScreenSaverDelayerManOSC()                                                            //09-06-22
    {
        Invoke("ScreenSaverDataaDelayerForManOSC", 0.05f);       
    }

}
