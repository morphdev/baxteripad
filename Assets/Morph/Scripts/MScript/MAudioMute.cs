using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class MAudioMute : MonoBehaviour
{
    public VideoPlayer vPlayer;
    public AudioSource audioso;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        vPlayer.SetDirectAudioMute((ushort)0, true);
        // ...and we set the audio source for this track
      //  vPlayer.SetTargetAudioSource(0, audioso);
    }
}
