using Com.Morph.Baxter;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchDrag : MonoBehaviour
{
    public int explodedID;
    private float dist;
    private bool dragging = false;
    private Vector3 offset;
    private Transform toDrag;

    public Camera rayCamera;
    public GameController gameControllerScript;
    public DataHandler dataScript;
    public OSC tvOSC;

    void Update()
    {
        Vector3 v3;
        //Debug.Log("Transform name " + transform.name);
        if (Input.touchCount != 1)
        {
            dragging = false;
            return;
        }

        Touch touch = Input.touches[0];
        Vector3 pos = touch.position;

        if (touch.phase == TouchPhase.Began)
        {
            Ray ray = rayCamera.ScreenPointToRay(pos);
            int layerMask = LayerMask.GetMask("WorldObjects");
            //int layerMask = (1 << cubeLayerIndex);
            //layerMask = ~layerMask;

            if (Physics.Raycast(ray, out RaycastHit hit, 100f, layerMask))
            {

                //if (hit.transform.name.Equals("ModelDrag"))
                //    return;

                //Debug.Log("Here");
                toDrag = hit.transform;
                dist = hit.transform.position.z - rayCamera.transform.position.z;
                v3 = new Vector3(pos.x, pos.y, dist);
                v3 = rayCamera.ScreenToWorldPoint(v3);
                offset = toDrag.position - v3;
                dragging = true;
            }
        }
        if (dragging && touch.phase == TouchPhase.Moved)
        {
            v3 = new Vector3(Input.mousePosition.x, Input.mousePosition.y, dist);
            v3 = rayCamera.ScreenToWorldPoint(v3);
            toDrag.position = v3 + offset;

            ModelData modelData = new ModelData
            {
                modelId = 0,
                modelName = toDrag.name,
                modelPosition = toDrag.localPosition,
                modelRotation = toDrag.rotation
            };
            if (gameControllerScript)
                gameControllerScript.SendData(Constants.TVHardwareClariaExplodedId, modelData);
            else
                SendData(explodedID, modelData);

        }
        if (dragging && (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled))
        {
            dragging = false;

            Ray ray = rayCamera.ScreenPointToRay(pos);
            int layerMask = LayerMask.GetMask("Default");

            if (Physics.Raycast(ray, out RaycastHit hit, 100f, layerMask))
            {
                //Debug.Log("Here: " + hit.transform.name);
                if (hit.transform.name.Equals("Magnet"))
                {
                    toDrag.position = hit.transform.parent.Find("Assemble/" + toDrag.name).position;
                    //toDrag.gameObject.SetActive(false);
                    //hit.transform.parent.Find("Assemble/" + toDrag.name).gameObject.SetActive(true);
                    //toDrag.gameObject.SetActive(false);
                }
            }

            ModelData modelData = new ModelData
            {
                modelId = 0,
                modelName = toDrag.name,
                modelPosition = toDrag.localPosition,
                modelRotation = toDrag.rotation
            };
            if (gameControllerScript)
                gameControllerScript.SendData(Constants.TVHardwareClariaExplodedId, modelData);
            else
                SendData(explodedID, modelData);
        }
    }

    private int GetId(string dragName)
    {
        switch (dragName)
        {
            case "Cassette":
                return 1;
            case "Digital_Board":
                return 2;
            case "Exuaht":
                return 3;
            case "Heat_pad":
                return 4;
            case "Lid":
                return 5;
            case "Mainfold_Assembly":
                return 6;
            case "Null":
                return 7;
            case "PowerSupply":
                return 8;
            case "Pump":
                return 9;
            case "Switch_Board":
                return 10;
            default:
                return 0;
        }
    }

    void SendData(int clickedDataId, ModelData modelInfo)
    {
        GameData gameData = new GameData
        {
            gameId = Constants.GameId,
            dataId = clickedDataId,
            uiId = 0,
            screenId = explodedID,
            modelData = modelInfo
        };

        dataScript.SendData(gameData.SaveToString(), tvOSC);
    }
}
