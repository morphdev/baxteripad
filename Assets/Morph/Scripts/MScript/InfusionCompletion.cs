using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfusionCompletion : MonoBehaviour
{
    public Sprite select, nonselect;
    public Button btn1, btn2, btn3;
    public GameObject h1, h2, h3;
    public List<Button> btns;
    public TherapyPage thpage;
    // Start is called before the first frame update
    void Start()
    {
        btn1.onClick.AddListener(Button1Op);
        btn2.onClick.AddListener(Button2Op);
        btn3.onClick.AddListener(Button3Op);
       
        btns.Add(btn1);
        btns.Add(btn2);
        btns.Add(btn3);
       
    }
    private void EndReach(UnityEngine.Video.VideoPlayer source)
    {
       // PlayState();
    }

  

    public Image play, pause, replay;
    public GameObject videoplayparent;
    public MVideoSeek mvs;
    public void PlayPause(bool isplay)
    {
        thpage.PlayPauseVideo(isplay);
        if (isplay)
        {
            pause.gameObject.SetActive(true);
            play.gameObject.SetActive(false);
            replay.gameObject.SetActive(false);

        }
        else
        {
            pause.gameObject.SetActive(false);
            play.gameObject.SetActive(true);
            replay.gameObject.SetActive(false);

        }
        videoplayparent.SetActive(!isplay);
    }
    public void Replay()
    {
        bool isplay = true;
        thpage.PlayPauseVideo(isplay);
        if (isplay)
        {
            pause.gameObject.SetActive(true);
            play.gameObject.SetActive(false);
            replay.gameObject.SetActive(false);
        }
        videoplayparent.SetActive(!isplay);

    }
    private void OnDisable()
    {
        mvs.video.loopPointReached -= EndReach;
    }
    private void OnEnable()
    {
        Button1Op();
        PlayPause(false);
        //mvs.video.isLooping = false;
        mvs.video.loopPointReached += EndReach;
    }
    // Update is called once per frame
    void Button1Op()
    {

        Debug.Log(" Butto1 Operation  ");
        h1.SetActive(false);
        h2.SetActive(false);
        h3.SetActive(false);
        //foreach (Button btn in btns)
        //    btn.image.sprite = nonselect;
        //btn1.image.sprite = select;
        currentvideostate = "infseccomplete";
        h1.SetActive(true);
        thpage.PlayVideo("infseccomplete");
    }
    void Button2Op()
    {
        h1.SetActive(false);
        h2.SetActive(false);
        h3.SetActive(false);

        Debug.Log(" Butto1 Operation  ");
        //foreach (Button btn in btns)
        //    btn.image.sprite = nonselect;
       // btn2.image.sprite = select;
        currentvideostate = "inflineflush";
        h2.SetActive(true);
        thpage.PlayVideo("inflineflush");

    }
    void Button3Op()
    {
        h1.SetActive(false);
        h2.SetActive(false);
        h3.SetActive(false);

        Debug.Log(" Butto1 Operation  ");
        //foreach (Button btn in btns)
        //    btn.image.sprite = nonselect;
        //btn3.image.sprite = select;
        currentvideostate = "infreturntofusion";
        h3.SetActive(true);

        thpage.PlayVideo("infreturntofusion");

    }
    public string currentvideostate = "";
    float dta = 0f;
    void PlayState()
    {
        if ((Time.time - dta) > 1f)
        {
            dta = Time.time;
            switch (currentvideostate)
            {
                case "infseccomplete":
                    Button2Op();
                    break;
                case "inflineflush":
                    Button3Op();
                    break;
              
            }
        }
    }
}
