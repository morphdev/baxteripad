using Com.Morph.Baxter;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using DG.Tweening;
using UnityEngine.UI;

public class VideoHandler : MonoBehaviour
{

    private VideoPlayer videoPlayer;

    public VideoClip[] videoClips;
    private int currentClipId;
    private GameController gameControllerScript;
    public GameObject clariaExplodedModel;
    public Slider slider;
    public Transform infoSlider;
    private GameObject currentInfo;
    public GameObject play;
    public GameObject pause;

    public GameObject initialPlay;
    public GameObject previewImage;

    public GameObject[] videoEndObjects;

    // Start is called before the first frame update
    void OnEnable()
    {
        foreach (GameObject g in videoEndObjects)
            g.SetActive(false);
        if(play)
            play.SetActive(false);
        if(pause)
            pause.SetActive(true);
        if (gameControllerScript == null)
            gameControllerScript = GameObject.FindGameObjectWithTag(Constants.GameControllerTag).GetComponent<GameController>();

        if (clariaExplodedModel)
            clariaExplodedModel.SetActive(false);
        currentClipId = 0;
        if (videoPlayer == null) {
            videoPlayer = GetComponentInChildren<VideoPlayer>();
            videoPlayer.targetTexture.Release();
        }
        if (videoClips.Length > 0)  //pre therapy
        {
            if (gameControllerScript == null)
                gameControllerScript = GameObject.FindGameObjectWithTag(Constants.GameControllerTag).GetComponent<GameController>();
            videoPlayer.clip = videoClips[currentClipId];
            if (initialPlay == null)
                videoPlayer.Play();
            slider.maxValue = (float)videoPlayer.length;
            InfoHandler();
            //OnPlayPause(false);
            gameControllerScript.SendData(GameManager.Instance.CurrentSubMenuScreen== SubMenuScreen.PreTherapy?Constants.TVPreTherapy[currentClipId]:Constants.TVPostTherapy[currentClipId], -1);
            play.SetActive(true);
            pause.SetActive(false);
        }
        videoPlayer.loopPointReached += OnVideoEnd;
        GetComponentInChildren<RawImage>().color = new Color(1f, 1f, 1f, 1f);
        if (initialPlay) {
            initialPlay.SetActive(true);
        }
        if (previewImage)
            previewImage.SetActive(true);


    }

    private void OnVideoEnd(VideoPlayer theVideoPlayer)
    {
        return;

        videoPlayer.loopPointReached -= OnVideoEnd;
        //if (GameManager.Instance.CurrentSubMenuScreen == SubMenuScreen.ClariaExploded) {
        //    //claria exploded video end
        //    //enable the model
        //    GetComponentInChildren<RawImage>().DOFade(0f, 1f).SetEase(Ease.InOutSine).OnComplete(()=> clariaExplodedModel.SetActive(true));
        //}



        //return;
        //if (videoClips.Length == 0) {
        //    //transform.gameObject.SetActive(false);
        //}
        //currentClipId++;
        //if (currentClipId < videoClips.Length) {
        //    videoPlayer.clip = videoClips[currentClipId];
        //    videoPlayer.Play();
        //    gameControllerScript.TherapiesHandler(currentClipId);
        //    videoPlayer.loopPointReached += OnVideoEnd;
        //    return;
        //}
        ////transform.gameObject.SetActive(false);
        //gameControllerScript.TherapiesHandler(currentClipId);

        currentClipId++;
        if (currentClipId < videoClips.Length)  //pre therapy
        {
            slider.GetComponent<VideoController>().enabled = false;
            videoPlayer.clip = videoClips[currentClipId];
            slider.maxValue = (float)videoPlayer.length;
            slider.GetComponent<VideoController>().enabled = true;
            //videoPlayer.Play();

            //gameControllerScript.SendData(GameManager.Instance.CurrentSubMenuScreen == SubMenuScreen.PreTherapy ? Constants.TVPreTherapy[currentClipId] : Constants.TVPostTherapy[currentClipId], 1);
           

            if (GameManager.Instance.CurrentSubMenuScreen == SubMenuScreen.PreTherapy)
            {
                //pre therapy
                if (currentClipId == 5)
                {
                    //connect yourself
                    gameControllerScript.SendOSCMessage(Constants.MannTherapyPreTherapyConnectAddress); //pointer animation
                }
                else
                {
                    gameControllerScript.SendOSCMessage(Constants.MannTherapyTherapyDefaultAddress); //default
                }
            }
            if (GameManager.Instance.CurrentSubMenuScreen == SubMenuScreen.PostTherapy)
            {
                //post therapy
                if (currentClipId == 2)
                {
                    //disconnect yourself
                    gameControllerScript.SendOSCMessage(Constants.MannTherapyPreTherapyConnectAddress); //pointer animation
                }
                else
                {
                    gameControllerScript.SendOSCMessage(Constants.MannTherapyTherapyDefaultAddress); //default
                }
            }


            videoPlayer.loopPointReached += OnVideoEnd;
            InfoHandler();
            return;
        }
        else {
            Debug.Log("VideoEnd");
            foreach (GameObject g in videoEndObjects)
                g.SetActive(true);

            play.SetActive(false);
            pause.SetActive(videoPlayer.isLooping);

            if (GameManager.Instance.CurrentSubMenuScreen == SubMenuScreen.PreTherapy)
            {
                play.SetActive(false);
                pause.SetActive(false);

            }
            if (GameManager.Instance.CurrentSubMenuScreen == SubMenuScreen.PostTherapy)
            {
                play.SetActive(false);
                pause.SetActive(false);
            }
        }

    }

    private void OnDisable()
    {
        videoPlayer.loopPointReached -= OnVideoEnd;
        videoPlayer.targetTexture.Release();
        if (clariaExplodedModel)
            clariaExplodedModel.SetActive(false);

    }

    private void InfoHandler() {
        if (currentInfo) {
            currentInfo.transform.Find("Off").gameObject.SetActive(true);
            currentInfo.transform.Find("On").gameObject.SetActive(false);
        }
        currentInfo = infoSlider.GetChild(3+currentClipId).gameObject;
        if (currentInfo)
        {
            currentInfo.transform.Find("Off").gameObject.SetActive(false);
            currentInfo.transform.Find("On").gameObject.SetActive(true);
        }
    }

    public void OnPlayPause(bool pausePlayback) {
        if (pausePlayback)
        {
            if(videoPlayer.isPlaying)
                videoPlayer.Pause();
            play.SetActive(true);
            pause.SetActive(false);
            if (initialPlay)
            {
                initialPlay.SetActive(true);
            }

            gameControllerScript.SendData(Constants.TVVideoControls, 0);
        }
        else {
            if (!videoPlayer.isPlaying)
            {

                videoPlayer.Play();                             // uncommented 17-08-22
            }
            play.SetActive(false);
            pause.SetActive(true);
            if (initialPlay)
            {
                initialPlay.SetActive(false);
            }
            //if(previewImage)
            //    previewImage.SetActive(false);
            gameControllerScript.SendData(Constants.TVVideoControls, 1);
        }
        gameControllerScript.PlayButtonClickSound();
    }

    public void OnPointerClick(int id) {
        videoPlayer.loopPointReached -= OnVideoEnd;
        currentClipId = id;
        videoPlayer.clip = videoClips[currentClipId];
        if (!play.activeInHierarchy)
            videoPlayer.Play();
        slider.maxValue = (float)videoPlayer.length;
        //gameControllerScript.TherapiesHandler(currentClipId);
        //gameControllerScript.SendData(Constants.TVPreTherapy[currentClipId], -1);
        gameControllerScript.SendData(GameManager.Instance.CurrentSubMenuScreen == SubMenuScreen.PreTherapy ? Constants.TVPreTherapy[currentClipId] : Constants.TVPostTherapy[currentClipId], play.activeInHierarchy?-1:1);

        if (GameManager.Instance.CurrentSubMenuScreen == SubMenuScreen.PreTherapy) {
            //pre therapy
            if (currentClipId == 5)
            {
                //connect yourself
                gameControllerScript.SendOSCMessage(Constants.MannTherapyPreTherapyConnectAddress); //pointer
            }
            else {
                gameControllerScript.SendOSCMessage(Constants.MannTherapyTherapyDefaultAddress); //default
            }
        }
        if (GameManager.Instance.CurrentSubMenuScreen == SubMenuScreen.PostTherapy)
        {
            //post therapy
            if (currentClipId == 2)
            {
                //disconnect yourself
                gameControllerScript.SendOSCMessage(Constants.MannTherapyPreTherapyConnectAddress);   //pointer
            }
            else
            {
                gameControllerScript.SendOSCMessage(Constants.MannTherapyTherapyDefaultAddress); //default
            }
        }

        videoPlayer.loopPointReached += OnVideoEnd;
        InfoHandler();
        gameControllerScript.PlayButtonClickSound();
    }


    public void OnReplay() {
        foreach (GameObject g in videoEndObjects)
            g.SetActive(false);
        videoPlayer.loopPointReached -= OnVideoEnd;
        videoPlayer.Play();
        play.SetActive(false);
        pause.SetActive(true);
        videoPlayer.loopPointReached += OnVideoEnd;
        gameControllerScript.PlayButtonClickSound();

    }


    //active therapy
    public void OnVideoChange() {
        if (videoPlayer == null)
            return;
        videoPlayer.loopPointReached -= OnVideoEnd;
        foreach (GameObject g in videoEndObjects)
            g.SetActive(false);
        play.SetActive(false);
        pause.SetActive(true);
        videoPlayer.loopPointReached += OnVideoEnd;
    }

    //pre post therapy
    public void OnPrePostTherapyReplay() {
        foreach (GameObject g in videoEndObjects)
            g.SetActive(false);
        play.SetActive(false);
        pause.SetActive(true);
        OnPointerClick(videoClips.Length-1);    //play the last video
        //videoPlayer.loopPointReached -= OnVideoEnd;
        //OnPlayPause(false);
        //videoPlayer.loopPointReached += OnVideoEnd;
    }

}
