using Com.Morph.Baxter;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClariaPatientHandler : MonoBehaviour
{
    private int currentScreenId;
    public GameController gameController;

    // Start is called before the first frame update
    void OnEnable()
    {
        currentScreenId = 0;
        //StartCoroutine(StartAnimation());
        //OnNextClick();
    }

    private IEnumerator StartAnimation() {
        currentScreenId++;
        if (currentScreenId < transform.childCount) {
            transform.GetChild(currentScreenId-1).gameObject.SetActive(false);
            transform.GetChild(currentScreenId).gameObject.SetActive(true);
            gameController.SendData(Constants.TVConnectivityPatientId, currentScreenId);
            yield return new WaitForSeconds(5f);
            StartCoroutine(StartAnimation());
        }
    }

    private void OnDisable()
    {

        transform.GetChild(currentScreenId).gameObject.SetActive(false);
        //enabled = false;
    }

    public void OnBackClick() {
        gameController.PlayButtonClickSound();
        currentScreenId--;
        if (currentScreenId < transform.childCount)
        {
            transform.GetChild(currentScreenId + 1).gameObject.SetActive(false);
            transform.GetChild(currentScreenId).gameObject.SetActive(true);
            gameController.SendData(Constants.TVConnectivityPatientOrderSuppliesId, currentScreenId);
        }
    }

    public void OnNextClick()
    {
        gameController.PlayButtonClickSound();
        //Debug.Log("Next");
        currentScreenId++;
        if (currentScreenId < transform.childCount)
        {
            transform.GetChild(currentScreenId - 1).gameObject.SetActive(false);
            transform.GetChild(currentScreenId).gameObject.SetActive(true);
            gameController.SendData(Constants.TVConnectivityPatientOrderSuppliesId, currentScreenId);
        }
    }

    public void OnReplay() {
        gameController.SendData(Constants.TVConnectivityPatientOrderSuppliesId, currentScreenId);
    }




}
