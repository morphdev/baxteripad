using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundController : MonoBehaviour
{
    public Button[] buttons;
    public AudioSource audioSource;
    public AudioClip btnClickSound;

    private void Start()
    {
        for (int i = 0; i < buttons.Length; i++)
        {
            int j = i;
            buttons[j].onClick.AddListener(PlayButtonClickSound);
        }
    }

    public void PlayButtonClickSound()
    {
        audioSource.PlayOneShot(btnClickSound);
    }

    private void OnDestroy()
    {
        for (int i = 0; i < buttons.Length; i++)
        {
            int j = i;
            buttons[j].onClick.RemoveListener(PlayButtonClickSound);
        }
    }
}
