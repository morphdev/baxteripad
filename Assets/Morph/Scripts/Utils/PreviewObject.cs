using Com.Morph.Baxter;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreviewObject : MonoBehaviour
{

    //Vector3 mPrevPos;
    //Vector3 mPosDelta;
    public Transform camTransform;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.touchCount == 1) {
            Touch touch = Input.GetTouch(0);
            transform.Rotate(transform.up, Vector3.Dot(touch.deltaPosition, camTransform.right) * Time.deltaTime * Constants.RotationSpeed, Space.World);
            transform.Rotate(camTransform.right, Vector3.Dot(touch.deltaPosition, camTransform.up) * Time.deltaTime * Constants.RotationSpeed, Space.World);
        }

        //if (Input.GetMouseButton(0)) {
        //    mPosDelta = Input.mousePosition - mPrevPos;

        //    if (Vector3.Dot(transform.up, Vector3.up) >= 0)
        //    {
        //        transform.Rotate(transform.up, -Vector3.Dot(mPosDelta, camTransform.right), Space.World);
        //    }
        //    else {
        //        transform.Rotate(transform.up, Vector3.Dot(mPosDelta, camTransform.right), Space.World);
        //    }

        //    transform.Rotate(camTransform.right, -Vector3.Dot(mPosDelta, camTransform.up), Space.World);
        //}

        //mPrevPos = Input.mousePosition;
    }
}
