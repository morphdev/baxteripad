using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.Morph.Baxter {

    public enum GameScreen { Start, Scan, ScanSuccess, ClariaHome }
    public enum SubMenuScreen { None, ShareSource, Nurse, Patient, Baxter, SoftwareUpdate, AlarmScenario, HeatPad, Cassette, Solutions, Therapies,
                                HardwareExploaded, HeatPadExploaded, CassetteExploaded, HardwareWireframe, HeatPadWireframe, CassetteWireframe, PostTherapy,
                                TherapiesDrain, TherapiesFill, TherapiesDwell, ClariaBlock, ClariaExploded, ClariaLanding, PatientOrderSupplies, TherapyHome,
                                PreTherapy, ActiveTherapy, TypeNormal, TypeTidal, AlarmScenarioOcclusion, AlarmScenarioLowDrain, HardwareInteractive
    }
    public enum ClariaState { None, Connectivity, Hardware, Therapy}
    public enum ClariaSubState { None, Baxter, Patient }
    public enum ClariaHardwareState { None, Block, Wireframe, Exploded, Interactive }
    public enum ClariaTherapiesState { Fill, Dwell, Drain }
    public enum ClariaAlarmScenarioState { None, Occlusion, LowDrain }
    public enum ClariaTypesOfTherapiesState { None, Normal, Tidal }
    //public enum ClariaPatientState { None, CodeActivation, AcceptTherapy, SoftwareUpdate, OrderSupplies }
    public enum ClariaPatientState { EnterActivation = 5, ConfirmConfigurations, AreYouMarySmith, ShareClinic, NewProgram, IsProgramCorrect, ProgramAccepted, PressGoToStart}
    public enum TheSystemType { TV, Mannequin, LeftProjector, RightProjector }
    public enum TheEnvironement { Loop, Home, OperationTheatre,NONE}

    public enum VoiceOverState { Introduction}

    public class GameManager {
        private static GameManager gmInstance;

        public GameScreen CurrentScreen { get; set; }
        public SubMenuScreen CurrentSubMenuScreen { get; set; }
        public ClariaState CurrentClariaState { get; set; }
        public ClariaSubState CurrentClariaSubState { get; set; }
        public ClariaHardwareState CurrentClariaHardwareState { get; set; }
        public ClariaTherapiesState CurrentClariaTherapiesState { get; set; }
        public ClariaAlarmScenarioState CurrentClariaAlarmScenarioState { get; set; }
        public ClariaTypesOfTherapiesState CurrentClariaTypesOfTherapiesState { get; set; }
        public ClariaPatientState CurrentClariaPatientState { get; set; }
        public TheEnvironement CurrentEnvironement { get; set; }
        public VoiceOverState CurrentVoiceOverState { get; set; }

        public static GameManager Instance
        {
            get {
                if (gmInstance == null)
                    gmInstance = new GameManager();
                return gmInstance;
            }
        }

        private GameManager() {
            CurrentScreen = GameScreen.Start;
            CurrentClariaState = ClariaState.None;
            CurrentClariaSubState = ClariaSubState.None;
            CurrentSubMenuScreen = SubMenuScreen.None;
            CurrentClariaHardwareState = ClariaHardwareState.None;
            CurrentClariaTherapiesState = ClariaTherapiesState.Fill;
            CurrentClariaAlarmScenarioState = ClariaAlarmScenarioState.None;
            CurrentClariaTypesOfTherapiesState = ClariaTypesOfTherapiesState.None;
            CurrentEnvironement = TheEnvironement.NONE;
            CurrentClariaPatientState = ClariaPatientState.EnterActivation;
            CurrentVoiceOverState = VoiceOverState.Introduction;
        }

        public static int Mod(int x, int m)
        {
            int r = x % m;
            return r < 0 ? r + m : r;
        }

        public void OnReset() {
            CurrentScreen = GameScreen.Start;
            CurrentClariaState = ClariaState.None;
            CurrentClariaSubState = ClariaSubState.None;
            CurrentSubMenuScreen = SubMenuScreen.None;
            CurrentClariaHardwareState = ClariaHardwareState.None;
            CurrentClariaTherapiesState = ClariaTherapiesState.Fill;
            CurrentClariaAlarmScenarioState = ClariaAlarmScenarioState.None;
            CurrentClariaTypesOfTherapiesState = ClariaTypesOfTherapiesState.None;
            CurrentEnvironement = TheEnvironement.Home;
            CurrentClariaPatientState = ClariaPatientState.EnterActivation;
            CurrentVoiceOverState = VoiceOverState.Introduction;
        }

    }

    public class Constants {
        public const int GameId = 2; //used for SFS data mapping (to identify different clients) 
        public const float DelayDuration = 0.5f;
        public const float RotationSpeed = 30f;
        public const float TweenDuration = 1f;

        public const string GameControllerTag = "GameController";

        //public const string TVLogoAddress = "/medias/Baxter_Logo.mp4/assign";
        //public const string TVConnectivityShareScreenAddress = "/medias/ShareSource.mp4/assign";
        //public const string TVConnectivityNurseAddress = "/medias/Nurse.mp4/assign";
        //public const string TVConnectivityPatientAddress = "/medias/Patient.mp4/assign";
        //public const string TVConnectivityBaxterAddress = "/medias/Baxter.mp4/assign";
        //public const string TVConnectivityBaxterSoftwareAddress = "/medias/SoftwareUpdate.mp4/assign";
        //public const string TVHardwareAddress = "/medias/HardwareBlock.mp4/assign";
        //public const string TVHardwareWireframeAddress = "/medias/HardwareWireframe.mp4/assign";
        //public const string TVHardwareExploadedAddress = "/medias/HardwareExploaded.mp4/assign";
        //public const string TVHardwareHeatPadAddress = "/medias/HarwareHeatPad.mp4/assign";
        //public const string TVHardwareHeatPadWireframeAddress = "/medias/HarwareHeatPadWireframe.mp4/assign";
        //public const string TVHardwareHeatPadExploadedAddress = "/medias/HarwareHeatPadExploaded.mp4/assign";
        //public static string[] TVHardwareSolutionsAddress = { "/medias/SolutionsDianeal.mp4/assign", "/medias/SolutionsExtraneal.mp4/assign", "/medias/SolutionsNutrineal.mp4/assign", "/medias/SolutionsPhysioneal.mp4/assign" };
        //public const string TVHardwareCasetteAddress = "/medias/HardwareCassette.mp4/assign";
        //public const string TVHardwareCasetteWireframeAddress = "/medias/HardwareCassetteWireframe.mp4/assign";
        //public const string TVHardwareCasetteExploadedAddress = "/medias/HardwareCassetteExploaded.mp4/assign";
        //public const string TVTherapyPatientSystemPrepAddress = "/medias/PatientSystemPrep.mp4/assign";
        //public static string[] TVTherapyTherapiesAddress = { "/medias/Drain.mp4/assign", "/medias/Fill.mp4/assign", "/medias/Dwell.mp4/assign", "/medias/Osmosis.mp4/assign" };
        //public const string TVTherapyAlarmScenarioOcclusionAddress = "/medias/AlarmScenarioOclusion.mp4/assign";
        //public const string TVTherapyAlarmScenarioLowDrainAddress = "/medias/AlarmScenarioLowDrain.mp4/assign";
        //public const string TVTherapyPostTherapyAddress = "/medias/PostTherapies.mp4/assign";
        //public const string TVTherapyTypeOfTherapiesNormalAddress = "/medias/Normal.mp4/assign";
        //public const string TVTherapyTypeOfTherapiesTidalAddress = "/medias/Tidal.mp4/assign";

        //public const string MannTherapyPatientSystemPrepAddress = "/medias/4.jpg/assign";
        //public static string[] MannTherapyTherapiesAddress = { "/medias/5.jpg/assign", "/medias/6.jpg/assign", "/medias/12_dwell.jpg/assign", "/medias/5.jpg/assign" };
        //public const string MannTherapyAlarmScenarioOcclusionAddress = "/medias/4.jpg/assign";
        //public const string MannTherapyAlarmScenarioLowDrainAddress = "/medias/13_yello.jpg/assign";
        //public const string MannTherapyPostTherapyAddress = "/medias/4.jpg/assign";
        //public const string MannTherapyTypeOfTherapiesNormalAddress = "/medias/6.jpg/assign";
        //public const string MannTherapyTypeOfTherapiesTidalAddress = "/medias/6.jpg/assign";

        public const string TVSelectAddress = "/surfaces/Display/select";
        public const string MannSelectAddress = "/surfaces/Mannequin/select";
        public const string Mann1SelectAddress = "/surfaces/Mannequin1/select";
        public const string Mann2SelectAddress = "/surfaces/Mannequin2/select";
        public const string MaskSelectAddress = "/surfaces/Mask/select";
        public const string Mask1SelectAddress = "/surfaces/Mask1/select";
        public const string Mask2SelectAddress = "/surfaces/Mask2/select";

        //public const string TVLogoAddress = "/cues/selected/cues/by_cell/col_1/row_1";
        //public const string TVConnectivityShareScreenAddress = "/cues/selected/cues/by_cell/col_2/row_1";
        //public const string TVConnectivityNurseAddress = "/cues/selected/cues/by_cell/col_3/row_1";
        //public const string TVConnectivityPatientAddress = "/cues/selected/cues/by_cell/col_4/row_1";
        //public const string TVConnectivityBaxterAddress = "/cues/selected/cues/by_cell/col_5/row_1";
        //public const string TVConnectivityBaxterSoftwareAddress = "/cues/selected/cues/by_cell/col_6/row_1";
        //public const string TVHardwareAddress = "/cues/selected/cues/by_cell/col_7/row_1";
        //public const string TVHardwareWireframeAddress = "/cues/selected/cues/by_cell/col_8/row_1";
        //public const string TVHardwareExploadedAddress = "/cues/selected/cues/by_cell/col_9/row_1";
        //public const string TVHardwareHeatPadAddress = "/cues/selected/cues/by_cell/col_10/row_1";
        //public const string TVHardwareHeatPadWireframeAddress = "/cues/selected/cues/by_cell/col_11/row_1";
        //public const string TVHardwareHeatPadExploadedAddress = "/cues/selected/cues/by_cell/col_12/row_1";
        //public static string[] TVHardwareSolutionsAddress = { "/cues/selected/cues/by_cell/col_13/row_1", "/cues/selected/cues/by_cell/col_14/row_1", "/cues/selected/cues/by_cell/col_15/row_1", "/cues/selected/cues/by_cell/col_16/row_1" };
        //public const string TVHardwareCasetteAddress = "/cues/selected/cues/by_cell/col_17/row_1";
        //public const string TVHardwareCasetteWireframeAddress = "/cues/selected/cues/by_cell/col_18/row_1";
        //public const string TVHardwareCasetteExploadedAddress = "/cues/selected/cues/by_cell/col_19/row_1";
        //public const string TVTherapyPatientSystemPrepAddress = "/cues/selected/cues/by_cell/col_20/row_1";
        //public static string[] TVTherapyTherapiesAddress = { "/cues/selected/cues/by_cell/col_21/row_1", "/cues/selected/cues/by_cell/col_22/row_1", "/cues/selected/cues/by_cell/col_23/row_1" };
        //public const string TVTherapyAlarmScenarioOcclusionAddress = "/cues/selected/cues/by_cell/col_24/row_1";
        //public const string TVTherapyAlarmScenarioLowDrainAddress = "/cues/selected/cues/by_cell/col_25/row_1";
        //public const string TVTherapyPostTherapyAddress = "/cues/selected/cues/by_cell/col_26/row_1";
        //public const string TVTherapyTypeOfTherapiesNormalAddress = "/cues/selected/cues/by_cell/col_27/row_1";
        //public const string TVTherapyTypeOfTherapiesTidalAddress = "/cues/selected/cues/by_cell/col_28/row_1";

        public const string MannTherapyAddress = "/cues/Bank-1/cues/by_cell/col_1/row_1"; //solid man
        public const string MannTherapyPatientSystemPrepAddress = "/cues/Bank-1/cues/by_cell/col_2/row_1";    //scan
        public const string MannTherapyPreTherapyConnectAddress = "/cues/Bank-1/cues/by_cell/col_2/row_1";    //pointer in loop
        public const string screenSaverAddress = "/cues/Bank-1/cues/by_cell/col_1/row_2";
        public static string[] MannTherapyTherapiesAddress = { "/cues/Bank-1/cues/by_cell/col_3/row_1", "/cues/Bank-1/cues/by_cell/col_4/row_1", "/cues/Bank-1/cues/by_cell/col_5/row_1" };   //fill dwell drain
        //public const string MannTherapyAlarmScenarioOcclusionAddress = "/cues/selected/cues/by_cell/col_6/row_2";
        //public const string MannTherapyAlarmScenarioLowDrainAddress = "/cues/selected/cues/by_cell/col_7/row_2";
        //public const string MannTherapyPostTherapyAddress = "/cues/selected/cues/by_cell/col_2/row_2";
        //public const string MannTherapyTypeOfTherapiesNormalAddress = "/cues/selected/cues/by_cell/col_9/row_2";
        //public const string MannTherapyTypeOfTherapiesTidalAddress = "/cues/selected/cues/by_cell/col_10/row_2";
        public const string MannTherapyTherapyDefaultAddress = "/cues/Bank-1/cues/by_cell/col_1/row_2";

        public const string RestartExperience = "/cues/Bank-1/cues/by_cell/col_1/row_2";

        public const int TVConnectivityShareScreenId = 1;
        public const int TVConnectivityNurseId = 2;
        public const int TVConnectivityPatientId = 3;
        public const int TVConnectivityBaxterId = 4;
        public const int TVHardwareId = 5;
        public const int TVHardwareWireframeId = 6;
        public const int TVHardwareExploadedId = 7;
        public const int TVHardwareHeatPadId = 8;
        public const int TVHardwareHeatPadWireframeId = 9;
        public const int TVHardwareHeatPadExploadedId = 10;
        public static int[] TVHardwareSolutionsId = { 11, 12, 13, 14 };
        public const int TVHardwareCasetteId = 15;
        public const int TVHardwareCasetteWireframeId = 16;
        public const int TVHardwareCasetteExploadedId = 17;
        public const int TVTherapyPatientSystemPrepId = 18;
        public static int[] TVTherapyTherapiesId = { 19, 20, 21 };
        public const int TVTherapyAlarmScenarioOcclusionId = 22;
        public const int TVTherapyAlarmScenarioLowDrainId = 23;
        public const int TVTherapyPostTherapyId = 24;
        public const int TVTherapyTypeOfTherapiesNormalId = 25;
        public const int TVTherapyTypeOfTherapiesTidalId = 26;

        public const int TVConnectivityPatientCodeActivationId = 27;
        public const int TVConnectivityPatientAcceptTherapyId = 28;
        public const int TVConnectivityPatientSoftwareUpdateId = 29;
        public const int TVConnectivityPatientOrderSuppliesId = 30;

        public const int TVHardwareClariaBlockId = 31;
        public const int TVHardwareClariaExplodedId = 32;
        public const int TVHardwareLVPExplodedId = 150;
        public const int TVHardwareSPExplodedId = 151;
        public const int TVClariaLanding = 33;
        public const int TVTherapyHome = 34;

        public static int[] TVPreTherapy = { 35, 36, 37, 38, 39, 40 };
        public static int[] TVActiveTherapyGraph = { 41, 42, 43 };
        public static int[] TVActiveTherapyCassette = { 44, 45, 46 };
        //public static int[] TVActiveTherapyMannequin = { 38, 39, 40 };
        public static int[] TVActiveTherapyOsmosis = { 59, 60 };
        public static int[] TVActiveTherapyMachine = { 48, 49, 50 };
        public static int[] TVPostTherapy = { 51, 52, 53, 54, 55, 56 };

        public const int TVVideoControls = 57;

        public const int TVAlarmScenario = 58;

        public const int TVEmpty = 61;

        public const int TVAlarmScenarioOcclusion = 62; //machine content
        public const int TVAlarmScenarioLowDrain = 63; //machine content


        public const string DataReceiverAddress = "/Data";

    }

    [System.Serializable]
    public class GameData
    {
        public int gameId;
        public int dataId;  //Video/Screen
        public int uiId;    //Nurse Action Buttons
        public int screenId;    //Nurse sub menu screen
        public Vector2 scrollPosition;    //Nurse scrollValue
        public bool isSpecialAction;  //(Nurse - side buttons) (Patient - video handling) action
        public float stopDuration;  //stops video after duration secs
        public bool isStart;  //Start of the video
        public int sideProjectorVideoId;  //Start of the video
        public ModelData modelData;

        public static GameData CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<GameData>(jsonString);
        }

        public string SaveToString()
        {
            return JsonUtility.ToJson(this);
        }

        // Given JSON input:
        // {"name":"Dr Charles","lives":3,"health":0.8}
        // this example will return a PlayerInfo object with
        // name == "Dr Charles", lives == 3, and health == 0.8f.
    }

    [System.Serializable]
    public class ModelData {
        public int modelId;
        public string modelName;
        public Vector3 modelPosition;
        public Quaternion modelRotation;
        public bool isReset = false;
        public bool isSouthWestView;
    }


}
