using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class progressvideo : MonoBehaviour
{
    public VideoClip clip1, clip2;
    public VideoPlayer vplayer;
    public TherapyPage thpage;

    public void OnEnable()
    {
        if (thpage == null)
            thpage = GameObject.FindObjectOfType<TherapyPage>();
         
     thpage.vplayer.loopPointReached += EndReach;
        PlayVideo1();
    }

    private void EndReach(VideoPlayer source)
    {
        PlayState();
    }

    public void PlayVideo1()
    {
        currentvideostate = "infusionprogress_1";

        thpage.PlayVideo("infusionprogress_1");
    }

    public void PlayVideo2()
    {
        //currentvideostate = "infusionprogress_2";

        //thpage.PlayVideo("infusionprogress_2");
    }

    public string currentvideostate = "";
    float dta = 0f;
    void PlayState()
    {
        if ((Time.time - dta) > 1f)
        {
            dta = Time.time;
            switch (currentvideostate)
            {
                case "infusionprogress_1":
                    PlayVideo2();
                    break;
                

            }
        }
    }
    internal void PassVideo(VideoPlayer vplayer, VideoClip inf_prog_clip, VideoClip inf_prog_clip2)
    {
       
    }
    private void OnDisable()
    {
        thpage.vplayer.loopPointReached -= EndReach;
    }
}
