﻿using UnityEngine;

public class DragMouseOrbit : MonoBehaviour
{
    public Transform target;
    public float distance = 2.0f;
    public float xSpeed = 20.0f;
    public float ySpeed = 20.0f;
    public float yMinLimit = -90f;
    public float yMaxLimit = 90f;
    public float distanceMin = 10f;
    public float distanceMax = 10f;
    public float smoothTime = 2f;
    public float zoomSmooth = 4f;
    float rotationYAxis = 0.0f;
    float rotationXAxis = 0.0f;
    public float velocityX = 0.0f;
    float velocityY = 0.0f;
    // Use this for initialization

    //public float 

    public GameController gameControllerScript;

    public bool allowDrag = false;


    void Start()
    {
        Vector3 angles = transform.eulerAngles;
        rotationYAxis = angles.y;
        rotationXAxis = angles.x;
        // Make the rigid body not change rotation
        if (GetComponent<Rigidbody>())
        {
            GetComponent<Rigidbody>().freezeRotation = true;
        }


    }
    void LateUpdate()
    {
            //Debug.Log(Vector3.Distance(target.transform.position, transform.position));

        if (!allowDrag) {
            return;
        }

        if (target)
        {

            if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved) {
                Debug.Log("touch");
                velocityX += xSpeed * Input.GetTouch(0).deltaPosition.x * distance * 0.0002f * Time.deltaTime;
                velocityY += ySpeed * Input.GetTouch(0).deltaPosition.y * 0.0002f;
            }
            rotationYAxis -= velocityX;
            rotationXAxis += velocityY;

            rotationXAxis = ClampAngle(rotationXAxis, yMinLimit, yMaxLimit);
            Quaternion fromRotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, 0);
            Quaternion toRotation = Quaternion.Euler(rotationXAxis, rotationYAxis, 0);
            Quaternion rotation = toRotation;
            //Quaternion rotation = Quaternion.Slerp(fromRotation, toRotation, Time.deltaTime * smoothTime);

            distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * 5, distanceMin, distanceMax);
            if (Physics.Linecast(target.position, transform.position, out RaycastHit hit))
            {
                distance -= hit.distance;
            }
            Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
            Vector3 toPosition = rotation * negDistance + target.position;
            Vector3 position;   // = Vector3.Lerp(transform.position, toPosition, Time.deltaTime * smoothTime);
                //position = toPosition;
                position = Vector3.Lerp(transform.position, toPosition, Time.deltaTime * zoomSmooth);

            transform.rotation = rotation;
            transform.position = position;

            //transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * smoothTime);
            //transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * smoothTime);

            //float dis = Vector3.Distance(transform.position, toPosition);
            ////Debug.Log(dis);
            //Debug.Log("D: "+dis+" F: "+ Vector3.Distance(transform.forward, target.forward));
            //if (dis < 0.01f)
            //{
            //    transform.forward = Vector3.Lerp(transform.forward, target.forward, Time.deltaTime * zoomSmooth);
            //}

            velocityX = Mathf.Lerp(velocityX, 0, Time.deltaTime * smoothTime);
            velocityY = Mathf.Lerp(velocityY, 0, Time.deltaTime * smoothTime);
        }

        //Debug.Log(transform.rotation.eulerAngles.y);
    }
    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
    }

    public void SetRotation(Vector2 theRotation) {
        rotationXAxis = theRotation.x;
        rotationYAxis = theRotation.y;
        velocityX = velocityY = 0f;
    }


    public void Test(float f) {
        velocityX = f;
    }
}