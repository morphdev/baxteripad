using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SeekSliderHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    public CanvasGroup sliderHandle;
    public CanvasGroup seekSliderHandle;

    public void OnPointerDown(PointerEventData eventData)
    {
        sliderHandle.alpha = 0f;
        seekSliderHandle.alpha = 1f;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        sliderHandle.alpha = 1f;
        seekSliderHandle.alpha = 0.01f;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

}
