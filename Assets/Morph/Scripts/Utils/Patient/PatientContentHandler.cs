using Com.Morph.Baxter;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class PatientContentHandler : MonoBehaviour
{
    public enum ContentType { AutoMoveToNextState, LastState, ActiveFillMachine, ActiveDwellMachine, ActiveDrainMachine }
    public ContentType contentType;
    public GameController gameController;
    public ActiveTherapyHandler therapyHandler;
    public GameObject lastStateText;
    public TextMeshProUGUI uiText;
    private Coroutine textCoroutine;

    private void OnEnable()
    {
        switch (contentType) {
            case ContentType.AutoMoveToNextState:
                Invoke("SwitchToNextState", 2f);
                return;
            case ContentType.LastState:
                if(lastStateText)
                    lastStateText.SetActive(true);
                return;
            case ContentType.ActiveFillMachine:
                uiText.text = "0";
                if (textCoroutine != null)
                    StopCoroutine(textCoroutine);
                textCoroutine = StartCoroutine(FillAnimation(0, 2000));
                return;
            case ContentType.ActiveDwellMachine:
                uiText.text = "1:08";
                if (textCoroutine != null)
                    StopCoroutine(textCoroutine);
                textCoroutine = StartCoroutine(DwellAnimation(68, 0));
                return;
            case ContentType.ActiveDrainMachine:
                uiText.text = "2000";
                if (textCoroutine != null)
                    StopCoroutine(textCoroutine);
                textCoroutine = StartCoroutine(DrainAnimation(2000, 200));
                return;

            default:
                return;
        }   
    }

    IEnumerator FillAnimation(int start, int end) {
        yield return new WaitForSeconds(0.5f);
        //Debug.Log(Time.time);
        for (int i = start; i <= end; i+=4) {
            yield return new WaitForEndOfFrame();
            uiText.text = i.ToString();
            //Debug.Log("I");
        }
        uiText.text = end.ToString();
        //Debug.Log(Time.time);
    }

    IEnumerator DwellAnimation(int start, int end)
    {
        yield return new WaitForSeconds(0.5f);
        TimeSpan time;
        for (int i = start; i >= end; i--)
        {
            time = TimeSpan.FromSeconds(i);
            uiText.text = time.Minutes.ToString("00")+":"+time.Seconds.ToString("00");
            yield return new WaitForSeconds(0.05f);
        }
    }

    IEnumerator DrainAnimation(int start, int end)
    {
        yield return new WaitForSeconds(0.5f);
        for (int i = start; i >= end; i-=4)
        {
            yield return new WaitForEndOfFrame();
            uiText.text = i.ToString();
        }
    }

    private void OnDisable()
    {
        switch (contentType)
        {
            case ContentType.LastState:
                if (lastStateText)
                    lastStateText.SetActive(false);
                return;
            default:
                return;
        }
    }

    private void SwitchToNextState() {
        switch (GameManager.Instance.CurrentSubMenuScreen) {
            case SubMenuScreen.Patient:
                gameController.OnPatientUIButtonClick(true);
                break;
            case SubMenuScreen.ActiveTherapy:
                therapyHandler.OnMachineButtonClick();
                break;
        }
    }


}
