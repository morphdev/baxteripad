using Com.Morph.Baxter;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisposablePortfolioAT : MonoBehaviour
{
    public string currentID;
    public string currentSubScreenID;
    private int currentScreenId;
    public GameController gameController;
    public MAppController mappcontroller;

    // Start is called before the first frame update
    void OnEnable()
    {
        currentScreenId = 0;
        Debug.Log( "id:: " + transform.GetChild(currentScreenId).gameObject.name);
        //StartCoroutine(StartAnimation());
        //OnNextClick();
    }

    private IEnumerator StartAnimation()
    {
        currentScreenId++;
        if (currentScreenId < transform.childCount)
        {
            transform.GetChild(currentScreenId - 1).gameObject.SetActive(false);
            transform.GetChild(currentScreenId).gameObject.SetActive(true);
            SendData(currentScreenId.ToString());
            //gameController.SendData(Constants.TVConnectivityPatientId, currentScreenId);
            yield return new WaitForSeconds(5f);
            StartCoroutine(StartAnimation());
        }
    }

    private void OnDisable()
    {

        transform.GetChild(currentScreenId).gameObject.SetActive(false);
        //enabled = false;
    }

    public void OnBackClick()
    {
       // gameController.PlayButtonClickSound();
        currentScreenId--;
        if (currentScreenId < transform.childCount)
        {
            transform.GetChild(currentScreenId + 1).gameObject.SetActive(false);
            transform.GetChild(currentScreenId).gameObject.SetActive(true);
            SendData(currentScreenId.ToString());
           // gameController.SendData(Constants.TVConnectivityPatientOrderSuppliesId, currentScreenId);
        }
    }

    public void OnNextClick()
    {
        //gameController.PlayButtonClickSound();
        //Debug.Log("Next");
        currentScreenId++;
        if (currentScreenId < transform.childCount)
        {
            transform.GetChild(currentScreenId - 1).gameObject.SetActive(false);
            transform.GetChild(currentScreenId).gameObject.SetActive(true);
            SendData(currentScreenId.ToString());
            //gameController.SendData(Constants.TVConnectivityPatientOrderSuppliesId, currentScreenId);
        }
       
    }

    public void OnReplay()
    {
        gameController.SendData(Constants.TVConnectivityPatientOrderSuppliesId, currentScreenId);
    }


    void SendData(string currentScreenID)
    {
        MAppData gameData = new MAppData
        {
            screenId = currentID,
            subScreenId = currentSubScreenID,
            subScreenChildId = currentScreenID
        };

        /*  GameData gameData = new GameData
          {
              gameId = Constants.GameId,
              dataId = clickedDataId,
              uiId = 0,
              screenId = explodedID,
              modelData = modelInfo
          };*/

        mappcontroller.SendDisplayData(gameData);
    }

}
