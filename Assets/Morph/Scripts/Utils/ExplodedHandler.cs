using Com.Morph.Baxter;
using UnityEngine;
using DG.Tweening;

public class ExplodedHandler : MonoBehaviour
{
    public int explodedID;
    public Transform camTransform;
    private Animator myAnimator;

    public GameObject[] interactiveButtons;
    public CanvasGroup[] hotspots;

    public Transform[] draggableParts;
    private Vector3[] draggablePartsPositions;

    private Vector3 myPosition;
    private Quaternion myRotation;

    public GameObject resetButton;
    public GameObject switchViewButton;

    public bool isCamDisableException;

    //private float dist;
    //private bool dragging = false;
    //private Vector3 offset;
    //private Transform toDrag;
    private Camera rayCamera;

    public GameController gameControllerScript;
    public DataHandler dataScript;
    public OSC tvOSC;
    //public GameObject viewButtons;

    private Vector3 view1Rotation = new Vector3(10f, 225f, 7.5f);
    private Vector3 view2Rotation = new Vector3(-10f, 45f, -7.5f);
   
    private Transform rootTransform;

    private bool isSouthWestView;

    private void Awake()
    {
        rootTransform = transform;
        rayCamera = camTransform.GetComponent<Camera>();
        if (rootTransform != null)
        {
            myPosition = rootTransform.position;
            myRotation = rootTransform.rotation;
        }
        draggablePartsPositions = new Vector3[draggableParts.Length];
        for (int i = 0; i < draggableParts.Length; i++)
        {
            draggablePartsPositions[i] = draggableParts[i].position;
        }
    }

    private void OnEnable()
    {


        if (resetButton)
        {
            ResetData();
            resetButton.SetActive(true);
        }

        if (switchViewButton)
        {
            switchViewButton.SetActive(true);
        }

        if (camTransform)
            camTransform.gameObject.SetActive(true);

        isSouthWestView = true;
        transform.rotation = Quaternion.Euler(view1Rotation);

        for (int i = 0; i < hotspots.Length; i++)
        {
            hotspots[i].transform.parent.gameObject.SetActive(isSouthWestView ? i <= 6 : i >= 7);
        }

        ModelData modelData = new ModelData
        {
            modelId = -1,
            modelPosition = myPosition,
            modelRotation = myRotation,
            isSouthWestView = isSouthWestView,
            isReset = true
        };
        //gameControllerScript.SendData(Constants.TVHardwareClariaExplodedId, modelData);


        for (int i = 0; i < interactiveButtons.Length; i++)
        {
            if (interactiveButtons[i])
            {
                interactiveButtons[i].SetActive(true);
            }
        }

        /*
        if (viewButtons) {
            viewButtons.SetActive(true);
            viewButtons.transform.GetChild(0).Find("Off").gameObject.SetActive(false);
            viewButtons.transform.GetChild(0).Find("On").gameObject.SetActive(true);
            viewButtons.transform.GetChild(1).Find("Off").gameObject.SetActive(true);
            viewButtons.transform.GetChild(1).Find("On").gameObject.SetActive(false);

            transform.rotation = Quaternion.Euler(view1Rotation);

            ModelData modelData = new ModelData
            {
                modelId = -1,
                modelPosition = myPosition,
                modelRotation = myRotation
            };
            gameControllerScript.SendData(Constants.TVHardwareClariaExplodedId, modelData);

        }
        */
    }

    private void OnDisable()
    {
        if (resetButton)
        {
            ResetData();
            resetButton.SetActive(false);
        }

        if (switchViewButton)
        {
            switchViewButton.SetActive(false);
        }

        if (camTransform && !isCamDisableException)
            camTransform.gameObject.SetActive(false);

        //if (viewButtons)
        //    viewButtons.SetActive(false);
        for (int i = 0; i < interactiveButtons.Length; i++)
        {
            if (interactiveButtons[i])
            {
                interactiveButtons[i].SetActive(false);
            }
        }
    }

    private void ResetData()
    {
        if (rootTransform == null)
            return;
        isSouthWestView = true;

        for (int i = 0; i < hotspots.Length; i++)
        {
            hotspots[i].transform.parent.gameObject.SetActive(isSouthWestView ? i <= 6 : i >= 7);
        }


        rootTransform.position = myPosition;
        rootTransform.rotation = myRotation;
        for (int i = 0; i < draggableParts.Length; i++)
        {
            draggableParts[i].position = draggablePartsPositions[i];
        }
        ModelData modelData = new ModelData
        {
            modelId = -1,
            modelPosition = myPosition,
            modelRotation = myRotation,
            isReset = true,
            isSouthWestView = isSouthWestView
        };
        if (gameControllerScript)
            gameControllerScript.SendData(Constants.TVHardwareClariaExplodedId, modelData);
        else
        {
            SendData(explodedID, modelData);
        }
    }

    void SendData(int clickedDataId, ModelData modelInfo)
    {
        GameData gameData = new GameData
        {
            gameId = Constants.GameId,
            dataId = clickedDataId,
            uiId = 0,
            screenId = explodedID,
            modelData = modelInfo
        };

        dataScript.SendData(gameData.SaveToString(), tvOSC);
    }
    private void SendData(int clickedDataId, int clickedUIId = -1, int subMenuScreenId = -1, bool isAction = false)
    {
        GameData gameData = new GameData
        {
            gameId = Constants.GameId,
            dataId = clickedDataId,
            uiId = clickedUIId,
            screenId = subMenuScreenId,
            isSpecialAction = isAction,
            modelData = null
        };
        Debug.Log("b");
        dataScript.SendData(gameData.SaveToString(), tvOSC);
    }

    public void OnInteractiveClick()
    {
        Debug.Log("a");
        SendData(explodedID);
    }

    // Start is called before the first frame update
    void Start()
    {
        //myAnimator = GetComponent<Animator>();
        OnExplodeEnd();
    }

    private void Update()
    {
        /*
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            //Debug.Log(touch.position);

            if (touch.position.y < Screen.height * 0.2f)
                return;

            Ray ray = rayCamera.ScreenPointToRay(touch.position);
            if (Physics.Raycast(ray, out RaycastHit hit, 100.0f))
            {
                return;
            }

            rootTransform.Rotate(rootTransform.up, Vector3.Dot(touch.deltaPosition, camTransform.right) * Time.deltaTime * Constants.RotationSpeed, Space.World);
            //transform.Rotate(camTransform.right, Vector3.Dot(touch.deltaPosition, camTransform.up) * Time.deltaTime * Constants.RotationSpeed, Space.World);

            //Debug.Log(Vector3.Dot(rootTransform.up, camTransform.up));

            if (resetButton == null)
                return;

            ModelData modelData = new ModelData
            {
                modelId = -1,
                modelPosition = rootTransform.position,
                modelRotation = rootTransform.rotation
            };
            gameControllerScript.SendData(Constants.TVHardwareClariaExplodedId, modelData);

        }
        */

        /*
        Vector3 v3;

        if (Input.touchCount != 1)
        {
            dragging = false;
            return;
        }

        Touch touch = Input.touches[0];
        Vector3 pos = touch.position;

        if (touch.phase == TouchPhase.Began)
        {
            Ray ray = rayCamera.ScreenPointToRay(pos);

            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                return;
                //Debug.Log("Here");
                //toDrag = hit.transform;
                //dist = hit.transform.position.z - rayCamera.transform.position.z;
                //v3 = new Vector3(pos.x, pos.y, dist);
                //v3 = rayCamera.ScreenToWorldPoint(v3);
                //offset = toDrag.position - v3;
                //dragging = true;
            }
        }
        if (dragging && touch.phase == TouchPhase.Moved)
        {
            v3 = new Vector3(Input.mousePosition.x, Input.mousePosition.y, dist);
            v3 = rayCamera.ScreenToWorldPoint(v3);
            toDrag.position = v3 + offset;
        }
        if (dragging && (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled))
        {
            dragging = false;

            Ray ray = rayCamera.ScreenPointToRay(pos);
            int layerMask = LayerMask.GetMask("Default");

            if (Physics.Raycast(ray, out RaycastHit hit, 100f, layerMask))
            {
                Debug.Log("Here: " + hit.transform.name);
                if (hit.transform.name.Equals("Magnet"))
                {
                    toDrag.position = hit.transform.parent.Find("Assemble/" + toDrag.name).position;
                    //toDrag.gameObject.SetActive(false);
                    //hit.transform.parent.Find("Assemble/" + toDrag.name).gameObject.SetActive(true);
                    //toDrag.gameObject.SetActive(false);
                }
            }
        }

        */



    }

    public void OnExplodeEnd()
    {
        //if (gameControllerScript == null)
        //    return;

        foreach (CanvasGroup canvasGroup in hotspots)
            canvasGroup.DOFade(1f, Constants.DelayDuration).SetEase(Ease.InOutSine);

        ModelData modelData = new ModelData
        {
            modelId = -1,
            modelPosition = myPosition,
            modelRotation = myRotation,
            isSouthWestView = isSouthWestView
        };
        if (gameControllerScript)
            gameControllerScript.SendData(Constants.TVHardwareClariaExplodedId, modelData);
        else
            SendData(explodedID, modelData);
    }

    public void OnResetButtonClick()
    {
        if (gameControllerScript)
            gameControllerScript.PlayButtonClickSound();
        ResetData();
    }

    public void OnViewButtonClick(int id)
    {
        /*
        viewButtons.transform.GetChild(0).Find("Off").gameObject.SetActive(true);
        viewButtons.transform.GetChild(0).Find("On").gameObject.SetActive(false);
        viewButtons.transform.GetChild(1).Find("Off").gameObject.SetActive(true);
        viewButtons.transform.GetChild(1).Find("On").gameObject.SetActive(false);

        viewButtons.transform.GetChild(id).Find("Off").gameObject.SetActive(false);
        viewButtons.transform.GetChild(id).Find("On").gameObject.SetActive(true);

        transform.rotation = Quaternion.Euler(view2Rotation);

        ModelData modelData = new ModelData
        {
            modelId = -1,
            modelPosition = myPosition,
            modelRotation = myRotation
        };
        gameControllerScript.SendData(Constants.TVHardwareClariaExplodedId, modelData);
        */
    }

    //switch view button click
    public void OnSwitchViewClick()
    {
        //NorthEast & SouthWest
        isSouthWestView = !isSouthWestView;

        //transform.DORotate(isSouthWestView?view1Rotation:view2Rotation, Constants.TweenDuration).SetEase(Ease.InOutSine);

        for (int i = 0; i < hotspots.Length; i++)
        {
            hotspots[i].transform.parent.gameObject.SetActive(isSouthWestView ? i <= 6 : i >= 7);
        }


        transform.rotation = Quaternion.Euler(isSouthWestView ? view1Rotation : view2Rotation);

        ModelData modelData = new ModelData
        {
            modelId = -1,
            modelPosition = myPosition,
            modelRotation = myRotation,
            isSouthWestView = isSouthWestView
        };

        if (gameControllerScript)
        {
            gameControllerScript.SendData(Constants.TVHardwareClariaExplodedId, modelData);
            gameControllerScript.PlayButtonClickSound();
        }
        else
        {
            SendData(explodedID, modelData);
        }
    }

}
