using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TargetViewer<T> : TargetMono, ITargetViewer where T : class
{
    protected T currentObject;
    protected virtual void Show(T current)
    {
        currentObject = current;
        hidedobject.SetActive(true);
    }
    protected virtual void Hide()
    {
        hidedobject.SetActive(false);
        currentObject = null;
    }    
    void ITargetViewer.Show(object a)
    {
        Show(a as T);

    }

    void ITargetViewer.Hide()
    {
        Hide();
    }
    Type ITargetViewer.GetType
    {
        get
        {
            return typeof(T);
        }
    }

    bool ITargetViewer.IsHideObject
    {
        get
        {
            return IsHide;
        }
    }
}

public class TargetMono : MonoBehaviour
{
    public GameObject hidedobject;
    public bool IsHide = false;
}