﻿using Com.Morph.Baxter;
using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoController : MonoBehaviour
{

    public Slider slider;

    //[HideInInspector]
    public VideoPlayer video;
    //private float playbackSpeed = 1f;
    private GameController gameControllerScript;

    private void Awake()
    {
        if (video != null)
            slider.maxValue = (float) video.length;
    }


    private string Durationtext
    {
        get;
        set;
    }

    private bool IsStarted
    {
        get;
        set;
    }

    public bool IsPlaying
    {
        get;
        set;
    }

    public double VideoTime
    {
        get;
        set;
    }


    public double Duration
    {
        get;
        set;
    }

    public double NTime
    {
        get { return VideoTime / Duration; }
    }

    private void Update()
    {

        if (video == null)
            return;
        if (video.time == 0)
            return;

        VideoTime = video.time;

        //debugText.text = VideoTime.ToString();
        slider.value = (float)VideoTime;

        //if (NTime >= 1f)
        //{
        //    slider.value = 1f;
        //    return;
        //}
        //slider.value = (float)NTime;

    }

    public void OnSeek(Slider seekSlider) {
        gameControllerScript.SendData(Constants.TVVideoControls, seekSlider.value);

        video.time = seekSlider.value * video.length;
        //slider.value = (float)VideoTime;
        //if (!video.isPlaying)
        //    video.Play();
    }

    private void OnEnable()
    {
        slider.value = 0f;
    }



}
