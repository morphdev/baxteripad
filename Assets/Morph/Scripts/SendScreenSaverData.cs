
using Com.Morph.Baxter;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendScreenSaverData : MonoBehaviour
{
    public DataHandler dataScript;
    public bool transmitAtStart;
    public OSC osc;

    public MAppController mappcontroller;                   // added 010722


    // Start is called before the first frame update
    void Start()
    {
        if (transmitAtStart)
            Invoke("TransmitScreenSaverData", 0.2f);
    }

    public void TransmitScreenSaverData()
    {
        dataScript.SendData(Constants.screenSaverAddress, osc);
    }

    public void TransmitScreenSaverDataa(string val)                                                      // added 09-06-22 to send data to manosc
    {
        //  dataScript.SendData("/cues/selected/cues/by_cell/"+ val +"/row_4", osc);                        // old - selected
       
       //  dataScript.SendData("/cues/Bank-1/cues/by_cell/"+ val +"/row_4", osc);                        //commented 010722

        mappcontroller.SendLeftRghtData("/cues/Bank-1/cues/by_cell/" + val + "/row_4");

        Debug.LogError("/cues/Bank-1/cues/by_cell/" + val + "/row_4");

    }


    public void TransmitScreenSaverDataWithDelay()
    {
        Invoke("TransmitScreenSaverDataForAcuteTherapy", 0.1f);
    }

    public void TransmitScreenSaverDataForAcuteTherapy()                                                      // added for first acute therapy btn 09-06-22 to send data to manosc
    {
        dataScript.SendData("/cues/Bank-1/cues/by_cell/col_1/row_4", osc);
       // dataScript.SendData("/cues/selected/cues/by_cell/col_1/row_4", osc);

    }

}
