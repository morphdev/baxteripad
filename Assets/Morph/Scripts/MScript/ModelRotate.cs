using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelRotate : MonoBehaviour
{
    private Vector3 startPosition;      //The coordinate point of the mouse button
    private float x;
    public bool isDown;                 //Indicates whether the mouse is down
    private void OnEnable()
    {
        isDown = false;
    }
    private void OnDisable()
    {
        isDown = false;
    }
    public void Update()
    {
        
        if(Input.GetMouseButtonDown(0))
        {
            isDown = true;
            startPosition = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0))
        {
            isDown = false;
        }
        if (isDown)
        {
            x = Input.mousePosition.x - startPosition.x;
            this.transform.Rotate(new Vector3(0, -x*0.2f, 0));           //Start spinning
            startPosition = Input.mousePosition;                    //Re-record the mouse position
        }
    }
}
