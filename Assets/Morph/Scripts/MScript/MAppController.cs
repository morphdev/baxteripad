using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MAppController : MonoBehaviour
{
    public OSC tvOSC;   //to send data to the display
    public OSC mannOSC; // to send data to the mannequin
    public OSC leftProjectorOSC;   //to send data to the leftProjector
    public OSC rightProjectorOSC; // to send data to the rightProjector
    public DataHandler dataScript;
    public int leftProjectorPort=11002;  //port
    public int rightProjectorPort= 11003;  //port
    // Start is called before the first frame update
    void Start()
    {
        tvOSC.SetAddressHandler("/Data", OnOSCMessageReceived);
    }

    private void OnOSCMessageReceived(OscMessage oscM)
    {
    }
    public void SendDisplayData(MAppData appdata)
    {
        dataScript.SendData(appdata.SaveToString(), tvOSC);
    }
    public void SendLeftRghtData(MAppData appdata)
    {
        dataScript.SendData(appdata.SaveToString(), true, leftProjectorOSC, rightProjectorOSC);
    }
    public void SendLeftRghtData(object appdata)
    {
      //  dataScript.SendData(appdata.ToString(), true, leftProjectorOSC, rightProjectorOSC);
        SendOSCMessage(mannOSC, appdata.ToString());        
    }
    private void SendOSCMessage(OSC osc, string messageAddress)
    {
        OscMessage message;

        message = new OscMessage
        {
            address = messageAddress
        };

        Debug.Log("OSC Message : " + message);

        message.values.Add(1);
        osc.Send(message);
    }
    public void SendManiquenData(string data)
    {
       // dataScript.Send(Constants.MannTherapyTherapyDefaultAddress); //default
    }
    private void OnDestroy()
    {
    }

    private void OnApplicationQuit()
    {
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    internal void ReceiveData(string text)
    {
        Debug.Log("Receive Data XXXX " + text);
    }
}
