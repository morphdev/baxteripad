using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// ADDED IN PD- Therapy -ALarm Scenario
public class PlayPauseEnablerPD : MonoBehaviour
{
    public Image play, pause;
    //public Image  replay;
    // public GameObject videoplayparent;
    // public MVideoSeek mvs;
  //  public AcuteTherapyOperations atOperations;

    private void OnEnable()
    {
        PlayPause(true);
       // PlayPause(false);
        // Invoke("PlayPauseAgain", 0.1f);
    }
    void PlayPauseAgain()
    {
       // PlayPause(true);
        // mvs.slider.maxValue = (float)atOperations.vplayer.length;
    }
    public void PlayPause(bool isplay)
    {
        //atOperations.PlayPauseVideo(isplay);

        if (isplay)
        {
            pause.gameObject.SetActive(true);
            play.gameObject.SetActive(false);
            // replay.gameObject.SetActive(false);

        }
        else
        {
            pause.gameObject.SetActive(false);
            play.gameObject.SetActive(true);
            //  replay.gameObject.SetActive(false);

        }
        // videoplayparent.SetActive(!isplay);
    }
    public void Replay()
    {
        bool isplay = true;
        //atOperations.PlayPauseVideo(isplay);
        if (isplay)
        {
            pause.gameObject.SetActive(true);
            play.gameObject.SetActive(false);
           // replay.gameObject.SetActive(false);
        }
        // videoplayparent.SetActive(!isplay);

    }
}
