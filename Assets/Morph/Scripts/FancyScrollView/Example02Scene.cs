﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace UnityEngine.UI.Extensions.Examples
{
    public class Example02Scene : MonoBehaviour
    {
        [SerializeField]
        Example02ScrollView scrollView;

        public ProductInfo[] productData;

        public GameObject backButton;

        void Start()
        {
            //var cellData = Enumerable.Range(0, 8)
            //     .Select(i => new Example02CellDto { productSprite = productData[i].productSprite, nameText = productData[i].productName, id = i })
            //     .ToList();

            //scrollView.UpdateData(cellData);
            OnItemClick(0);
            //ScrollPositionController scrollPositionController;
            //scrollPositionController.OnItemSelected += OnItemClick;

            //scrollView

        }

        public void OnItemClick(int id)
        {
            //Debug.Log(id);
            List<Example02CellDto> cellData = new List<Example02CellDto>();
            for (int i = id, j = id; i < id + productData.Length; i++, j=(j+1)%productData.Length) {
                cellData.Add(new Example02CellDto { productSprite = productData[j].productSprite, nameText = productData[j].productName, id = j });
            }
            scrollView.UpdateData(cellData);
            StartCoroutine(DelayedActivation(scrollView.gameObject));
        }

        private IEnumerator DelayedActivation(GameObject g) {
            yield return new WaitForEndOfFrame();
            g.SetActive(true);
            yield return new WaitForEndOfFrame();
            g.SetActive(false);
            yield return new WaitForEndOfFrame();
            g.SetActive(true);
            backButton.SetActive(true);
        }

    }

    [System.Serializable]
    public class ProductInfo {
        public Sprite productSprite;
        public string productName;
    }


}
