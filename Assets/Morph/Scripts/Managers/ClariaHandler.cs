using System.Collections;
using System.Collections.Generic;
using Com.Morph.Baxter;
using UnityEngine;

public class ClariaHandler : MonoBehaviour
{

    public GameObject[] pointers;
    private Transform myTransform;
    public Camera uiCamera;

    private void Awake()
    {
        foreach (GameObject g in pointers) {
            g.GetComponentInChildren<Canvas>().worldCamera = uiCamera;
            g.GetComponent<WorldUIHandler>().uiCamera = uiCamera;
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        myTransform = transform;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved)
            myTransform.rotation = Quaternion.AngleAxis(Constants.RotationSpeed * Time.deltaTime, Vector3.up);
    }

    private void OnEnable()
    {
        foreach (GameObject g in pointers) 
            g.SetActive(true);
    }
}
