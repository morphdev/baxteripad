using Com.Morph.Baxter;
using UnityEngine;

public class DummyHandler : MonoBehaviour
{
    private GameController gameControllerScript;
    private void Awake()
    {
        gameControllerScript = GameObject.FindGameObjectWithTag(Constants.GameControllerTag).GetComponent<GameController>();
    }

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("@@@@ AR Start @@@@");
    }

    private void OnEnable()
    {
        Debug.Log("@@@@ AR OnEnable @@@@");
       // gameControllerScript.OnScanSuccess();                     // commented 31-03-22
    }

    private void OnDisable()
    {
        Debug.Log("@@@@ AR OnDisable @@@@");
    }

}
