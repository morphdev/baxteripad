﻿using Com.Morph.Baxter;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;

/// <summary>
/// DataHandler
/// </summary>
public class DataHandler : MonoBehaviour
{
    private Thread receiveThread;   //receiveThread
    private UdpClient client;   //udp client
    public int defaultPort;  //port
    public int destinationPort;  //port
    public int leftProjectorPort;  //port
    public int rightProjectorPort;  //port
    public string destinationIP;


    //private UIController uiScript;  //reference to the uiScript

    #region UnityCallbacks
    // Start is called before the first frame update
    void Start()
    {
        defaultPort = PlayerPrefs.GetInt("srcPort", 8003); //get the value from the PlayerPref
        destinationPort = PlayerPrefs.GetInt("destPort", 11000); //get the value from the PlayerPref
        //destinationIP = PlayerPrefs.GetString("destIP", "192.168.1.192");
        destinationIP = PlayerPrefs.GetString("destIP", "255.255.255.255");
        leftProjectorPort = 11002;//PlayerPrefs.GetInt("leftPort", 11002); //get the value from the PlayerPref
        rightProjectorPort = 11003;//PlayerPrefs.GetInt("rightPort", 11003); //get the value from the PlayerPref
    }

    private void OnDestroy()
    {
        AbortThread();
    }

    private void OnApplicationQuit()
    {
        AbortThread();
    }
    #endregion

    #region UDP
    /// <summary>
    /// Initializes the UDP
    /// </summary>
    private void InitUDP()
    {
        receiveThread = new Thread(new ThreadStart(ReceiveData))
        {
            IsBackground = true
        };
        receiveThread.Start();
    }

    /// <summary>
    /// ReceiveData thread
    /// </summary>
    private void ReceiveData()
    {
        client = new UdpClient(defaultPort);
        while (true)
        {
            try
            {
                IPEndPoint anyIP = new IPEndPoint(IPAddress.Parse("0.0.0.0"), defaultPort);
                byte[] data = client.Receive(ref anyIP);

                string text = Encoding.UTF8.GetString(data);
                Debug.Log(text);
                //uiScript.OnDataReceived(text);
                if (Application.loadedLevel == 2)
                {
                    GameObject.FindObjectOfType<MAppController>().ReceiveData(text);
                }
                if (Application.loadedLevel == 1)
                {

                }
                if (Application.loadedLevel == 2)
                {

                }
            }
            catch (Exception e)
            {
                Debug.Log(e.ToString());
            }
        }
    }

    private void AbortThread()
    {
        if (receiveThread != null)
            receiveThread.Abort();
    }

    static void SendUdp(int srcPort, string dstIp, int dstPort, byte[] data)
    {
        using (UdpClient c = new UdpClient(srcPort))
            c.Send(data, data.Length, dstIp, dstPort);

        //Debug.Log(data.Length);

        //var data = Encoding.Default.GetBytes(message);
        //using (var udpClient = new UdpClient(AddressFamily.InterNetwork))
        //{
        //    var address = IPAddress.Parse("224.168.100.2");
        //    var ipEndPoint = new IPEndPoint(address, 8088);
        //    udpClient.JoinMulticastGroup(address);
        //    udpClient.Send(data, data.Length, ipEndPoint);
        //    udpClient.Close();
        //}

    }

    //void SetLocalIP() {
    //    using Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0);
    //    socket.Connect("8.8.8.8", 65530);
    //    IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;
    //    localIP = endPoint.Address.ToString();
    //}
    #endregion

    #region HelperFunctons
    public void SendData(string dataToSend, OSC oscReceiver)
    {
        //SendUdp(defaultPort, destinationIP, destinationPort, Encoding.ASCII.GetBytes(dataToSend));
        OscMessage message;

        message = new OscMessage
        {
            address = Constants.DataReceiverAddress
        };

        Debug.Log("OSC  " + dataToSend);

        message.values.Add(dataToSend);
        oscReceiver.Send(message);
    }

    public void SendData(string dataToSend, bool isSideProjector, OSC leftReceiver, OSC rightReceiver)
    {
        //SendUdp(defaultPort, destinationIP, leftProjectorPort, Encoding.ASCII.GetBytes(dataToSend));
        //SendUdp(defaultPort, destinationIP, rightProjectorPort, Encoding.ASCII.GetBytes(dataToSend));
        OscMessage message;

        message = new OscMessage
        {
            address = Constants.DataReceiverAddress
        };
        message.values.Add(dataToSend);
        leftReceiver.Send(message);
        rightReceiver.Send(message);
        //Debug.Log("Left message : " + message);
        //Debug.Log("Right message : " + message);
        Debug.Log("Sending: " + leftReceiver.outIP + " p: " + leftReceiver.outPort);
        Debug.Log("Sending: " + rightReceiver.outIP);
    }

    #endregion
}
/*
using Com.Morph.Baxter;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;

/// <summary>
/// DataHandler
/// </summary>
public class DataHandler : MonoBehaviour
{
    private Thread receiveThread;   //receiveThread
    private UdpClient client;   //udp client
    private int defaultPort;  //port
    private int destinationPort;  //port
    private int leftProjectorPort;  //port
    private int rightProjectorPort;  //port
    private string destinationIP;


    //private UIController uiScript;  //reference to the uiScript

    #region UnityCallbacks
    // Start is called before the first frame update
    void Start()
    {
        defaultPort = PlayerPrefs.GetInt("srcPort", 8003); //get the value from the PlayerPref
        destinationPort = PlayerPrefs.GetInt("destPort", 11000); //get the value from the PlayerPref
        //destinationIP = PlayerPrefs.GetString("destIP", "192.168.1.192");
        destinationIP = PlayerPrefs.GetString("destIP", "255.255.255.255");
        leftProjectorPort = PlayerPrefs.GetInt("leftPort", 11002); //get the value from the PlayerPref
        rightProjectorPort = PlayerPrefs.GetInt("rightPort", 11003); //get the value from the PlayerPref
    }

    private void OnDestroy()
    {
        AbortThread();
    }

    private void OnApplicationQuit()
    {
        AbortThread();
    }
    #endregion

    #region UDP
    /// <summary>
    /// Initializes the UDP
    /// </summary>
    private void InitUDP()
    {
        receiveThread = new Thread(new ThreadStart(ReceiveData))
        {
            IsBackground = true
        };
        receiveThread.Start();
    }

    /// <summary>
    /// ReceiveData thread
    /// </summary>
    private void ReceiveData()
    {
        client = new UdpClient(defaultPort);
        while (true)
        {
            try
            {
                IPEndPoint anyIP = new IPEndPoint(IPAddress.Parse("0.0.0.0"), defaultPort);
                byte[] data = client.Receive(ref anyIP);

                string text = Encoding.UTF8.GetString(data);
                Debug.Log(text);
                //uiScript.OnDataReceived(text);
            }
            catch (Exception e)
            {
                Debug.Log(e.ToString());
            }
        }
    }

    private void AbortThread()
    {
        if (receiveThread != null)
            receiveThread.Abort();
    }

    static void SendUdp(int srcPort, string dstIp, int dstPort, byte[] data)
    {
        using (UdpClient c = new UdpClient(srcPort))
            c.Send(data, data.Length, dstIp, dstPort);

        //Debug.Log(data.Length);

        //var data = Encoding.Default.GetBytes(message);
        //using (var udpClient = new UdpClient(AddressFamily.InterNetwork))
        //{
        //    var address = IPAddress.Parse("224.168.100.2");
        //    var ipEndPoint = new IPEndPoint(address, 8088);
        //    udpClient.JoinMulticastGroup(address);
        //    udpClient.Send(data, data.Length, ipEndPoint);
        //    udpClient.Close();
        //}

    }

    //void SetLocalIP() {
    //    using Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0);
    //    socket.Connect("8.8.8.8", 65530);
    //    IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;
    //    localIP = endPoint.Address.ToString();
    //}
    #endregion

    #region HelperFunctons
    public void SendData(string dataToSend, OSC oscReceiver)
    {
        //SendUdp(defaultPort, destinationIP, destinationPort, Encoding.ASCII.GetBytes(dataToSend));
        OscMessage message;

        message = new OscMessage
        {
            address = Constants.DataReceiverAddress
        };
        message.values.Add(dataToSend);
        oscReceiver.Send(message);
    }

    public void SendData(string dataToSend, bool isSideProjector, OSC leftReceiver, OSC rightReceiver)
    {
        //SendUdp(defaultPort, destinationIP, leftProjectorPort, Encoding.ASCII.GetBytes(dataToSend));
        //SendUdp(defaultPort, destinationIP, rightProjectorPort, Encoding.ASCII.GetBytes(dataToSend));
        OscMessage message;

        message = new OscMessage
        {
            address = Constants.DataReceiverAddress
        };
        message.values.Add(dataToSend);
        leftReceiver.Send(message);
        rightReceiver.Send(message);
        //Debug.Log("Left message : " + message);
        //Debug.Log("Right message : " + message);
        Debug.Log("Sending: " + leftReceiver.outIP + " p: " + leftReceiver.outPort);
        Debug.Log("Sending: " + rightReceiver.outIP);
    }

    #endregion
}
*/
