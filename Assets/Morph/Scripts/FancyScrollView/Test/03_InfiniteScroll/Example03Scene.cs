﻿using System.Linq;

namespace UnityEngine.UI.Extensions.Examples
{
    public class Example03Scene : MonoBehaviour
    {
        [SerializeField]
        Example03ScrollView scrollView;

        void Start()
        {
            var cellData = Enumerable.Range(0, 5)
                .Select(i => new Example03CellDto { Message = "Cell " + i })
                .ToList();

            scrollView.UpdateData(cellData);
        }
    }
}
